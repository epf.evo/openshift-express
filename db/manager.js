const sqlite3 = require('sqlite3').verbose();
const path = require('path');

const getApplicationDB = function () {
    const dbPath = path.join(__dirname, 'application.db')
    console.log(dbPath)
    return new sqlite3.Database(dbPath, (err) => {
        if (err) { console.error(err.message); }
        //console.log('Connected to the application database.');
    });
}
const getMasterdataDB = function () {
    const dbPath = path.join(__dirname, 'masterdata.db')
    console.log(dbPath)
    return new sqlite3.Database(dbPath, (err) => {
        if (err) { console.error(err.message); }
        //console.log('Connected to the masterdata database.');
    });
}
const getTransactionDB = function () {
    const dbPath = path.join(__dirname, 'transactiondata.db')
    console.log(dbPath)
    return new sqlite3.Database(dbPath, (err) => {
        if (err) { console.error(err.message); }
        //console.log('Connected to the transaction database.');
    });
}

const application = {
    getApplicationSettings: function (employerEpfNum) {
        return new Promise(function (resolve, reject) {
            const trxDb = getTransactionDB();
            let employer = null
            trxDb.get(`SELECT * FROM employer where employerEpfNum='${employerEpfNum}'`, [], (err, result) => {
                employer = result
                if(!employer) {
                    reject(`Employer ${employerEpfNum} not found`)
                    return;
                }

                const appDb = getApplicationDB();
                //console.log(`SELECT * FROM applicationSettings where employerEpfNum='${employer.employerEpfNum}'`);
                appDb.all(`SELECT * FROM applicationSettings where employerEpfNum='${employer.employerEpfNum}'`, [], (err, rows) => {
                    rows = rows.map(row => {
                        delete row.employerEpfNum
                        row.settingsContent = JSON.parse(row.settingsContent)
                        row.completed = row.completed == 1
                        return row
                    })

                    terms = rows.filter(row => row.settingsType == "terms")[0]
                    steps = rows.filter(row => row.settingsType != "terms")
                        .sort((a, b) => a.settingsType - b.settingsType)

                    resolve({
                        employer: employer,
                        terms: terms,
                        steps: steps
                    });
                })
                appDb.close();
            })
            trxDb.close()
        })

    },
    setApplicationSettings: function (object) {
        let employer = object.employer
        return new Promise(function (resolve, reject) {
            const trxDb = getTransactionDB();
            trxDb.run(`
                INSERT OR REPLACE INTO employer (employerEpfNum,companyName)
                VALUES ("${employer.employerEpfNum}","${employer.companyName}")`)
            resolve({})
        })
    }
}

////////////////////////////////////////////////////////////////



const masterData = {
    getListUserStatus: function () {
        return new Promise(function (resolve, reject) {
            const db = getMasterdataDB();
            db.all(`SELECT * FROM userStatus`, [], (err, rows) => { resolve(rows); })
            db.close();
        })
    },
    getListIdentificationType: function () {
        return new Promise(function (resolve, reject) {
            const db = getMasterdataDB();
            db.all(`SELECT * FROM identificationType`, [], (err, rows) => { resolve(rows); })
            db.close();
        })
    },
    getListApprovalStatus: function () {
        return new Promise(function (resolve, reject) {
            const db = getMasterdataDB();
            db.all(`SELECT * FROM approvalStatus`, [], (err, rows) => { resolve(rows); })
            db.close();
        })
    },
    getListIdType: function () {
        return new Promise(function (resolve, reject) {
            const db = getMasterdataDB();
            db.all(`SELECT * FROM idType`, [], (err, rows) => { resolve(rows); })
            db.close();
        })
    }
}

const transactionData = {
    getEmployer: function (employerEpfNum) {
        return new Promise(function (resolve, reject) {
            const trxDb = getTransactionDB();
            let employer = null
            trxDb.get(`SELECT * FROM employer where employerEpfNum='${employerEpfNum}'`, [], (err, result) => {
                employer = result
                employer.payrollAgreement = JSON.parse(employer.payrollAgreement)
                employer.settings = JSON.parse(employer.settings)
                if(!employer) {
                    reject(`Employer ${employerEpfNum} not found`)
                    return;
                }
                resolve(employer);
            })
        })
    },
    setEmployer: function (employerEpfNum,object) {
        let employer = object

        let payrollAgreement = JSON.stringify(employer.payrollAgreement)
        let settings = JSON.stringify(employer.settings)
        let sql = `INSERT OR REPLACE INTO employer (employerEpfNum,employerName,payrollAgreement,settings) VALUES (?,?,?,?)`
        console.log(sql,)
        //console.log(`payrollAgreement = ${payrollAgreement}`)
        return new Promise(function (resolve, reject) {
            const trxDb = getTransactionDB();
            trxDb.run(sql,[employer.employerEpfNum,employer.employerName,payrollAgreement,settings])
            resolve({})
        })
    }
}
/*
const initialize = function() {

    const userStatus = [
        '- One',
        '- Two',
        '- Three'
    ]

    const approvalStatus = [
        'Saved as Draft',
        'Submitted',
        'Submit for Approval',
        'Approved',
        'Partially Approved',
        'Rejected',
    ]

    const idType = [
        { id : 'mykad', name : 'MyKad' },
        { id : 'oldic', name : 'Old IC' },
        { id : 'passp', name : 'Passport' }
    ]


    return new Promise(function(resolve,reject){
        let messages = []
        const db = getMasterdataDB()
        db.serialize(() => {
            db.run('DROP TABLE IF EXISTS userStatus')
            db.run('DROP TABLE IF EXISTS identificationType')
            db.run('DROP TABLE IF EXISTS approvalStatus')
            db.run('DROP TABLE IF EXISTS idType')

            db.run('CREATE TABLE userStatus(name text)')
            db.run('CREATE TABLE approvalStatus(name text)')
            db.run('CREATE TABLE idType(id text, name text)')

            db.run(
                'INSERT INTO userStatus(name) VALUES ' + userStatus.map(
                    (status) => '(?)'
                ).join(','),
                userStatus, 
                insertCallback
            );

            db.run(
                'INSERT INTO approvalStatus(name) VALUES ' + approvalStatus.map(
                    (status) => '(?)'
                ).join(','), 
                approvalStatus, 
                insertCallback
            );

            let stmt = db.prepare("INSERT INTO idType(id,name) VALUES (?,?)");
            idType.map((row) => {
                stmt.run(row.id,row.name)
            });
            stmt.finalize();
            
            db.close();
            messages.push("Initialized")
            console.log(messages)
            resolve(messages);
        });
        
        
    });

    
}
*/
const insertCallback = function (err) {
    if (err) { return console.error(err.message); }
    //console.log(`Rows inserted ${this.changes}`);
}

module.exports = {
    //initialize  : initialize,
    masterData: masterData,
    transactionData: transactionData,
    application: application
}