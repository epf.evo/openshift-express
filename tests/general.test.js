const fetch = require('node-fetch');
const mockdata = require('./mockdata')
const host = 'http://localhost:8080';

const post = (payload) => {
    return {
        method: 'post',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    }
}



describe('Test Employer', () => {
    test('Testing get and set employer API', async () => {
        await fetch(`${host}/api/tx/employer/0123456789`, post(mockdata.employer01))

        let testResult = await fetch(`${host}/api/tx/employer/0123456789`).then(r=>r.json())
        expect(testResult.value).toEqual(mockdata.employer01)

    })
})
