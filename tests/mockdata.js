const settings01 = {
    "statutory": {
        "completed": true,
        "content": {
            "epf": {
                "accountNumber": 123456789,
                "activate": true,
                "content": {
                    "above": {
                        "employee": 12,
                        "employer": 13
                    },
                    "below": {
                        "employee": 12,
                        "employer": 13
                    }
                }
            },
            "socso": {
                "accountNumber": 900322085544,
                "activate": true,
                "content": {}
            },
            "lhdn": {
                "accountNumber": 21560704020,
                "activate": true,
                "content": {}
            },
            "eis": {
                "accountNumber": 21560704020,
                "activate": true,
                "content": {}
            },
            "hrdf": {
                "accountNumber": 21560704020,
                "activate": true,
                "content": {}
            }
        }
    },
    "compensation": {
        "completed": true,
        "content": [
            {
                "id": "",
                "name": "Basic Fixed Salary",
                "statutory": [
                    {
                        "name": "epf",
                        "status": true
                    },
                    {
                        "name": "socso",
                        "status": true
                    },
                    {
                        "name": "lhdn",
                        "status": true
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            },
            {
                "id": "",
                "name": "Hourly Salary",
                "statutory": [
                    {
                        "name": "epf",
                        "status": false
                    },
                    {
                        "name": "socso",
                        "status": false
                    },
                    {
                        "name": "lhdn",
                        "status": true
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            },
            {
                "id": "",
                "name": "Bonus",
                "statutory": [
                    {
                        "name": "epf",
                        "status": true
                    },
                    {
                        "name": "socso",
                        "status": false
                    },
                    {
                        "name": "lhdn",
                        "status": true
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            },
            {
                "id": "",
                "name": "Fixed Allowance",
                "statutory": [
                    {
                        "name": "epf",
                        "status": true
                    },
                    {
                        "name": "socso",
                        "status": true
                    },
                    {
                        "name": "lhdn",
                        "status": true
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            },
            {
                "id": "",
                "name": "Commission",
                "statutory": [
                    {
                        "name": "epf",
                        "status": true
                    },
                    {
                        "name": "socso",
                        "status": true
                    },
                    {
                        "name": "lhdn",
                        "status": true
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            },
            {
                "id": "",
                "name": "Service Charge",
                "statutory": [
                    {
                        "name": "epf",
                        "status": false
                    },
                    {
                        "name": "socso",
                        "status": true
                    },
                    {
                        "name": "lhdn",
                        "status": false
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            },
            {
                "id": "",
                "name": "Overtime",
                "statutory": [
                    {
                        "name": "epf",
                        "status": false
                    },
                    {
                        "name": "socso",
                        "status": true
                    },
                    {
                        "name": "lhdn",
                        "status": true
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            },
            {
                "id": "",
                "name": "Paid Leave",
                "statutory": [
                    {
                        "name": "epf",
                        "status": true
                    },
                    {
                        "name": "socso",
                        "status": true
                    },
                    {
                        "name": "lhdn",
                        "status": true
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            },
            {
                "id": "",
                "name": "Director's Fee",
                "statutory": [
                    {
                        "name": "epf",
                        "status": false
                    },
                    {
                        "name": "socso",
                        "status": false
                    },
                    {
                        "name": "lhdn",
                        "status": true
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            },
            {
                "id": "",
                "name": "Tips",
                "statutory": [
                    {
                        "name": "epf",
                        "status": false
                    },
                    {
                        "name": "socso",
                        "status": false
                    },
                    {
                        "name": "lhdn",
                        "status": true
                    },
                    {
                        "name": "hrdr",
                        "status": false
                    },
                    {
                        "name": "eis",
                        "status": false
                    }
                ]
            }
        ]
    },
    "bankAccount": {
        "completed": true,
        "content": {
            "bank": "Maybank",
            "accountName": "Company One Sdn. Bhd.",
            "accountNumber": 21345623432
        }
    },
    "payrollSchedule": {
        "completed": true,
        "content": {
            "type": {
                "once": true,
                "twice": false,
                "weekly": false
            },
            "value": [28]
        }
    },
    "user": {
        "content": [
            {
                "id": "",
                "name": "Sarah",
                "role": "Admin",
                "email": "email@address.com",
                "phone": "+60125749305",
                "date": "2012-04-23T18:25:43.511Z"
            },
            {
                "id": "",
                "name": "Cristina Chen Hui Min",
                "role": "Checker",
                "email": "email@address.com",
                "phone": "+60125749305",
                "date": "2012-04-23T18:25:43.511Z"
            },
            {
                "id": "",
                "name": "Muhammad Famirul Huzainan",
                "role": "Maker",
                "email": "email@address.com",
                "phone": "+60125749305",
                "date": "2012-04-23T18:25:43.511Z"
            },
            {
                "id": "",
                "name": "Terry Wallace",
                "role": "Maker",
                "email": "email@address.com",
                "phone": "+60125749305",
                "date": "2012-04-23T18:25:43.511Z"
            }
        ]
    }
}
const employer01 = {
    "employerEpfNum": "0123456789",
    "employerName": "Company One Sdn. Bhd.",
    "payrollAgreement": {
        "general": false,
        "optin": true
    },
    "settings": settings01
}

module.exports = {
    employer01 : employer01,
    settings01 : settings01
}

