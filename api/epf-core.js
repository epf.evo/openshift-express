const db = require('../db/manager')

const initialize = function(req,res) {

    res.setHeader('Content-Type', 'application/json');
    db.initialize().then((messages)=>{
        const requestDate = new Date()
        res.send(JSON.stringify({ 
            messages : messages,
            date     : requestDate
        }));
    })
    
}

const userStatus = function(req,res) {
    res.setHeader('Content-Type', 'application/json');
    db.masterData.getListUserStatus().then((list)=>{
        const requestDate = new Date()
        res.send(JSON.stringify({
            values : list,
            date   : requestDate
        }));
    });
}

const identificationType = function(req,res) {
    res.setHeader('Content-Type', 'application/json');
    db.masterData.getListIdentificationType().then((list)=>{
        const requestDate = new Date()
        res.send(JSON.stringify({
            values : list,
            date   : requestDate
        }));
    });
}

const approvalStatus = function(req,res) {
    res.setHeader('Content-Type', 'application/json');
    db.masterData.getListApprovalStatus().then((list)=>{
        const requestDate = new Date()
        res.send(JSON.stringify({
            values : list,
            date   : requestDate
        }));
    });
}

const idType = function(req,res) {
    res.setHeader('Content-Type', 'application/json');
    db.masterData.getListIdType().then((list)=>{
        const requestDate = new Date()
        res.send(JSON.stringify({
            values : list,
            date   : requestDate
        }));
    });
}

const ping = function(req,res) {
    const requestDate = new Date()  
    let value = getParameter(req,'value','Not Provided')
    console.log('Body',req.body)
    console.log('Query',req.query)
    

    //res.setHeader('Content-Type', 'application/json');
    res.json({
        data   : {
            value : value
        },
        date   : requestDate
    });
}

module.exports = {
    ping               : ping,
    initialize         : initialize,
    userStatus         : userStatus,

    approvalStatus     : approvalStatus,
    idType             : idType,
}