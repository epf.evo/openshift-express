var webPush    = require('web-push');

const publicVapidKey  = 'BO2wgS_RCQzLuDkUa9IKiq8Tb_JeuMFfU8p6jyTAPS1Zv3KuApi251jz8rl4MMWVZTqwpQJhLqVLUSSZwLFVSHg'
const privateVapidKey = 'rP16ICcBMg3kvFLO57lUkjuIt5MF2KS4hcZ8jwXcFhI'

let subscriptions = {};

webPush.setVapidDetails('mailto:test@example.com',publicVapidKey,privateVapidKey)

const sendNotification = function (subscription,data) {
  webPush.sendNotification(subscription,JSON.stringify(data||{}))
  .then(function() {
    console.log('Push Application Server - Notification sent to ' + subscription.endpoint);
  }).catch(function() {
    console.log('ERROR in sending Notification, endpoint removed ' + subscription.endpoint);
    delete subscriptions[subscription.endpoint];
  });
}

const vapidPublicKey = function(req,res) {
    res.send(publicVapidKey);
}

const register = (callback = () => {}) => function(req, res) {
    var subscription = req.body.subscription

    if (!subscriptions[subscription.endpoint]) {
      subscriptions[subscription.endpoint] = subscription;
      //res.log = JSON.stringify(subscriptions)
      callback(subscription)
    }
    res.sendStatus(201);
  }

const unregister = (callback = () => {}) => function(req, res) {
  var subscription = req.body.subscription;
  if (subscriptions[subscription.endpoint]) {
    delete subscriptions[subscription.endpoint];
    //res.log = JSON.stringify(subscriptions)
    callback(subscription)
  }
  res.sendStatus(201);
}

const pushAll = function(req, res, next) {
    const pushSubscription = req.body.pushSubscription;
    const notificationMessage = req.body.notificationMessage;
  
    if (!pushSubscription) {
      res.status(400).send(constants.errors.ERROR_SUBSCRIPTION_REQUIRED);
      return next(false);
    }
  
    if (subscriptions.length) {
      subscriptions.map((subscription, index) => {
        let jsonSub = JSON.parse(subscription);
  
        // Use the web-push library to send the notification message to subscribers
        webPush
          .sendNotification(jsonSub, notificationMessage)
          .then(success => handleSuccess(success, index))
          .catch(error => handleError(error, index));
      });
    } else {
      res.send(constants.messages.NO_SUBSCRIBERS_MESSAGE);
      return next(false);
    }
  
    function handleSuccess(success, index) {
      res.send(constants.messages.SINGLE_PUBLISH_SUCCESS_MESSAGE);
      return next(false);
    }
  
    function handleError(error, index) {
      res.status(500).send(constants.errors.ERROR_MULTIPLE_PUBLISH);
      return next(false);
    }
};

const pushSingle = function(req, res, next) {
    const pushSubscription = req.body.pushSubscription;
    const notificationMessage = req.body.notificationMessage;
  
    if (!pushSubscription) {
      res.status(400).send(constants.errors.ERROR_SUBSCRIPTION_REQUIRED);
      return next(false);
    }
  
    if (subscriptions.length) {
      subscriptions.map((subscription, index) => {
        let jsonSub = JSON.parse(subscription);
  
        // Use the web-push library to send the notification message to subscribers
        webPush
          .sendNotification(jsonSub, notificationMessage)
          .then(success => handleSuccess(success, index))
          .catch(error => handleError(error, index));
      });
    } else {
      res.send(constants.messages.NO_SUBSCRIBERS_MESSAGE);
      return next(false);
    }
  
    function handleSuccess(success, index) {
      res.send(constants.messages.SINGLE_PUBLISH_SUCCESS_MESSAGE);
      return next(false);
    }
  
    function handleError(error, index) {
      res.status(500).send(constants.errors.ERROR_MULTIPLE_PUBLISH);
      return next(false);
    }
};

module.exports = {
  vapidPublicKey     : vapidPublicKey,
  register           : register,
  unregister         : unregister,
  pushSingle         : pushSingle,
  pushAll            : pushAll
};

/*
//FOR TESTING ONLY, SEND NOTIFICATIONS AT INTERVALS
const pushInterval = 30;
setInterval(function() {
  Object.values(subscriptions).forEach(subscription=>sendNotification(subscription,{
    title   : "Test title",
    options : {
      body: "Test message",
      icon: 'images/notification-flat.png',
      vibrate: [100, 50, 100],
      data: {
        dateOfArrival: Date.now(),
        primaryKey: 1
      },
      actions: [
        {action: 'explore', title: 'Go to the site',
          icon: 'images/checkmark.png'},
        {action: 'close', title: 'Close the notification',
          icon: 'images/xmark.png'},
      ]
    }
  }));
}, pushInterval * 1000);
*/
