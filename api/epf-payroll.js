const db = require('../db/manager')

const getEmployer = function(req,res) {
    const requestDate = new Date()
    db.transactionData.getEmployer(req.params.id).then((employer)=>{
        res.json({
            value  : employer,
            status : 200,
            date   : requestDate
        });
    })
}
const setEmployer = function(req,res) {
    const requestDate = new Date()
    let objUpdate = req.body
    db.transactionData.setEmployer(req.params.id,objUpdate).then((updated)=>{
        res.json({
            value  : {},
            status : 200,
            date   : requestDate
        });
    })
}
const getApplicationSettings = function(req,res) {
    const requestDate = new Date()
    //res.setHeader('Content-Type', 'application/json');
    let employerEpfNo = req.query.employerEpfNo 
    //console.log('getApplicationSettings Query',req.query)
    db.application.getApplicationSettings(employerEpfNo).then((settings)=>{
        res.json({
            value  : settings,
            status : 200,
            date   : requestDate
        });
    })
    .catch(error=>{
        res.send(JSON.stringify({
            error  : error,
            status : 400,
            date   : requestDate
        }));
    });
}

const setApplicationSettings = function(req,res) {
    const requestDate = new Date()
    let objUpdate = req.body
    res.setHeader('Content-Type', 'application/json');
    db.application.setApplicationSettings(objUpdate).then((updated)=>{
        res.send(JSON.stringify({
            value  : updated,
            status : 200,
            date   : requestDate
        }));
    })
    .catch(error=>{
        res.send(JSON.stringify({
            error  : error,
            status : 400,
            date   : requestDate
        }));
    });
}
module.exports = {
    getApplicationSettings : getApplicationSettings,
    setApplicationSettings : setApplicationSettings,

    getEmployer : getEmployer,
    setEmployer : setEmployer
}