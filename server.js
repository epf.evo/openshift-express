//  OpenShift sample Node application
var express    = require('express'),
    app        = express(),
    morgan     = require('morgan'),
    bodyParser = require('body-parser');
    
Object.assign=require('object-assign')

app.engine('html', require('ejs').renderFile);

morgan.token('handler', function (req,res) { return res.handler })
morgan.token('log', function (req,res) { return res.log })
app.use(morgan(':handler :method :url :log'));
app.use(express.static('build/app',{setHeaders:function(res){res.handler='[STATIC]'}}));

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
    mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL,
    mongoURLLabel = "";

const path_prefix = ''
const path = require('path');

const epfCore    = require('./api/epf-core');
const epfPayroll = require('./api/epf-payroll');
const push    = require('./api/push')
const jsonParser = bodyParser.json({ type: 'application/json' } );

const handler = function(handler) {
  return function (req,res,next) {
    res.handler = handler
    next();
  }
}
//Allow CORS for development purposes
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// /api/jb provide paths to one off jobs to be executed
app.get ('/api/jb/initialize'         , epfCore.initialize);

// /api/ap provide paths to appliction related info
app.get ('/api/ap/getSettings'        ,handler('[APP DATA]\t'), jsonParser, epfPayroll.getApplicationSettings);
app.post('/api/ap/setSettings'        ,handler('[APP DATA]\t'), jsonParser, epfPayroll.setApplicationSettings);

// /api/md provide paths to masterdata
app.get ('/api/md/ping'               ,handler('[MASTER DATA]\t'), jsonParser, epfCore.ping);
app.get ('/api/md/userStatus'         ,handler('[MASTER DATA]\t'), jsonParser, epfCore.userStatus);
app.get ('/api/md/approvalStatus'     ,handler('[MASTER DATA]\t'), jsonParser, epfCore.approvalStatus);
app.get ('/api/md/idType'             ,handler('[MASTER DATA]\t'), jsonParser, epfCore.idType);

// /api/tx provide paths to transactions
app.post('/api/tx/ping'               ,jsonParser, epfCore.ping);
app.get ('/api/tx/employer/:id'       ,jsonParser, epfPayroll.getEmployer);
app.post('/api/tx/employer/:id'       ,jsonParser, epfPayroll.setEmployer);

app.get('/api/push/getVapidPublicKey',handler('[PUSH]\t'),jsonParser,push.vapidPublicKey,);
app.post('/api/push/register'        ,handler('[PUSH]\t'),jsonParser,push.register((s)=>console.log(s)));
app.post('/api/push/unregister'      ,handler('[PUSH]\t'),jsonParser,push.unregister((s)=>console.log(s)));
//app.get('/api/tx/push/single'           ,push.pushSingle);
//app.get('/api/tx/push/all'              ,push.pushAll);


app.get('/test/*', (req, res) => {
  res.handler = '[TEST]\t'
  const resolvedPath = path.join(__dirname + path_prefix, 'build',  req.path[req.path.length - 1]==='/' ? `${req.path}index.html` : req.path)
  //console.log(resolvedPath)
  res.sendFile(resolvedPath);
});

app.get('/*', (req, res) => {
  res.handler = '[APP]\t'
  const resolvedPath = path.join(__dirname + path_prefix, 'build/app', 'index.html')
  //console.log(resolvedPath)
  res.sendFile(resolvedPath);
}); 

// error handling
app.use(function(err, req, res, next){
  console.error(err.stack);
  res.handler = '[CATCH]\t'
  res.status(500).send('Something bad happened!');
});

app.listen(port, ip);
console.log('Server running on http://%s:%s', ip, port);

module.exports = app;
