// Listen to `push` notification event. Define the text to be displayed
// and show the notification.
self.addEventListener('push', function (event) {
  const payload = JSON.parse(event.data ? event.data.text() : `{
    title : 'No title',
    message : { body : 'No message' }
  }`);
  console.log(payload)

  event.waitUntil(self.registration.showNotification(payload.title, payload.options));
});
self.addEventListener('notificationclick', event => {
  console.log('Notification click event', event);
});

// Listen to  `pushsubscriptionchange` event which is fired when
// subscription expires. Subscribe again and register the new subscription
// in the server by sending a POST request with endpoint. Real world
// application would probably use also user identification.
self.addEventListener('pushsubscriptionchange', function (event) {
  console.log('Subscription expired');
  event.waitUntil(
    self.registration.pushManager.subscribe({ userVisibleOnly: true })
      .then(function (subscription) {
        console.log('Subscribed after expiration', subscription.endpoint);
        return fetch('register', {
          method: 'post',
          headers: {
            'Content-type': 'application/json'
          },
          body: JSON.stringify({
            endpoint: subscription.endpoint
          })
        });
      })
  );
});