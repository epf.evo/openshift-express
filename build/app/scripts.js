if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('service-worker.js')
        .then(function (registration) {
            console.log('Service Worker registration successful with scope: ', registration.scope);
        }, function (err) {
            console.log('Service Worker registration failed: ', err);
        });

    navigator.serviceWorker.ready
        .then(function (registration) {
            console.log('Service Worker ready');
            return registration
        })
        .then(function (registration){
            registration.update();
            return registration
        })
        .then(function (registration) {
            if (!('Notification' in window)) {
                console.log('This browser does not support notifications!');
                return registration;
            }
            Notification.requestPermission(status => {
                console.log('Notification permission status:', status);
                if (status === 'granted') {
                    pushSubscribe()
                }
            });
        });

    let urlBase64ToUint8Array = function (base64String) {
        var padding = '='.repeat((4 - base64String.length % 4) % 4);
        var base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

        var rawData = window.atob(base64);
        var outputArray = new Uint8Array(rawData.length);

        for (var i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }
    // Get the `registration` from service worker and create a new
    // subscription using `registration.pushManager.subscribe`. Then
    // register received new subscription by sending a POST request with
    // the subscription to the server.
    let pushSubscribe = function () {
        navigator.serviceWorker.ready
            .then(async function (registration) {
                // Get the server's public key
                const response = await fetch('/api/push/getVapidPublicKey');
                const vapidPublicKey = await response.text();
                // Chrome doesn't accept the base64-encoded (string) vapidPublicKey yet
                // urlBase64ToUint8Array() is defined in /tools.js
                const convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);
                // Subscribe the user
                return registration.pushManager.subscribe({
                    userVisibleOnly: true,
                    applicationServerKey: convertedVapidKey
                });
            }).then(function (subscription) {
                console.log('Subscribed', subscription.endpoint);
                return fetch('/api/push/register', {
                    method: 'post',
                    headers: {
                        'Content-type': 'application/json'
                    },
                    body: JSON.stringify({
                        subscription: subscription
                    })
                });
            })//.then(setUnsubscribeButton);
    }

    // Get existing subscription from service worker, unsubscribe
    // (`subscription.unsubscribe()`) and unregister it in the server with
    // a POST request to stop sending push messages to
    // unexisting endpoint.
    let pushUnsubscribe = function () {
        navigator.serviceWorker.ready
            .then(function (registration) {
                return registration.pushManager.getSubscription();
            }).then(function (subscription) {
                return subscription.unsubscribe()
                    .then(function () {
                        console.log('Unsubscribed', subscription.endpoint);
                        return fetch('/api/push/unregister', {
                            method: 'post',
                            headers: {
                                'Content-type': 'application/json'
                            },
                            body: JSON.stringify({
                                subscription: subscription
                            })
                        });
                    });
            })//.then(setSubscribeButton);
    }
}


;
//# sourceMappingURL=scripts.js.map