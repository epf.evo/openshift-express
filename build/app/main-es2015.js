(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./account/account.module": "./src/app/account/account.module.ts",
	"./dashboard/dashboard.module": "./src/app/dashboard/dashboard.module.ts",
	"./employees/employees.module": "./src/app/employees/employees.module.ts",
	"./payroll/payroll.module": "./src/app/payroll/payroll.module.ts",
	"./transactions/transactions.module": "./src/app/transactions/transactions.module.ts"
};

function webpackAsyncContext(req) {
	return Promise.resolve().then(function() {
		if(!__webpack_require__.o(map, req)) {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		}

		var id = map[req];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/account-addusers/account-addusers.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/account-addusers/account-addusers.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  account-addusers works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/account-bank/account-bank.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/account-bank/account-bank.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  account-bank works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/account-dda/account-dda.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/account-dda/account-dda.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  account-dda works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/account-fpx/account-fpx.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/account-fpx/account-fpx.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  account-fpx works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/account-overview/account-overview.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/account-overview/account-overview.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\">\r\n  <epf-page-title>Settings</epf-page-title>\r\n</section>\r\n\r\n<section class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <p>\r\n        account-overview works!\r\n      </p>\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </div>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/account-payment/account-payment.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/account-payment/account-payment.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  account-payment works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/account-salarytransfer/account-salarytransfer.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/account-salarytransfer/account-salarytransfer.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  account-salarytransfer works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/account-settings/account-settings.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/account-settings/account-settings.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  account-settings works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/account/account-users/account-users.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/account/account-users/account-users.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  account-users works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div\r\n  class=\"epf-layout {{ fontsizeclass }}\"\r\n  [class.toggleNav]=\"toggleClass == true\"\r\n>\r\n  <header class=\"epf-layout__header\">\r\n    <epf-header\r\n      (sizeClass)=\"getClass($event)\"\r\n      (toggleNav)=\"getToggle($event)\"\r\n    ></epf-header>\r\n  </header>\r\n\r\n  <nav class=\"epf-layout__sidenav\">\r\n    <epf-navigation [class.toggleNav]=\"toggleClass == true\"></epf-navigation>\r\n  </nav>\r\n\r\n  <div class=\"epf-layout__body\">\r\n    <div class=\"epf-layout__body__content\">\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n    <div class=\"epf-layout__body__footer\">\r\n      <epf-footer></epf-footer>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/home/home.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/home/home.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\">\r\n  <epf-page-title>Payroll Dashboard</epf-page-title>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/optin/optin.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/optin/optin.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\">\r\n  <epf-page-title>Payroll Home</epf-page-title>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/employees/employee-add-details/employee-add-details.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/employees/employee-add-details/employee-add-details.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  employee-add-details works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/employees/employee-add/employee-add.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/employees/employee-add/employee-add.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  employee-add works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/employees/employee-edit/employee-edit.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/employees/employee-edit/employee-edit.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  employee-edit works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/employees/employee-overview/employee-overview.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/employees/employee-overview/employee-overview.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\">\r\n  <epf-page-title>Employees (0)</epf-page-title>\r\n</section>\r\n<section class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <epf-table [prop]=\"table_opts\"> </epf-table>\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </div>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/employees/employee-search/employee-search.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/employees/employee-search/employee-search.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  employee-search works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/error-page/error-page.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/error-page/error-page.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <p>\r\n        error-page works!\r\n      </p>\r\n    </div>\r\n  </div>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/footer/footer.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/footer/footer.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <p>\r\n        &copy; KWSP | EPF 2019. All rights reserved.\r\n      </p>\r\n    </div>\r\n  </div>\r\n</footer>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/header/fontsize-toggle/fontsize-toggle.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/header/fontsize-toggle/fontsize-toggle.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button (click)=\"decrement(counter)\">A-</button>\r\n<button (click)=\"increment(counter)\">A+</button>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/header/header.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/header/header.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"epf-header\" [class.toggleNav]=\"togglestate == true\">\r\n  <div class=\"epf-header__logo\">\r\n    <div class=\"epf-logo\">\r\n      <img src=\"assets/images/epf_logo.png\" />\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"epf-header__title\">\r\n    <span class=\"icon icon-menu\" (click)=\"sendingToggleNav()\"></span>\r\n    <p>\r\n      <strong>{{ optin.employer.companyName }}</strong>\r\n      <span class=\"divider\">|</span>\r\n      No. of KWSP: {{ optin.employer.employerEpfNum }}\r\n    </p>\r\n  </div>\r\n\r\n  <div class=\"epf-header__actions\">\r\n    <epf-language-toggle></epf-language-toggle>\r\n    <ul>\r\n      <li>\r\n        <p>\r\n          <span class=\"icon-settings\"></span>\r\n        </p>\r\n      </li>\r\n      <li>\r\n        <p>\r\n          <span class=\"icon-messsage\"></span>\r\n        </p>\r\n      </li>\r\n      <li>\r\n        <p>\r\n          <span class=\"icon-notifications\"></span>\r\n        </p>\r\n      </li>\r\n      <li>\r\n        <p>\r\n          <span class=\"icon-enquiries\"></span>\r\n        </p>\r\n      </li>\r\n      <li>\r\n        <p>\r\n          <span class=\"icon-logout\"></span>\r\n        </p>\r\n      </li>\r\n    </ul>\r\n    <!-- <epf-fontsize-toggle (sizeChange)=\"getSize($event)\"></epf-fontsize-toggle> -->\r\n  </div>\r\n\r\n  <epf-routing-spinner *ngIf=\"loading\"></epf-routing-spinner>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/header/language-toggle/language-toggle.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/header/language-toggle/language-toggle.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"epf-languagetoggle\">\r\n  <span class=\"languagebtn active\" (click)=\"useLanguage('en')\">en</span>\r\n  <span class=\"divider\">|</span>\r\n  <span class=\"languagebtn\" (click)=\"useLanguage('bm')\">bm</span>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/navigation/navigation.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/navigation/navigation.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"epf-navigation\">\r\n  <ul>\r\n    <li routerLinkActive=\"active\">\r\n      <p>\r\n        <a [routerLink]=\"['home']\">\r\n          <span class=\"icon icon-home\"></span>\r\n          <span class=\"text\">{{ \"navigation.dashboard\" | translate }}</span>\r\n        </a>\r\n      </p>\r\n    </li>\r\n    <li routerLinkActive=\"active\">\r\n      <p>\r\n        <a [routerLink]=\"['payroll']\">\r\n          <span class=\"icon icon-payroll\"></span>\r\n          <span class=\"text\">{{ \"navigation.payroll\" | translate }}</span>\r\n        </a>\r\n      </p>\r\n    </li>\r\n    <li routerLinkActive=\"active\">\r\n      <p>\r\n        <a [routerLink]=\"['employees']\">\r\n          <span class=\"icon icon-employees\"></span>\r\n          <span class=\"text\">{{ \"navigation.emoployees\" | translate }}</span>\r\n        </a>\r\n      </p>\r\n    </li>\r\n    <li routerLinkActive=\"active\">\r\n      <p>\r\n        <a [routerLink]=\"['transactions']\">\r\n          <span class=\"icon icon-transactions\"></span>\r\n          <span class=\"text\">{{ \"navigation.transactions\" | translate }}</span>\r\n        </a>\r\n      </p>\r\n    </li>\r\n    <li routerLinkActive=\"active\">\r\n      <p>\r\n        <a [routerLink]=\"['settings']\">\r\n          <span class=\"icon icon-settings\"></span>\r\n          <span class=\"text\">{{ \"navigation.settings\" | translate }}</span>\r\n        </a>\r\n      </p>\r\n    </li>\r\n  </ul>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layouts/routing-spinner/routing-spinner.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layouts/routing-spinner/routing-spinner.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>laoding...</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/payroll/payroll-edit/payroll-edit.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/payroll/payroll-edit/payroll-edit.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  payroll-edit works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/payroll/payroll-overview/payroll-overview.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/payroll/payroll-overview/payroll-overview.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\">\r\n  <epf-page-title>Payroll</epf-page-title>\r\n</section>\r\n\r\n<section class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <epf-table [prop]=\"table_opts\">\r\n        <caption>\r\n          <b>This is in bold</b>\r\n          and this is not\r\n        </caption>\r\n      </epf-table>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/payroll/payroll-payment/payroll-payment.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/payroll/payroll-payment/payroll-payment.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  payroll-payment works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/epf-fields-buttons.component.html":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/epf-fields-buttons.component.html ***!
  \************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<epf-fields-wrapper>\r\n  <div class=\"epf-fields-button\">\r\n    <ng-content></ng-content>\r\n  </div>\r\n</epf-fields-wrapper>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/epf-fields-dropdowns.component.html":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/epf-fields-dropdowns.component.html ***!
  \****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<epf-fields-wrapper>\r\n  <div class=\"epf-fields-wrapper__dropdown\">\r\n    <epf-fields-label [labelInput]=\"labelvalue\"></epf-fields-label>\r\n    <ng-content></ng-content>\r\n  </div>\r\n</epf-fields-wrapper>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/epf-fields-inputs.component.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/epf-fields-inputs.component.html ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<epf-fields-wrapper>\r\n  <div class=\"epf-fields-wrapper__input\">\r\n    <epf-fields-label [labelInput]=\"labelvalue\"></epf-fields-label>\r\n    <ng-content></ng-content>\r\n  </div>\r\n</epf-fields-wrapper>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/epf-fields-textarea.component.html":
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/epf-fields-textarea.component.html ***!
  \**************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<epf-fields-wrapper>\r\n  <epf-fields-label [labelInput]=\"labelvalue\"></epf-fields-label>\r\n  <ng-content></ng-content>\r\n</epf-fields-wrapper>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/epf-fields-toggles.component.html":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/epf-fields-toggles.component.html ***!
  \************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<epf-fields-wrapper>\r\n  <epf-fields-label></epf-fields-label>\r\n  <ng-content></ng-content>\r\n</epf-fields-wrapper>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/epf-fields-error.component.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/epf-fields-error.component.html ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"epf-fields-error\" [class.hide]=\"_hide\">\r\n  {{ _text }}\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/epf-fields-label.component.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/epf-fields-label.component.html ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"epf-fields-wrapper__label\">\r\n  <label>{{ label }}</label>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/epf-fields-wrapper.component.html":
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/epf-fields-wrapper.component.html ***!
  \**************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"epf-fields-wrapper\">\r\n  <ng-content></ng-content>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/epf-table/epf-table/epf-table.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/epf-table/epf-table/epf-table.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"epf-table_wrapper\">\r\n  <div class=\"epf-table_wrapper__header\">\r\n    <div class=\"epf-table_wrapper__title\">\r\n      <ng-content select=\"caption\"></ng-content>\r\n    </div>\r\n    <div class=\"epf-table_wrapper__search\" *ngIf=\"property.searchable\">\r\n      <mat-form-field floatLabel=\"never\">\r\n        <mat-label\r\n          >Search\r\n          <span *ngIf=\"_searchbycol\">by {{ _searchby }}</span></mat-label\r\n        >\r\n        <input\r\n          matInput\r\n          (keyup)=\"applyFilter($event.target.value)\"\r\n          placeholder=\"Filter\"\r\n        />\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"epf-table_wrapper__table\" *ngIf=\"!property.expandable\">\r\n    <table mat-table [dataSource]=\"dataSource\" matSort>\r\n      <!-- <ng-container matColumnDef=\"select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n          <mat-checkbox (change)=\"$event ? masterToggle() : null\" [checked]=\"selection.hasValue() && isAllSelected()\" [indeterminate]=\"selection.hasValue() && !isAllSelected()\"\r\n            [aria-label]=\"checkboxLabel()\">\r\n          </mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n          <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"$event ? selection.toggle(row) : null\" [checked]=\"selection.isSelected(row)\"\r\n            [aria-label]=\"checkboxLabel(row)\">\r\n          </mat-checkbox>\r\n        </td>\r\n      </ng-container> -->\r\n\r\n      <ng-container\r\n        [matColumnDef]=\"column\"\r\n        *ngFor=\"let column of displayedColumns\"\r\n      >\r\n        <div *ngIf=\"property.sortable\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            {{ columnNames[column] || column }}\r\n          </th>\r\n        </div>\r\n        <div *ngIf=\"!property.sortable\">\r\n          <th mat-header-cell *matHeaderCellDef>\r\n            {{ columnNames[column] || column }}\r\n          </th>\r\n        </div>\r\n\r\n        <td\r\n          mat-cell\r\n          *matCellDef=\"let element\"\r\n          [attr.data-label]=\"columnNames[column]\"\r\n        >\r\n          <div\r\n            *ngIf=\"columnTypes[column]; else nonRinggit\"\r\n            [innerHtml]=\"element[column] | ringgit\"\r\n          ></div>\r\n          <ng-template #nonRinggit>{{ element[column] }}</ng-template>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns\"></tr>\r\n    </table>\r\n  </div>\r\n\r\n  <div\r\n    class=\"epf-table_wrapper__table table_expandable\"\r\n    *ngIf=\"property.expandable\"\r\n  >\r\n    <table mat-table [dataSource]=\"dataSource\" matSort multiTemplateDataRows>\r\n      <ng-container\r\n        [matColumnDef]=\"column\"\r\n        *ngFor=\"let column of displayedColumns\"\r\n      >\r\n        <div *ngIf=\"property.sortable\">\r\n          <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n            {{ columnNames[column] || column }}\r\n          </th>\r\n        </div>\r\n        <div *ngIf=\"!property.sortable\">\r\n          <th mat-header-cell *matHeaderCellDef>\r\n            {{ columnNames[column] || column }}\r\n          </th>\r\n        </div>\r\n\r\n        <td\r\n          mat-cell\r\n          *matCellDef=\"let element\"\r\n          [attr.data-label]=\"columnNames[column]\"\r\n        >\r\n          <div\r\n            *ngIf=\"columnTypes[column]; else nonRinggit\"\r\n            [innerHtml]=\"element[column] | ringgit\"\r\n          ></div>\r\n          <ng-template #nonRinggit>{{ element[column] }}</ng-template>\r\n        </td>\r\n      </ng-container>\r\n\r\n      <ng-container matColumnDef=\"expandedDetail\">\r\n        <td\r\n          mat-cell\r\n          *matCellDef=\"let element\"\r\n          [attr.colspan]=\"displayedColumns.length\"\r\n          style=\"overflow: hidden\"\r\n          [@detailExpand]=\"\r\n            element == expandedElement ? 'expanded' : 'collapsed'\r\n          \"\r\n          [innerHTML]=\"getHTML(table_expandable(element))\"\r\n        ></td>\r\n      </ng-container>\r\n\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr\r\n        mat-row\r\n        *matRowDef=\"let element; columns: displayedColumns\"\r\n        [class.expanded]=\"expandedElement === element\"\r\n        (click)=\"expandedElement = expandedElement === element ? null : element\"\r\n      ></tr>\r\n      <tr mat-row *matRowDef=\"let row; columns: ['expandedDetail']\"></tr>\r\n    </table>\r\n  </div>\r\n\r\n  <ng-container *ngIf=\"!dataSource.filteredData.length\">\r\n    <p>No records found</p>\r\n  </ng-container>\r\n\r\n  <div class=\"epf-table_wrapper__pagination\" *ngIf=\"property.pagination\">\r\n    <mat-paginator [pageSize]=\"5\" [pageSizeOptions]=\"[5, 10, 15]\">\r\n    </mat-paginator>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/page-title/page-title.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/page-title/page-title.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row epf-page-title\">\r\n  <div class=\"col-md-12\">\r\n    <h1>\r\n      <ng-content></ng-content>\r\n    </h1>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/transactions/transaction-agency/transaction-agency.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/transactions/transaction-agency/transaction-agency.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  transaction-agency works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/transactions/transaction-overview/transaction-overview.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/transactions/transaction-overview/transaction-overview.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"container-fluid\">\r\n  <epf-page-title>Transactions</epf-page-title>\r\n</section>\r\n<section class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <p>\r\n        transaction-overview works!\r\n      </p>\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </div>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/transactions/transaction-record/transaction-record.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/transactions/transaction-record/transaction-record.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  transaction-record works!\r\n</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/transactions/transaction-slips/transaction-slips.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/transactions/transaction-slips/transaction-slips.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  transaction-slips works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/account/account-addusers/account-addusers.component.sass":
/*!**************************************************************************!*\
  !*** ./src/app/account/account-addusers/account-addusers.component.sass ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC1hZGR1c2Vycy9hY2NvdW50LWFkZHVzZXJzLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/account/account-addusers/account-addusers.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/account/account-addusers/account-addusers.component.ts ***!
  \************************************************************************/
/*! exports provided: AccountAddusersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountAddusersComponent", function() { return AccountAddusersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AccountAddusersComponent = class AccountAddusersComponent {
    constructor() { }
    ngOnInit() {
    }
};
AccountAddusersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-account-addusers',
        template: __webpack_require__(/*! raw-loader!./account-addusers.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/account-addusers/account-addusers.component.html"),
        styles: [__webpack_require__(/*! ./account-addusers.component.sass */ "./src/app/account/account-addusers/account-addusers.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AccountAddusersComponent);



/***/ }),

/***/ "./src/app/account/account-addusers/index.ts":
/*!***************************************************!*\
  !*** ./src/app/account/account-addusers/index.ts ***!
  \***************************************************/
/*! exports provided: AccountAddusersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_addusers_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-addusers.component */ "./src/app/account/account-addusers/account-addusers.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountAddusersComponent", function() { return _account_addusers_component__WEBPACK_IMPORTED_MODULE_0__["AccountAddusersComponent"]; });




/***/ }),

/***/ "./src/app/account/account-bank/account-bank.component.sass":
/*!******************************************************************!*\
  !*** ./src/app/account/account-bank/account-bank.component.sass ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC1iYW5rL2FjY291bnQtYmFuay5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/account/account-bank/account-bank.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/account/account-bank/account-bank.component.ts ***!
  \****************************************************************/
/*! exports provided: AccountBankComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountBankComponent", function() { return AccountBankComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AccountBankComponent = class AccountBankComponent {
    constructor() { }
    ngOnInit() {
    }
};
AccountBankComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-account-bank',
        template: __webpack_require__(/*! raw-loader!./account-bank.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/account-bank/account-bank.component.html"),
        styles: [__webpack_require__(/*! ./account-bank.component.sass */ "./src/app/account/account-bank/account-bank.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AccountBankComponent);



/***/ }),

/***/ "./src/app/account/account-bank/index.ts":
/*!***********************************************!*\
  !*** ./src/app/account/account-bank/index.ts ***!
  \***********************************************/
/*! exports provided: AccountBankComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_bank_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-bank.component */ "./src/app/account/account-bank/account-bank.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountBankComponent", function() { return _account_bank_component__WEBPACK_IMPORTED_MODULE_0__["AccountBankComponent"]; });




/***/ }),

/***/ "./src/app/account/account-dda/account-dda.component.sass":
/*!****************************************************************!*\
  !*** ./src/app/account/account-dda/account-dda.component.sass ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC1kZGEvYWNjb3VudC1kZGEuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/account/account-dda/account-dda.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/account/account-dda/account-dda.component.ts ***!
  \**************************************************************/
/*! exports provided: AccountDdaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountDdaComponent", function() { return AccountDdaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AccountDdaComponent = class AccountDdaComponent {
    constructor() { }
    ngOnInit() {
    }
};
AccountDdaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-account-dda',
        template: __webpack_require__(/*! raw-loader!./account-dda.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/account-dda/account-dda.component.html"),
        styles: [__webpack_require__(/*! ./account-dda.component.sass */ "./src/app/account/account-dda/account-dda.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AccountDdaComponent);



/***/ }),

/***/ "./src/app/account/account-dda/index.ts":
/*!**********************************************!*\
  !*** ./src/app/account/account-dda/index.ts ***!
  \**********************************************/
/*! exports provided: AccountDdaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_dda_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-dda.component */ "./src/app/account/account-dda/account-dda.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountDdaComponent", function() { return _account_dda_component__WEBPACK_IMPORTED_MODULE_0__["AccountDdaComponent"]; });




/***/ }),

/***/ "./src/app/account/account-fpx/account-fpx.component.sass":
/*!****************************************************************!*\
  !*** ./src/app/account/account-fpx/account-fpx.component.sass ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC1mcHgvYWNjb3VudC1mcHguY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/account/account-fpx/account-fpx.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/account/account-fpx/account-fpx.component.ts ***!
  \**************************************************************/
/*! exports provided: AccountFpxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountFpxComponent", function() { return AccountFpxComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AccountFpxComponent = class AccountFpxComponent {
    constructor() { }
    ngOnInit() {
    }
};
AccountFpxComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-account-fpx',
        template: __webpack_require__(/*! raw-loader!./account-fpx.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/account-fpx/account-fpx.component.html"),
        styles: [__webpack_require__(/*! ./account-fpx.component.sass */ "./src/app/account/account-fpx/account-fpx.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AccountFpxComponent);



/***/ }),

/***/ "./src/app/account/account-fpx/index.ts":
/*!**********************************************!*\
  !*** ./src/app/account/account-fpx/index.ts ***!
  \**********************************************/
/*! exports provided: AccountFpxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_fpx_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-fpx.component */ "./src/app/account/account-fpx/account-fpx.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountFpxComponent", function() { return _account_fpx_component__WEBPACK_IMPORTED_MODULE_0__["AccountFpxComponent"]; });




/***/ }),

/***/ "./src/app/account/account-overview/account-overview.component.sass":
/*!**************************************************************************!*\
  !*** ./src/app/account/account-overview/account-overview.component.sass ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC1vdmVydmlldy9hY2NvdW50LW92ZXJ2aWV3LmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/account/account-overview/account-overview.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/account/account-overview/account-overview.component.ts ***!
  \************************************************************************/
/*! exports provided: AccountOverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountOverviewComponent", function() { return AccountOverviewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AccountOverviewComponent = class AccountOverviewComponent {
    constructor() { }
    ngOnInit() {
        let value = 'Test masterdata request';
        fetch(`/api/md/ping?value=${encodeURIComponent(value)}`)
            .then(r => r.json())
            .then(r => console.log('Dummy fetch request masterdata', r));
        fetch('/api/tx/ping', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                value: 'Test transaction request'
            })
        })
            .then(r => r.json())
            .then(r => console.log('Dummy fetch request transaction', r));
    }
};
AccountOverviewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-account-overview',
        template: __webpack_require__(/*! raw-loader!./account-overview.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/account-overview/account-overview.component.html"),
        styles: [__webpack_require__(/*! ./account-overview.component.sass */ "./src/app/account/account-overview/account-overview.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AccountOverviewComponent);



/***/ }),

/***/ "./src/app/account/account-overview/index.ts":
/*!***************************************************!*\
  !*** ./src/app/account/account-overview/index.ts ***!
  \***************************************************/
/*! exports provided: AccountOverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_overview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-overview.component */ "./src/app/account/account-overview/account-overview.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountOverviewComponent", function() { return _account_overview_component__WEBPACK_IMPORTED_MODULE_0__["AccountOverviewComponent"]; });




/***/ }),

/***/ "./src/app/account/account-payment/account-payment.component.sass":
/*!************************************************************************!*\
  !*** ./src/app/account/account-payment/account-payment.component.sass ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC1wYXltZW50L2FjY291bnQtcGF5bWVudC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/account/account-payment/account-payment.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/account/account-payment/account-payment.component.ts ***!
  \**********************************************************************/
/*! exports provided: AccountPaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountPaymentComponent", function() { return AccountPaymentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AccountPaymentComponent = class AccountPaymentComponent {
    constructor() { }
    ngOnInit() {
    }
};
AccountPaymentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-account-payment',
        template: __webpack_require__(/*! raw-loader!./account-payment.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/account-payment/account-payment.component.html"),
        styles: [__webpack_require__(/*! ./account-payment.component.sass */ "./src/app/account/account-payment/account-payment.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AccountPaymentComponent);



/***/ }),

/***/ "./src/app/account/account-payment/index.ts":
/*!**************************************************!*\
  !*** ./src/app/account/account-payment/index.ts ***!
  \**************************************************/
/*! exports provided: AccountPaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_payment_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-payment.component */ "./src/app/account/account-payment/account-payment.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountPaymentComponent", function() { return _account_payment_component__WEBPACK_IMPORTED_MODULE_0__["AccountPaymentComponent"]; });




/***/ }),

/***/ "./src/app/account/account-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/account/account-routing.module.ts ***!
  \***************************************************/
/*! exports provided: AccountRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountRoutingModule", function() { return AccountRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _account_overview__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./account-overview */ "./src/app/account/account-overview/index.ts");
/* harmony import */ var _account_users__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./account-users */ "./src/app/account/account-users/index.ts");
/* harmony import */ var _account_payment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./account-payment */ "./src/app/account/account-payment/index.ts");
/* harmony import */ var _account_settings__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./account-settings */ "./src/app/account/account-settings/index.ts");
/* harmony import */ var _account_addusers__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./account-addusers */ "./src/app/account/account-addusers/index.ts");
/* harmony import */ var _account_fpx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./account-fpx */ "./src/app/account/account-fpx/index.ts");
/* harmony import */ var _account_dda__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./account-dda */ "./src/app/account/account-dda/index.ts");
/* harmony import */ var _account_bank__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./account-bank */ "./src/app/account/account-bank/index.ts");
/* harmony import */ var _account_salarytransfer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./account-salarytransfer */ "./src/app/account/account-salarytransfer/index.ts");












const routes = [
    {
        path: "",
        component: _account_overview__WEBPACK_IMPORTED_MODULE_3__["AccountOverviewComponent"],
        children: [
            {
                path: "users",
                component: _account_users__WEBPACK_IMPORTED_MODULE_4__["AccountUsersComponent"],
                children: [
                    {
                        path: "add",
                        component: _account_addusers__WEBPACK_IMPORTED_MODULE_7__["AccountAddusersComponent"]
                    }
                ]
            },
            {
                path: "payment",
                component: _account_payment__WEBPACK_IMPORTED_MODULE_5__["AccountPaymentComponent"],
                children: [
                    {
                        path: "fpx",
                        component: _account_fpx__WEBPACK_IMPORTED_MODULE_8__["AccountFpxComponent"]
                    },
                    {
                        path: "dda",
                        component: _account_dda__WEBPACK_IMPORTED_MODULE_9__["AccountDdaComponent"]
                    },
                    {
                        path: "bank",
                        component: _account_bank__WEBPACK_IMPORTED_MODULE_10__["AccountBankComponent"]
                    },
                    {
                        path: "transfer",
                        component: _account_salarytransfer__WEBPACK_IMPORTED_MODULE_11__["AccountSalarytransferComponent"]
                    }
                ]
            },
            {
                path: "statutory",
                component: _account_settings__WEBPACK_IMPORTED_MODULE_6__["AccountSettingsComponent"]
            }
        ]
    }
];
let AccountRoutingModule = class AccountRoutingModule {
};
AccountRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AccountRoutingModule);



/***/ }),

/***/ "./src/app/account/account-salarytransfer/account-salarytransfer.component.sass":
/*!**************************************************************************************!*\
  !*** ./src/app/account/account-salarytransfer/account-salarytransfer.component.sass ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC1zYWxhcnl0cmFuc2Zlci9hY2NvdW50LXNhbGFyeXRyYW5zZmVyLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/account/account-salarytransfer/account-salarytransfer.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/account/account-salarytransfer/account-salarytransfer.component.ts ***!
  \************************************************************************************/
/*! exports provided: AccountSalarytransferComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountSalarytransferComponent", function() { return AccountSalarytransferComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AccountSalarytransferComponent = class AccountSalarytransferComponent {
    constructor() { }
    ngOnInit() {
    }
};
AccountSalarytransferComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-account-salarytransfer',
        template: __webpack_require__(/*! raw-loader!./account-salarytransfer.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/account-salarytransfer/account-salarytransfer.component.html"),
        styles: [__webpack_require__(/*! ./account-salarytransfer.component.sass */ "./src/app/account/account-salarytransfer/account-salarytransfer.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AccountSalarytransferComponent);



/***/ }),

/***/ "./src/app/account/account-salarytransfer/index.ts":
/*!*********************************************************!*\
  !*** ./src/app/account/account-salarytransfer/index.ts ***!
  \*********************************************************/
/*! exports provided: AccountSalarytransferComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_salarytransfer_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-salarytransfer.component */ "./src/app/account/account-salarytransfer/account-salarytransfer.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountSalarytransferComponent", function() { return _account_salarytransfer_component__WEBPACK_IMPORTED_MODULE_0__["AccountSalarytransferComponent"]; });




/***/ }),

/***/ "./src/app/account/account-settings/account-settings.component.sass":
/*!**************************************************************************!*\
  !*** ./src/app/account/account-settings/account-settings.component.sass ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC1zZXR0aW5ncy9hY2NvdW50LXNldHRpbmdzLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/account/account-settings/account-settings.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/account/account-settings/account-settings.component.ts ***!
  \************************************************************************/
/*! exports provided: AccountSettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountSettingsComponent", function() { return AccountSettingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AccountSettingsComponent = class AccountSettingsComponent {
    constructor() { }
    ngOnInit() {
    }
};
AccountSettingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-account-settings',
        template: __webpack_require__(/*! raw-loader!./account-settings.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/account-settings/account-settings.component.html"),
        styles: [__webpack_require__(/*! ./account-settings.component.sass */ "./src/app/account/account-settings/account-settings.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AccountSettingsComponent);



/***/ }),

/***/ "./src/app/account/account-settings/index.ts":
/*!***************************************************!*\
  !*** ./src/app/account/account-settings/index.ts ***!
  \***************************************************/
/*! exports provided: AccountSettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_settings_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-settings.component */ "./src/app/account/account-settings/account-settings.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountSettingsComponent", function() { return _account_settings_component__WEBPACK_IMPORTED_MODULE_0__["AccountSettingsComponent"]; });




/***/ }),

/***/ "./src/app/account/account-users/account-users.component.sass":
/*!********************************************************************!*\
  !*** ./src/app/account/account-users/account-users.component.sass ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjY291bnQvYWNjb3VudC11c2Vycy9hY2NvdW50LXVzZXJzLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/account/account-users/account-users.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/account/account-users/account-users.component.ts ***!
  \******************************************************************/
/*! exports provided: AccountUsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountUsersComponent", function() { return AccountUsersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AccountUsersComponent = class AccountUsersComponent {
    constructor() { }
    ngOnInit() {
    }
};
AccountUsersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-account-users',
        template: __webpack_require__(/*! raw-loader!./account-users.component.html */ "./node_modules/raw-loader/index.js!./src/app/account/account-users/account-users.component.html"),
        styles: [__webpack_require__(/*! ./account-users.component.sass */ "./src/app/account/account-users/account-users.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AccountUsersComponent);



/***/ }),

/***/ "./src/app/account/account-users/index.ts":
/*!************************************************!*\
  !*** ./src/app/account/account-users/index.ts ***!
  \************************************************/
/*! exports provided: AccountUsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_users_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-users.component */ "./src/app/account/account-users/account-users.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountUsersComponent", function() { return _account_users_component__WEBPACK_IMPORTED_MODULE_0__["AccountUsersComponent"]; });




/***/ }),

/***/ "./src/app/account/account.module.ts":
/*!*******************************************!*\
  !*** ./src/app/account/account.module.ts ***!
  \*******************************************/
/*! exports provided: AccountModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountModule", function() { return AccountModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _account_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./account-routing.module */ "./src/app/account/account-routing.module.ts");
/* harmony import */ var _account_overview__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./account-overview */ "./src/app/account/account-overview/index.ts");
/* harmony import */ var _account_users__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./account-users */ "./src/app/account/account-users/index.ts");
/* harmony import */ var _account_payment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./account-payment */ "./src/app/account/account-payment/index.ts");
/* harmony import */ var _account_settings__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./account-settings */ "./src/app/account/account-settings/index.ts");
/* harmony import */ var _account_addusers__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./account-addusers */ "./src/app/account/account-addusers/index.ts");
/* harmony import */ var _account_fpx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./account-fpx */ "./src/app/account/account-fpx/index.ts");
/* harmony import */ var _account_dda__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./account-dda */ "./src/app/account/account-dda/index.ts");
/* harmony import */ var _account_bank__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./account-bank */ "./src/app/account/account-bank/index.ts");
/* harmony import */ var _account_salarytransfer__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./account-salarytransfer */ "./src/app/account/account-salarytransfer/index.ts");














let AccountModule = class AccountModule {
};
AccountModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _account_overview__WEBPACK_IMPORTED_MODULE_5__["AccountOverviewComponent"],
            _account_users__WEBPACK_IMPORTED_MODULE_6__["AccountUsersComponent"],
            _account_payment__WEBPACK_IMPORTED_MODULE_7__["AccountPaymentComponent"],
            _account_settings__WEBPACK_IMPORTED_MODULE_8__["AccountSettingsComponent"],
            _account_addusers__WEBPACK_IMPORTED_MODULE_9__["AccountAddusersComponent"],
            _account_fpx__WEBPACK_IMPORTED_MODULE_10__["AccountFpxComponent"],
            _account_dda__WEBPACK_IMPORTED_MODULE_11__["AccountDdaComponent"],
            _account_bank__WEBPACK_IMPORTED_MODULE_12__["AccountBankComponent"],
            _account_salarytransfer__WEBPACK_IMPORTED_MODULE_13__["AccountSalarytransferComponent"]
        ],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _account_routing_module__WEBPACK_IMPORTED_MODULE_4__["AccountRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]],
        exports: [
            _account_overview__WEBPACK_IMPORTED_MODULE_5__["AccountOverviewComponent"],
            _account_users__WEBPACK_IMPORTED_MODULE_6__["AccountUsersComponent"],
            _account_payment__WEBPACK_IMPORTED_MODULE_7__["AccountPaymentComponent"],
            _account_settings__WEBPACK_IMPORTED_MODULE_8__["AccountSettingsComponent"],
            _account_addusers__WEBPACK_IMPORTED_MODULE_9__["AccountAddusersComponent"],
            _account_fpx__WEBPACK_IMPORTED_MODULE_10__["AccountFpxComponent"],
            _account_dda__WEBPACK_IMPORTED_MODULE_11__["AccountDdaComponent"],
            _account_bank__WEBPACK_IMPORTED_MODULE_12__["AccountBankComponent"],
            _account_salarytransfer__WEBPACK_IMPORTED_MODULE_13__["AccountSalarytransferComponent"]
        ]
    })
], AccountModule);



/***/ }),

/***/ "./src/app/account/index.ts":
/*!**********************************!*\
  !*** ./src/app/account/index.ts ***!
  \**********************************/
/*! exports provided: AccountModule, AccountOverviewComponent, AccountUsersComponent, AccountPaymentComponent, AccountSettingsComponent, AccountAddusersComponent, AccountFpxComponent, AccountDdaComponent, AccountBankComponent, AccountSalarytransferComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _account_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account.module */ "./src/app/account/account.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountModule", function() { return _account_module__WEBPACK_IMPORTED_MODULE_0__["AccountModule"]; });

/* harmony import */ var _account_overview__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./account-overview */ "./src/app/account/account-overview/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountOverviewComponent", function() { return _account_overview__WEBPACK_IMPORTED_MODULE_1__["AccountOverviewComponent"]; });

/* harmony import */ var _account_users__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./account-users */ "./src/app/account/account-users/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountUsersComponent", function() { return _account_users__WEBPACK_IMPORTED_MODULE_2__["AccountUsersComponent"]; });

/* harmony import */ var _account_payment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./account-payment */ "./src/app/account/account-payment/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountPaymentComponent", function() { return _account_payment__WEBPACK_IMPORTED_MODULE_3__["AccountPaymentComponent"]; });

/* harmony import */ var _account_settings__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./account-settings */ "./src/app/account/account-settings/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountSettingsComponent", function() { return _account_settings__WEBPACK_IMPORTED_MODULE_4__["AccountSettingsComponent"]; });

/* harmony import */ var _account_addusers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./account-addusers */ "./src/app/account/account-addusers/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountAddusersComponent", function() { return _account_addusers__WEBPACK_IMPORTED_MODULE_5__["AccountAddusersComponent"]; });

/* harmony import */ var _account_fpx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./account-fpx */ "./src/app/account/account-fpx/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountFpxComponent", function() { return _account_fpx__WEBPACK_IMPORTED_MODULE_6__["AccountFpxComponent"]; });

/* harmony import */ var _account_dda__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./account-dda */ "./src/app/account/account-dda/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountDdaComponent", function() { return _account_dda__WEBPACK_IMPORTED_MODULE_7__["AccountDdaComponent"]; });

/* harmony import */ var _account_bank__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./account-bank */ "./src/app/account/account-bank/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountBankComponent", function() { return _account_bank__WEBPACK_IMPORTED_MODULE_8__["AccountBankComponent"]; });

/* harmony import */ var _account_salarytransfer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./account-salarytransfer */ "./src/app/account/account-salarytransfer/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AccountSalarytransferComponent", function() { return _account_salarytransfer__WEBPACK_IMPORTED_MODULE_9__["AccountSalarytransferComponent"]; });













/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _layouts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layouts */ "./src/app/layouts/index.ts");




const routes = [
    {
        path: "",
        loadChildren: "./dashboard/dashboard.module#DashboardModule"
    },
    {
        path: "home",
        loadChildren: "./dashboard/dashboard.module#DashboardModule"
    },
    {
        path: "payroll",
        loadChildren: "./payroll/payroll.module#PayrollModule"
    },
    {
        path: "employees",
        loadChildren: "./employees/employees.module#EmployeesModule"
    },
    {
        path: "transactions",
        loadChildren: "./transactions/transactions.module#TransactionsModule"
    },
    {
        path: "settings",
        loadChildren: "./account/account.module#AccountModule"
    },
    {
        path: "404",
        component: _layouts__WEBPACK_IMPORTED_MODULE_3__["ErrorPageComponent"]
    },
    {
        path: "**",
        component: _layouts__WEBPACK_IMPORTED_MODULE_3__["ErrorPageComponent"]
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.sass":
/*!************************************!*\
  !*** ./src/app/app.component.sass ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".epf-layout {\n  width: 100vw;\n  height: 100vh;\n  position: relative;\n}\n.epf-layout__header {\n  height: 70px;\n  width: 100%;\n  position: fixed;\n  background-color: #ffffff;\n  box-shadow: 0 0px 20px rgba(0, 0, 0, 0.08);\n  z-index: 1;\n}\n.epf-layout__sidenav {\n  width: 250px;\n  bottom: 0;\n  height: calc(100vh - 70px);\n  position: fixed;\n  z-index: 1;\n  background: linear-gradient(0deg, #312783 0%, #3828BB 100%);\n  transition: all 0.2s ease-in-out;\n}\n.epf-layout__body {\n  width: calc(100% - 250px);\n  padding: 30px 15px;\n  position: absolute;\n  top: 70px;\n  right: 0;\n  transition: all 0.2s ease-in-out;\n}\n.epf-layout__body__content {\n  width: 100%;\n  min-height: calc(100vh - 169px);\n}\n.epf-layout__body__footer {\n  width: 100%;\n}\n.epf-layout.toggleNav .epf-layout__sidenav {\n  width: 126px;\n}\n.epf-layout.toggleNav .epf-layout__body {\n  width: calc(100% - 126px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQzpcXERldlxcUHJvamVjdHNcXGV5XFxjbG91ZFxccGF5cm9sbC1wcm90b3R5cGUtLW9wdC1pblxccHJvdG90eXBlL3NyY1xcYXBwXFxhcHAuY29tcG9uZW50LnNhc3MiLCJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2FzcyIsInNyYy9hcHAvQzpcXERldlxcUHJvamVjdHNcXGV5XFxjbG91ZFxccGF5cm9sbC1wcm90b3R5cGUtLW9wdC1pblxccHJvdG90eXBlL3NyY1xcX3N0eWxlc1xcX2NvbG91cnMuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUNGSjtBREdJO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EseUJFUUE7RUZQQSwwQ0FBQTtFQUNBLFVBQUE7QUNEUjtBREVJO0VBQ0ksWUFBQTtFQUNBLFNBQUE7RUFDQSwwQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsMkRBQUE7RUFDQSxnQ0FBQTtBQ0FSO0FEQ0k7RUFDSSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLGdDQUFBO0FDQ1I7QURBUTtFQUNJLFdBQUE7RUFDQSwrQkFBQTtBQ0VaO0FERFE7RUFDSSxXQUFBO0FDR1o7QURBWTtFQUNJLFlBQUE7QUNFaEI7QUREWTtFQUNJLHlCQUFBO0FDR2hCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uL19zdHlsZXMvX2NvbG91cnNcIlxyXG5AaW1wb3J0IFwiLi4vX3N0eWxlcy9fdmFyaWFibGVzXCJcclxuXHJcbi5lcGYtbGF5b3V0XHJcbiAgICB3aWR0aDogMTAwdndcclxuICAgIGhlaWdodDogMTAwdmhcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZVxyXG4gICAgJl9faGVhZGVyXHJcbiAgICAgICAgaGVpZ2h0OiA3MHB4XHJcbiAgICAgICAgd2lkdGg6IDEwMCVcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWRcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkd2hpdGVcclxuICAgICAgICBib3gtc2hhZG93OiAwIDBweCAyMHB4IHJnYmEoMCwwLDAsMC4wOClcclxuICAgICAgICB6LWluZGV4OiAxXHJcbiAgICAmX19zaWRlbmF2XHJcbiAgICAgICAgd2lkdGg6IDI1MHB4XHJcbiAgICAgICAgYm90dG9tOiAwXHJcbiAgICAgICAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gNzBweClcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWRcclxuICAgICAgICB6LWluZGV4OiAxXHJcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDBkZWcsICRncmFkaWVudF9ibHVlMSAwJSwgJGdyYWRpZW50X2JsdWUyIDEwMCUpXHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXRcclxuICAgICZfX2JvZHlcclxuICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gMjUwcHgpXHJcbiAgICAgICAgcGFkZGluZzogJHBhZGRpbmctYmlnICRwYWRkaW5nLWJpZy8yXHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlXHJcbiAgICAgICAgdG9wOiA3MHB4XHJcbiAgICAgICAgcmlnaHQ6IDBcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dFxyXG4gICAgICAgICZfX2NvbnRlbnRcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCVcclxuICAgICAgICAgICAgbWluLWhlaWdodDogY2FsYygxMDB2aCAtIDE2OXB4KVxyXG4gICAgICAgICZfX2Zvb3RlclxyXG4gICAgICAgICAgICB3aWR0aDogMTAwJVxyXG4gICAgJi50b2dnbGVOYXZcclxuICAgICAgICAuZXBmLWxheW91dFxyXG4gICAgICAgICAgICAmX19zaWRlbmF2XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTI2cHhcclxuICAgICAgICAgICAgJl9fYm9keVxyXG4gICAgICAgICAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDEyNnB4KVxyXG4gICAgICAgICAgICAiLCIuZXBmLWxheW91dCB7XG4gIHdpZHRoOiAxMDB2dztcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmVwZi1sYXlvdXRfX2hlYWRlciB7XG4gIGhlaWdodDogNzBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgYm94LXNoYWRvdzogMCAwcHggMjBweCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xuICB6LWluZGV4OiAxO1xufVxuLmVwZi1sYXlvdXRfX3NpZGVuYXYge1xuICB3aWR0aDogMjUwcHg7XG4gIGJvdHRvbTogMDtcbiAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gNzBweCk7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogMTtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDBkZWcsICMzMTI3ODMgMCUsICMzODI4QkIgMTAwJSk7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xufVxuLmVwZi1sYXlvdXRfX2JvZHkge1xuICB3aWR0aDogY2FsYygxMDAlIC0gMjUwcHgpO1xuICBwYWRkaW5nOiAzMHB4IDE1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3MHB4O1xuICByaWdodDogMDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG59XG4uZXBmLWxheW91dF9fYm9keV9fY29udGVudCB7XG4gIHdpZHRoOiAxMDAlO1xuICBtaW4taGVpZ2h0OiBjYWxjKDEwMHZoIC0gMTY5cHgpO1xufVxuLmVwZi1sYXlvdXRfX2JvZHlfX2Zvb3RlciB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmVwZi1sYXlvdXQudG9nZ2xlTmF2IC5lcGYtbGF5b3V0X19zaWRlbmF2IHtcbiAgd2lkdGg6IDEyNnB4O1xufVxuLmVwZi1sYXlvdXQudG9nZ2xlTmF2IC5lcGYtbGF5b3V0X19ib2R5IHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDEyNnB4KTtcbn0iLCIvLyBNYWluIENvbG91cnNcclxuJGVwZl9ibHVlOiAjMzEyNzgyXHJcbiRlcGZfcmVkOiAjRTMwNjEzXHJcbiRlcGZfZ29sZDogI0Q0QTkzOVxyXG5cclxuLy8gU3VwcG9ydGl2ZSBDb2xvdXJzXHJcbiRibHVlMTogIzQxMzFCOFxyXG4kYmx1ZTI6ICM1MDNGRENcclxuJGJsdWUzOiAjRjZGQUZFXHJcblxyXG4vLyBOZXV0cmFsIENvbG91cnNcclxuJGRhcmtfZ3JleTE6ICMzMzMzMzNcclxuJGRhcmtfZ3JleTI6ICM2NjY2NjZcclxuJGRhcmtfZ3JleTM6ICM5OTk5OTlcclxuJGRhcmtfZ3JleTQ6ICNERERERERcclxuJGxpZ2h0X2dyZXkxOiAjRjRGNEY0XHJcbiRsaWdodF9ncmV5MjogI0Y4RjhGOFxyXG4kbGlnaHRfZ3JleTM6ICNGQ0ZDRkNcclxuXHJcbiR3aGl0ZTogI2ZmZmZmZlxyXG4kYmxhY2s6ICMwMDAwMDBcclxuXHJcbiRncmFkaWVudF9ibHVlMTogIzMxMjc4M1xyXG4kZ3JhZGllbnRfYmx1ZTI6ICMzODI4QkIiXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let AppComponent = class AppComponent {
    constructor(router, translate) {
        this.router = router;
        this.translate = translate;
        this.loading = false;
        this.fontsizeclass = "fontsize__normal";
        this.toggleClass = false;
        this.router.events.subscribe((routerEvent) => {
            this.checkRouterEvent(routerEvent);
            this.translate.setDefaultLang("en");
        });
    }
    checkRouterEvent(routerEvent) {
        if (routerEvent instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationStart"]) {
            this.loading = true;
        }
        if (routerEvent instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"] ||
            routerEvent instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationCancel"] ||
            routerEvent instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationError"]) {
            this.loading = false;
        }
    }
    getClass(data) {
        this.fontsizeclass = data;
    }
    getToggle(data) {
        this.toggleClass = data;
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-root",
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.sass */ "./src/app/app.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule, HttpLoaderFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm2015/ngx-translate-http-loader.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared */ "./src/app/shared/index.ts");
/* harmony import */ var _dashboard__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./dashboard */ "./src/app/dashboard/index.ts");
/* harmony import */ var _payroll__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./payroll */ "./src/app/payroll/index.ts");
/* harmony import */ var _employees__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./employees */ "./src/app/employees/index.ts");
/* harmony import */ var _transactions__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./transactions */ "./src/app/transactions/index.ts");
/* harmony import */ var _account__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./account */ "./src/app/account/index.ts");
/* harmony import */ var _layouts__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./layouts */ "./src/app/layouts/index.ts");

















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _layouts__WEBPACK_IMPORTED_MODULE_16__["HeaderComponent"],
            _layouts__WEBPACK_IMPORTED_MODULE_16__["NavigationComponent"],
            _layouts__WEBPACK_IMPORTED_MODULE_16__["LanguageToggleComponent"],
            _layouts__WEBPACK_IMPORTED_MODULE_16__["FooterComponent"],
            _layouts__WEBPACK_IMPORTED_MODULE_16__["ErrorPageComponent"],
            _layouts__WEBPACK_IMPORTED_MODULE_16__["RoutingSpinnerComponent"],
            _layouts__WEBPACK_IMPORTED_MODULE_16__["FontsizeToggleComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            _shared__WEBPACK_IMPORTED_MODULE_10__["SharedModule"],
            _dashboard__WEBPACK_IMPORTED_MODULE_11__["DashboardModule"],
            _payroll__WEBPACK_IMPORTED_MODULE_12__["PayrollModule"],
            _employees__WEBPACK_IMPORTED_MODULE_13__["EmployeesModule"],
            _transactions__WEBPACK_IMPORTED_MODULE_14__["TransactionsModule"],
            _account__WEBPACK_IMPORTED_MODULE_15__["AccountModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forRoot({
                loader: {
                    provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateLoader"],
                    useFactory: HttpLoaderFactory,
                    deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"]]
                }
            })
        ],
        providers: [_shared__WEBPACK_IMPORTED_MODULE_10__["OptinService"]],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], AppModule);

function HttpLoaderFactory(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_8__["TranslateHttpLoader"](http);
}


/***/ }),

/***/ "./src/app/dashboard/dashboard-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/dashboard/dashboard-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home */ "./src/app/dashboard/home/index.ts");
/* harmony import */ var _optin__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./optin */ "./src/app/dashboard/optin/index.ts");





const routes = [
    {
        path: "",
        component: _optin__WEBPACK_IMPORTED_MODULE_4__["OptinComponent"],
        children: [
            {
                path: "home",
                component: _home__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"]
            }
        ]
    }
];
let DashboardRoutingModule = class DashboardRoutingModule {
};
DashboardRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], DashboardRoutingModule);



/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/dashboard/dashboard-routing.module.ts");
/* harmony import */ var _optin__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./optin */ "./src/app/dashboard/optin/index.ts");
/* harmony import */ var _home__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home */ "./src/app/dashboard/home/index.ts");







let DashboardModule = class DashboardModule {
};
DashboardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_optin__WEBPACK_IMPORTED_MODULE_5__["OptinComponent"], _home__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_4__["DashboardRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]],
        exports: [_optin__WEBPACK_IMPORTED_MODULE_5__["OptinComponent"], _home__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"]]
    })
], DashboardModule);



/***/ }),

/***/ "./src/app/dashboard/home/home.component.sass":
/*!****************************************************!*\
  !*** ./src/app/dashboard/home/home.component.sass ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9ob21lL2hvbWUuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/dashboard/home/home.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/home/home.component.ts ***!
  \**************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() { }
    ngOnInit() { }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-home",
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.sass */ "./src/app/dashboard/home/home.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HomeComponent);



/***/ }),

/***/ "./src/app/dashboard/home/index.ts":
/*!*****************************************!*\
  !*** ./src/app/dashboard/home/index.ts ***!
  \*****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.component */ "./src/app/dashboard/home/home.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return _home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"]; });




/***/ }),

/***/ "./src/app/dashboard/index.ts":
/*!************************************!*\
  !*** ./src/app/dashboard/index.ts ***!
  \************************************/
/*! exports provided: DashboardModule, HomeComponent, OptinComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dashboard_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard.module */ "./src/app/dashboard/dashboard.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return _dashboard_module__WEBPACK_IMPORTED_MODULE_0__["DashboardModule"]; });

/* harmony import */ var _home__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home */ "./src/app/dashboard/home/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return _home__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"]; });

/* harmony import */ var _optin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./optin */ "./src/app/dashboard/optin/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OptinComponent", function() { return _optin__WEBPACK_IMPORTED_MODULE_2__["OptinComponent"]; });






/***/ }),

/***/ "./src/app/dashboard/optin/index.ts":
/*!******************************************!*\
  !*** ./src/app/dashboard/optin/index.ts ***!
  \******************************************/
/*! exports provided: OptinComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _optin_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./optin.component */ "./src/app/dashboard/optin/optin.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OptinComponent", function() { return _optin_component__WEBPACK_IMPORTED_MODULE_0__["OptinComponent"]; });




/***/ }),

/***/ "./src/app/dashboard/optin/optin.component.sass":
/*!******************************************************!*\
  !*** ./src/app/dashboard/optin/optin.component.sass ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9vcHRpbi9vcHRpbi5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/optin/optin.component.ts":
/*!****************************************************!*\
  !*** ./src/app/dashboard/optin/optin.component.ts ***!
  \****************************************************/
/*! exports provided: OptinComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OptinComponent", function() { return OptinComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let OptinComponent = class OptinComponent {
    constructor() { }
    ngOnInit() { }
};
OptinComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-optin",
        template: __webpack_require__(/*! raw-loader!./optin.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/optin/optin.component.html"),
        styles: [__webpack_require__(/*! ./optin.component.sass */ "./src/app/dashboard/optin/optin.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], OptinComponent);



/***/ }),

/***/ "./src/app/employees/employee-add-details/employee-add-details.component.sass":
/*!************************************************************************************!*\
  !*** ./src/app/employees/employee-add-details/employee-add-details.component.sass ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtcGxveWVlcy9lbXBsb3llZS1hZGQtZGV0YWlscy9lbXBsb3llZS1hZGQtZGV0YWlscy5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/employees/employee-add-details/employee-add-details.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/employees/employee-add-details/employee-add-details.component.ts ***!
  \**********************************************************************************/
/*! exports provided: EmployeeAddDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeAddDetailsComponent", function() { return EmployeeAddDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EmployeeAddDetailsComponent = class EmployeeAddDetailsComponent {
    constructor() { }
    ngOnInit() {
    }
};
EmployeeAddDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-employee-add-details',
        template: __webpack_require__(/*! raw-loader!./employee-add-details.component.html */ "./node_modules/raw-loader/index.js!./src/app/employees/employee-add-details/employee-add-details.component.html"),
        styles: [__webpack_require__(/*! ./employee-add-details.component.sass */ "./src/app/employees/employee-add-details/employee-add-details.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EmployeeAddDetailsComponent);



/***/ }),

/***/ "./src/app/employees/employee-add-details/index.ts":
/*!*********************************************************!*\
  !*** ./src/app/employees/employee-add-details/index.ts ***!
  \*********************************************************/
/*! exports provided: EmployeeAddDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _employee_add_details_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./employee-add-details.component */ "./src/app/employees/employee-add-details/employee-add-details.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeAddDetailsComponent", function() { return _employee_add_details_component__WEBPACK_IMPORTED_MODULE_0__["EmployeeAddDetailsComponent"]; });




/***/ }),

/***/ "./src/app/employees/employee-add/employee-add.component.sass":
/*!********************************************************************!*\
  !*** ./src/app/employees/employee-add/employee-add.component.sass ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtcGxveWVlcy9lbXBsb3llZS1hZGQvZW1wbG95ZWUtYWRkLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/employees/employee-add/employee-add.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/employees/employee-add/employee-add.component.ts ***!
  \******************************************************************/
/*! exports provided: EmployeeAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeAddComponent", function() { return EmployeeAddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EmployeeAddComponent = class EmployeeAddComponent {
    constructor() { }
    ngOnInit() {
    }
};
EmployeeAddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-employee-add',
        template: __webpack_require__(/*! raw-loader!./employee-add.component.html */ "./node_modules/raw-loader/index.js!./src/app/employees/employee-add/employee-add.component.html"),
        styles: [__webpack_require__(/*! ./employee-add.component.sass */ "./src/app/employees/employee-add/employee-add.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EmployeeAddComponent);



/***/ }),

/***/ "./src/app/employees/employee-add/index.ts":
/*!*************************************************!*\
  !*** ./src/app/employees/employee-add/index.ts ***!
  \*************************************************/
/*! exports provided: EmployeeAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _employee_add_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./employee-add.component */ "./src/app/employees/employee-add/employee-add.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeAddComponent", function() { return _employee_add_component__WEBPACK_IMPORTED_MODULE_0__["EmployeeAddComponent"]; });




/***/ }),

/***/ "./src/app/employees/employee-edit/employee-edit.component.sass":
/*!**********************************************************************!*\
  !*** ./src/app/employees/employee-edit/employee-edit.component.sass ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtcGxveWVlcy9lbXBsb3llZS1lZGl0L2VtcGxveWVlLWVkaXQuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/employees/employee-edit/employee-edit.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/employees/employee-edit/employee-edit.component.ts ***!
  \********************************************************************/
/*! exports provided: EmployeeEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeEditComponent", function() { return EmployeeEditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EmployeeEditComponent = class EmployeeEditComponent {
    constructor() { }
    ngOnInit() {
    }
};
EmployeeEditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-employee-edit',
        template: __webpack_require__(/*! raw-loader!./employee-edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/employees/employee-edit/employee-edit.component.html"),
        styles: [__webpack_require__(/*! ./employee-edit.component.sass */ "./src/app/employees/employee-edit/employee-edit.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EmployeeEditComponent);



/***/ }),

/***/ "./src/app/employees/employee-edit/index.ts":
/*!**************************************************!*\
  !*** ./src/app/employees/employee-edit/index.ts ***!
  \**************************************************/
/*! exports provided: EmployeeEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _employee_edit_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./employee-edit.component */ "./src/app/employees/employee-edit/employee-edit.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeEditComponent", function() { return _employee_edit_component__WEBPACK_IMPORTED_MODULE_0__["EmployeeEditComponent"]; });




/***/ }),

/***/ "./src/app/employees/employee-overview/employee-overview.component.sass":
/*!******************************************************************************!*\
  !*** ./src/app/employees/employee-overview/employee-overview.component.sass ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtcGxveWVlcy9lbXBsb3llZS1vdmVydmlldy9lbXBsb3llZS1vdmVydmlldy5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/employees/employee-overview/employee-overview.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/employees/employee-overview/employee-overview.component.ts ***!
  \****************************************************************************/
/*! exports provided: EmployeeOverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeOverviewComponent", function() { return EmployeeOverviewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _employee_datatable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employee_datatable */ "./src/app/employees/employee-overview/employee_datatable.ts");
/* harmony import */ var lit_html__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lit-html */ "./node_modules/lit-html/lit-html.js");
/* harmony import */ var lit_html_directives_unsafe_html_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lit-html/directives/unsafe-html.js */ "./node_modules/lit-html/directives/unsafe-html.js");
/* harmony import */ var _shared_generals_pipes_ringgit_pipe__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/_generals/pipes/ringgit.pipe */ "./src/app/shared/_generals/pipes/ringgit.pipe.ts");






let EmployeeOverviewComponent = class EmployeeOverviewComponent {
    constructor() {
        this.table_opts = {
            searchable: false,
            searchby: "",
            pagination: false,
            sortable: false,
            expandable: true,
            table_data: _employee_datatable__WEBPACK_IMPORTED_MODULE_2__["employeeTable"],
            table_expandable: function (element) {
                return lit_html__WEBPACK_IMPORTED_MODULE_3__["html"] `
        <table>
          <tr>
            <td><span>Employement Type</span> ${element.employment_type}</td>
            <td><span>Payment Method</span> ${element.payment_method}</td>
            <td><span>NRIC</span> ${element.nric}</td>
          </tr>

          <tr>
            <td>
              <span>Employer Epf Contribution</span>
              ${Object(lit_html_directives_unsafe_html_js__WEBPACK_IMPORTED_MODULE_4__["unsafeHTML"])(Object(_shared_generals_pipes_ringgit_pipe__WEBPACK_IMPORTED_MODULE_5__["getHTMLRinggit"])(element.employer_epf_contribution))}
            </td>
            <td>
              <span>Income Tax</span>
              ${Object(lit_html_directives_unsafe_html_js__WEBPACK_IMPORTED_MODULE_4__["unsafeHTML"])(Object(_shared_generals_pipes_ringgit_pipe__WEBPACK_IMPORTED_MODULE_5__["getHTMLRinggit"])(element.income_tax))}
            </td>
            <td>
              <span>Employee Epf Contribution</span>
              ${Object(lit_html_directives_unsafe_html_js__WEBPACK_IMPORTED_MODULE_4__["unsafeHTML"])(Object(_shared_generals_pipes_ringgit_pipe__WEBPACK_IMPORTED_MODULE_5__["getHTMLRinggit"])(element.employee_epf_contribution))}
            </td>
          </tr>
        </table>
      `;
            },
            table_columns: [
                {
                    display: "Name of Employee",
                    slug: "name_of_employee",
                    currency: false
                },
                {
                    display: "Salary Amount",
                    slug: "salary_amount",
                    currency: true
                },
                {
                    display: "Salary Type",
                    slug: "salary_type",
                    currency: false
                },
                {
                    display: "Statutory Eligibility",
                    slug: "statutory_eligibility",
                    currency: false
                }
            ]
        };
    }
    ngOnInit() { }
};
EmployeeOverviewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-employee-overview",
        template: __webpack_require__(/*! raw-loader!./employee-overview.component.html */ "./node_modules/raw-loader/index.js!./src/app/employees/employee-overview/employee-overview.component.html"),
        styles: [__webpack_require__(/*! ./employee-overview.component.sass */ "./src/app/employees/employee-overview/employee-overview.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EmployeeOverviewComponent);



/***/ }),

/***/ "./src/app/employees/employee-overview/employee_datatable.ts":
/*!*******************************************************************!*\
  !*** ./src/app/employees/employee-overview/employee_datatable.ts ***!
  \*******************************************************************/
/*! exports provided: employeeTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "employeeTable", function() { return employeeTable; });
const employeeTable = [
    {
        name_of_employee: "Chong Jia Yong",
        salary_amount: 2000.0,
        salary_type: ["Basic"],
        statutory_eligibility: [
            "EPF",
            "Tax",
            "SOCSO",
            "EIS",
            "LHDN",
            "HRDF",
            "Zakat"
        ],
        employment_type: "Full Time",
        payment_method: "Bank Transfer",
        nric: 880301085848,
        employee_epf_contribution: 200.0,
        employer_epf_contribution: 250.0,
        income_tax: 15.0
    },
    {
        name_of_employee: "Cristina Chen Hui Min",
        salary_amount: 2250.0,
        salary_type: ["Basic", "Commission"],
        statutory_eligibility: ["EPF", "Tax"],
        employment_type: "Full Time",
        payment_method: "Bank Transfer",
        nric: 880301085848,
        employee_epf_contribution: 200.0,
        employer_epf_contribution: 250.0,
        income_tax: 15.0
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan",
        salary_amount: 4600.0,
        salary_type: ["Basic", "Commission"],
        statutory_eligibility: ["EPF", "Tax"],
        employment_type: "Full Time",
        payment_method: "Bank Transfer",
        nric: 880301085848,
        employee_epf_contribution: 200.0,
        employer_epf_contribution: 250.0,
        income_tax: 15.0
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan",
        salary_amount: 4600.0,
        salary_type: ["Basic", "Commission"],
        statutory_eligibility: ["EPF", "Tax"],
        employment_type: "Full Time",
        payment_method: "Bank Transfer",
        nric: 880301085848,
        employee_epf_contribution: 200.0,
        employer_epf_contribution: 250.0,
        income_tax: 15.0
    },
    {
        name_of_employee: "Terry Wallace",
        salary_amount: 5800.0,
        salary_type: ["Basic"],
        statutory_eligibility: ["EPF", "Tax", "SOCSO"],
        employment_type: "Full Time",
        payment_method: "Bank Transfer",
        nric: 880301085848,
        employee_epf_contribution: 200.0,
        employer_epf_contribution: 250.0,
        income_tax: 15.0
    }
];


/***/ }),

/***/ "./src/app/employees/employee-overview/index.ts":
/*!******************************************************!*\
  !*** ./src/app/employees/employee-overview/index.ts ***!
  \******************************************************/
/*! exports provided: EmployeeOverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _employee_overview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./employee-overview.component */ "./src/app/employees/employee-overview/employee-overview.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeOverviewComponent", function() { return _employee_overview_component__WEBPACK_IMPORTED_MODULE_0__["EmployeeOverviewComponent"]; });




/***/ }),

/***/ "./src/app/employees/employee-search/employee-search.component.sass":
/*!**************************************************************************!*\
  !*** ./src/app/employees/employee-search/employee-search.component.sass ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtcGxveWVlcy9lbXBsb3llZS1zZWFyY2gvZW1wbG95ZWUtc2VhcmNoLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/employees/employee-search/employee-search.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/employees/employee-search/employee-search.component.ts ***!
  \************************************************************************/
/*! exports provided: EmployeeSearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeSearchComponent", function() { return EmployeeSearchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EmployeeSearchComponent = class EmployeeSearchComponent {
    constructor() { }
    ngOnInit() {
    }
};
EmployeeSearchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-employee-search',
        template: __webpack_require__(/*! raw-loader!./employee-search.component.html */ "./node_modules/raw-loader/index.js!./src/app/employees/employee-search/employee-search.component.html"),
        styles: [__webpack_require__(/*! ./employee-search.component.sass */ "./src/app/employees/employee-search/employee-search.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EmployeeSearchComponent);



/***/ }),

/***/ "./src/app/employees/employee-search/index.ts":
/*!****************************************************!*\
  !*** ./src/app/employees/employee-search/index.ts ***!
  \****************************************************/
/*! exports provided: EmployeeSearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _employee_search_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./employee-search.component */ "./src/app/employees/employee-search/employee-search.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeSearchComponent", function() { return _employee_search_component__WEBPACK_IMPORTED_MODULE_0__["EmployeeSearchComponent"]; });




/***/ }),

/***/ "./src/app/employees/employees-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/employees/employees-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: EmployeesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesRoutingModule", function() { return EmployeesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _employee_overview__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employee-overview */ "./src/app/employees/employee-overview/index.ts");
/* harmony import */ var _employee_search__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./employee-search */ "./src/app/employees/employee-search/index.ts");
/* harmony import */ var _employee_edit__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./employee-edit */ "./src/app/employees/employee-edit/index.ts");
/* harmony import */ var _employee_add__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./employee-add */ "./src/app/employees/employee-add/index.ts");
/* harmony import */ var _employee_add_details__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./employee-add-details */ "./src/app/employees/employee-add-details/index.ts");








const routes = [
    {
        path: "",
        component: _employee_overview__WEBPACK_IMPORTED_MODULE_3__["EmployeeOverviewComponent"],
        children: [
            {
                path: "search",
                component: _employee_search__WEBPACK_IMPORTED_MODULE_4__["EmployeeSearchComponent"]
            },
            {
                path: "add",
                component: _employee_add__WEBPACK_IMPORTED_MODULE_6__["EmployeeAddComponent"]
            },
            {
                path: "edit:id",
                component: _employee_edit__WEBPACK_IMPORTED_MODULE_5__["EmployeeEditComponent"]
            },
            {
                path: "add-details:id",
                component: _employee_add_details__WEBPACK_IMPORTED_MODULE_7__["EmployeeAddDetailsComponent"]
            }
        ]
    }
];
let EmployeesRoutingModule = class EmployeesRoutingModule {
};
EmployeesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], EmployeesRoutingModule);



/***/ }),

/***/ "./src/app/employees/employees.module.ts":
/*!***********************************************!*\
  !*** ./src/app/employees/employees.module.ts ***!
  \***********************************************/
/*! exports provided: EmployeesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesModule", function() { return EmployeesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _employees_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./employees-routing.module */ "./src/app/employees/employees-routing.module.ts");
/* harmony import */ var _employee_overview__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./employee-overview */ "./src/app/employees/employee-overview/index.ts");
/* harmony import */ var _employee_search__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./employee-search */ "./src/app/employees/employee-search/index.ts");
/* harmony import */ var _employee_edit__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./employee-edit */ "./src/app/employees/employee-edit/index.ts");
/* harmony import */ var _employee_add__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./employee-add */ "./src/app/employees/employee-add/index.ts");
/* harmony import */ var _employee_add_details__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./employee-add-details */ "./src/app/employees/employee-add-details/index.ts");










let EmployeesModule = class EmployeesModule {
};
EmployeesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _employee_overview__WEBPACK_IMPORTED_MODULE_5__["EmployeeOverviewComponent"],
            _employee_search__WEBPACK_IMPORTED_MODULE_6__["EmployeeSearchComponent"],
            _employee_edit__WEBPACK_IMPORTED_MODULE_7__["EmployeeEditComponent"],
            _employee_add__WEBPACK_IMPORTED_MODULE_8__["EmployeeAddComponent"],
            _employee_add_details__WEBPACK_IMPORTED_MODULE_9__["EmployeeAddDetailsComponent"]
        ],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _employees_routing_module__WEBPACK_IMPORTED_MODULE_4__["EmployeesRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]],
        exports: [
            _employee_overview__WEBPACK_IMPORTED_MODULE_5__["EmployeeOverviewComponent"],
            _employee_search__WEBPACK_IMPORTED_MODULE_6__["EmployeeSearchComponent"],
            _employee_edit__WEBPACK_IMPORTED_MODULE_7__["EmployeeEditComponent"],
            _employee_add__WEBPACK_IMPORTED_MODULE_8__["EmployeeAddComponent"],
            _employee_add_details__WEBPACK_IMPORTED_MODULE_9__["EmployeeAddDetailsComponent"]
        ]
    })
], EmployeesModule);



/***/ }),

/***/ "./src/app/employees/index.ts":
/*!************************************!*\
  !*** ./src/app/employees/index.ts ***!
  \************************************/
/*! exports provided: EmployeesModule, EmployeeOverviewComponent, EmployeeSearchComponent, EmployeeEditComponent, EmployeeAddComponent, EmployeeAddDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _employees_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./employees.module */ "./src/app/employees/employees.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeesModule", function() { return _employees_module__WEBPACK_IMPORTED_MODULE_0__["EmployeesModule"]; });

/* harmony import */ var _employee_overview__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./employee-overview */ "./src/app/employees/employee-overview/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeOverviewComponent", function() { return _employee_overview__WEBPACK_IMPORTED_MODULE_1__["EmployeeOverviewComponent"]; });

/* harmony import */ var _employee_search__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employee-search */ "./src/app/employees/employee-search/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeSearchComponent", function() { return _employee_search__WEBPACK_IMPORTED_MODULE_2__["EmployeeSearchComponent"]; });

/* harmony import */ var _employee_edit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employee-edit */ "./src/app/employees/employee-edit/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeEditComponent", function() { return _employee_edit__WEBPACK_IMPORTED_MODULE_3__["EmployeeEditComponent"]; });

/* harmony import */ var _employee_add__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./employee-add */ "./src/app/employees/employee-add/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeAddComponent", function() { return _employee_add__WEBPACK_IMPORTED_MODULE_4__["EmployeeAddComponent"]; });

/* harmony import */ var _employee_add_details__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./employee-add-details */ "./src/app/employees/employee-add-details/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmployeeAddDetailsComponent", function() { return _employee_add_details__WEBPACK_IMPORTED_MODULE_5__["EmployeeAddDetailsComponent"]; });









/***/ }),

/***/ "./src/app/layouts/error-page/error-page.component.sass":
/*!**************************************************************!*\
  !*** ./src/app/layouts/error-page/error-page.component.sass ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvZXJyb3ItcGFnZS9lcnJvci1wYWdlLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/layouts/error-page/error-page.component.ts":
/*!************************************************************!*\
  !*** ./src/app/layouts/error-page/error-page.component.ts ***!
  \************************************************************/
/*! exports provided: ErrorPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorPageComponent", function() { return ErrorPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ErrorPageComponent = class ErrorPageComponent {
    constructor() { }
    ngOnInit() {
    }
};
ErrorPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-error-page',
        template: __webpack_require__(/*! raw-loader!./error-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/error-page/error-page.component.html"),
        styles: [__webpack_require__(/*! ./error-page.component.sass */ "./src/app/layouts/error-page/error-page.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ErrorPageComponent);



/***/ }),

/***/ "./src/app/layouts/error-page/index.ts":
/*!*********************************************!*\
  !*** ./src/app/layouts/error-page/index.ts ***!
  \*********************************************/
/*! exports provided: ErrorPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _error_page_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./error-page.component */ "./src/app/layouts/error-page/error-page.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ErrorPageComponent", function() { return _error_page_component__WEBPACK_IMPORTED_MODULE_0__["ErrorPageComponent"]; });




/***/ }),

/***/ "./src/app/layouts/footer/footer.component.sass":
/*!******************************************************!*\
  !*** ./src/app/layouts/footer/footer.component.sass ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1, h2, h3, h4,\n.h1, .h2, .h3, .h4 {\n  margin: 0;\n}\nh1:first-child, h2:first-child, h3:first-child, h4:first-child,\n.h1:first-child, .h2:first-child, .h3:first-child, .h4:first-child {\n  margin-top: 0;\n}\nh1:last-child, h2:last-child, h3:last-child, h4:last-child,\n.h1:last-child, .h2:last-child, .h3:last-child, .h4:last-child {\n  margin-bottom: 0;\n}\nh1, .h1 {\n  font-family: \"EPF Book\";\n  font-size: 56px;\n  font-size: 3.5em;\n  line-height: 5.25em;\n}\nh3, .h3 {\n  font-family: \"EPF Medium\";\n  font-size: 26px;\n  font-size: 1.625em;\n  line-height: 2.4375em;\n}\np {\n  width: 100%;\n  font-family: \"EPF Book\";\n  font-size: 16px;\n  font-size: 1em;\n  line-height: 1.5em;\n  transition: all 0.2s ease-in-out;\n}\np:first-child {\n  margin-top: 0;\n}\np:last-child {\n  margin-bottom: 0;\n}\np a {\n  text-decoration: none;\n  transition: all 0.2s ease-in-out;\n  color: inherit;\n}\np a:hover {\n  text-decoration: none;\n  color: inherit;\n}\nfooter {\n  margin-top: 30px;\n}\nfooter p {\n  font-size: 10px;\n  font-size: 0.625em;\n  line-height: 0.9375em;\n  color: #999999;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0cy9mb290ZXIvQzpcXERldlxcUHJvamVjdHNcXGV5XFxjbG91ZFxccGF5cm9sbC1wcm90b3R5cGUtLW9wdC1pblxccHJvdG90eXBlL3NyY1xcYXBwXFxsYXlvdXRzXFxmb290ZXJcXGZvb3Rlci5jb21wb25lbnQuc2FzcyIsInNyYy9hcHAvbGF5b3V0cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zYXNzIiwic3JjL2FwcC9sYXlvdXRzL2Zvb3Rlci9DOlxcRGV2XFxQcm9qZWN0c1xcZXlcXGNsb3VkXFxwYXlyb2xsLXByb3RvdHlwZS0tb3B0LWluXFxwcm90b3R5cGUvc3RkaW4iLCJzcmMvYXBwL2xheW91dHMvZm9vdGVyL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zcmNcXF9zdHlsZXNcXF92YXJpYWJsZXMuc2FzcyIsInNyYy9hcHAvbGF5b3V0cy9mb290ZXIvQzpcXERldlxcUHJvamVjdHNcXGV5XFxjbG91ZFxccGF5cm9sbC1wcm90b3R5cGUtLW9wdC1pblxccHJvdG90eXBlL3NyY1xcX3N0eWxlc1xcX2NvbG91cnMuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFpQkE7O0VBRUksU0FBQTtBQ2hCSjtBRGlCSTs7RUFDSSxhQUFBO0FDZFI7QURlSTs7RUFDSSxnQkFBQTtBQ1pSO0FEY0E7RUFDSSx1QkF0QkU7RUFRRixlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ0lKO0FEV0E7RUFDSSx5QkF2Qk07RUFLTixlQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBQ1dKO0FEUUE7RUFDSSxXQUFBO0VBQ0EsdUJBL0JFO0VBUUYsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQXVCQSxnQ0FBQTtBQ0hKO0FESUk7RUFDSSxhQUFBO0FDRlI7QURHSTtFQUNJLGdCQUFBO0FDRFI7QURFSTtFQUNJLHFCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxjQUFBO0FDQVI7QURDUTtFQUNJLHFCQUFBO0VBQ0EsY0FBQTtBQ0NaO0FDNUNBO0VBQ0ksZ0JDRFM7QUZnRGI7QUM5Q0k7RUZLQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFRUxJLGNFSUs7QUg4Q2IiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXRzL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBEZWZhdWx0IHNpemluZ1xyXG4kYmFzZS1mb250LXNpemU6IDE2ICFkZWZhdWx0XHJcbiRiYXNlLWxpbmUtaGVpZ2h0OiAxLjUgIWRlZmF1bHRcclxuXHJcbiRwcmk6ICdFUEYgQm9vaydcclxuJHByaS1pdDogJ0VQRiBCb29rIEl0YWxpYydcclxuJHByaS1kZW1pOiAnRVBGIERlbWknXHJcbiRwcmktbWVkOiAnRVBGIE1lZGl1bSdcclxuJHByaS1odnk6ICdFUEYgSGVhdnknXHJcblxyXG4vLyBNaXhpbnNcclxuQG1peGluIGZvbnQtc2l6ZSgkc2l6ZS1pbi1weCwgJGxpbmUtaGVpZ2h0OjEuNSlcclxuICAgIGZvbnQtc2l6ZTogJHNpemUtaW4tcHggKiAxcHhcclxuICAgIGZvbnQtc2l6ZTogKCRzaXplLWluLXB4IC8gJGJhc2UtZm9udC1zaXplKSAqIDFlbVxyXG4gICAgbGluZS1oZWlnaHQ6ICgoJHNpemUtaW4tcHggLyAkYmFzZS1mb250LXNpemUpICogJGxpbmUtaGVpZ2h0KSAqIDFlbVxyXG5cclxuXHJcbmgxLCBoMiwgaDMsIGg0LFxyXG4uaDEsIC5oMiwgLmgzLCAuaDRcclxuICAgIG1hcmdpbjogMFxyXG4gICAgJjpmaXJzdC1jaGlsZFxyXG4gICAgICAgIG1hcmdpbi10b3A6IDBcclxuICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBcclxuXHJcbmgxLCAuaDFcclxuICAgIGZvbnQtZmFtaWx5OiAkcHJpXHJcbiAgICBAaW5jbHVkZSBmb250LXNpemUoNTYpXHJcblxyXG5oMywgLmgzXHJcbiAgICBmb250LWZhbWlseTogJHByaS1tZWRcclxuICAgIEBpbmNsdWRlIGZvbnQtc2l6ZSgyNilcclxuXHJcbnBcclxuICAgIHdpZHRoOiAxMDAlXHJcbiAgICBmb250LWZhbWlseTogJHByaVxyXG4gICAgQGluY2x1ZGUgZm9udC1zaXplKDE2KVxyXG4gICAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXRcclxuICAgICY6Zmlyc3QtY2hpbGRcclxuICAgICAgICBtYXJnaW4tdG9wOiAwXHJcbiAgICAmOmxhc3QtY2hpbGRcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwXHJcbiAgICBhXHJcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lXHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXRcclxuICAgICAgICBjb2xvcjogaW5oZXJpdFxyXG4gICAgICAgICY6aG92ZXJcclxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lXHJcbiAgICAgICAgICAgIGNvbG9yOiBpbmhlcml0XHJcbiIsImgxLCBoMiwgaDMsIGg0LFxuLmgxLCAuaDIsIC5oMywgLmg0IHtcbiAgbWFyZ2luOiAwO1xufVxuaDE6Zmlyc3QtY2hpbGQsIGgyOmZpcnN0LWNoaWxkLCBoMzpmaXJzdC1jaGlsZCwgaDQ6Zmlyc3QtY2hpbGQsXG4uaDE6Zmlyc3QtY2hpbGQsIC5oMjpmaXJzdC1jaGlsZCwgLmgzOmZpcnN0LWNoaWxkLCAuaDQ6Zmlyc3QtY2hpbGQge1xuICBtYXJnaW4tdG9wOiAwO1xufVxuaDE6bGFzdC1jaGlsZCwgaDI6bGFzdC1jaGlsZCwgaDM6bGFzdC1jaGlsZCwgaDQ6bGFzdC1jaGlsZCxcbi5oMTpsYXN0LWNoaWxkLCAuaDI6bGFzdC1jaGlsZCwgLmgzOmxhc3QtY2hpbGQsIC5oNDpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cblxuaDEsIC5oMSB7XG4gIGZvbnQtZmFtaWx5OiBcIkVQRiBCb29rXCI7XG4gIGZvbnQtc2l6ZTogNTZweDtcbiAgZm9udC1zaXplOiAzLjVlbTtcbiAgbGluZS1oZWlnaHQ6IDUuMjVlbTtcbn1cblxuaDMsIC5oMyB7XG4gIGZvbnQtZmFtaWx5OiBcIkVQRiBNZWRpdW1cIjtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBmb250LXNpemU6IDEuNjI1ZW07XG4gIGxpbmUtaGVpZ2h0OiAyLjQzNzVlbTtcbn1cblxucCB7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LWZhbWlseTogXCJFUEYgQm9va1wiO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsaW5lLWhlaWdodDogMS41ZW07XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xufVxucDpmaXJzdC1jaGlsZCB7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5wOmxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxucCBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbiAgY29sb3I6IGluaGVyaXQ7XG59XG5wIGE6aG92ZXIge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiBpbmhlcml0O1xufVxuXG5mb290ZXIge1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufVxuZm9vdGVyIHAge1xuICBmb250LXNpemU6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMC42MjVlbTtcbiAgbGluZS1oZWlnaHQ6IDAuOTM3NWVtO1xuICBjb2xvcjogIzk5OTk5OTtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vLi4vX3N0eWxlcy9fdXRpbHNcIlxyXG5AaW1wb3J0IFwiLi4vLi4vLi4vX3N0eWxlcy9fY29sb3Vyc1wiXHJcbkBpbXBvcnQgXCIuLi8uLi8uLi9fc3R5bGVzL192YXJpYWJsZXNcIlxyXG5AaW1wb3J0IFwiLi4vLi4vLi4vX3N0eWxlcy9fZm9udHNcIlxyXG5cclxuZm9vdGVyXHJcbiAgICBtYXJnaW4tdG9wOiAkbWFyZ2luX2JpZ1xyXG4gICAgcFxyXG4gICAgICAgIEBpbmNsdWRlIGZvbnQtc2l6ZSgxMClcclxuICAgICAgICBjb2xvcjogJGRhcmtfZ3JleTMiLCIvLyBwYWRkaW5nXHJcbiRwYWRkaW5nX2JpZzogMzBweFxyXG4kcGFkZGluZzogMTBweFxyXG5cclxuLy8gbWFyZ2luXHJcbiRtYXJnaW5fYmlnOiAzMHB4XHJcbiRtYXJnaW46IDEwcHgiLCIvLyBNYWluIENvbG91cnNcclxuJGVwZl9ibHVlOiAjMzEyNzgyXHJcbiRlcGZfcmVkOiAjRTMwNjEzXHJcbiRlcGZfZ29sZDogI0Q0QTkzOVxyXG5cclxuLy8gU3VwcG9ydGl2ZSBDb2xvdXJzXHJcbiRibHVlMTogIzQxMzFCOFxyXG4kYmx1ZTI6ICM1MDNGRENcclxuJGJsdWUzOiAjRjZGQUZFXHJcblxyXG4vLyBOZXV0cmFsIENvbG91cnNcclxuJGRhcmtfZ3JleTE6ICMzMzMzMzNcclxuJGRhcmtfZ3JleTI6ICM2NjY2NjZcclxuJGRhcmtfZ3JleTM6ICM5OTk5OTlcclxuJGRhcmtfZ3JleTQ6ICNERERERERcclxuJGxpZ2h0X2dyZXkxOiAjRjRGNEY0XHJcbiRsaWdodF9ncmV5MjogI0Y4RjhGOFxyXG4kbGlnaHRfZ3JleTM6ICNGQ0ZDRkNcclxuXHJcbiR3aGl0ZTogI2ZmZmZmZlxyXG4kYmxhY2s6ICMwMDAwMDBcclxuXHJcbiRncmFkaWVudF9ibHVlMTogIzMxMjc4M1xyXG4kZ3JhZGllbnRfYmx1ZTI6ICMzODI4QkIiXX0= */"

/***/ }),

/***/ "./src/app/layouts/footer/footer.component.ts":
/*!****************************************************!*\
  !*** ./src/app/layouts/footer/footer.component.ts ***!
  \****************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FooterComponent = class FooterComponent {
    constructor() { }
    ngOnInit() { }
};
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-footer",
        template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/footer/footer.component.html"),
        styles: [__webpack_require__(/*! ./footer.component.sass */ "./src/app/layouts/footer/footer.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FooterComponent);



/***/ }),

/***/ "./src/app/layouts/footer/index.ts":
/*!*****************************************!*\
  !*** ./src/app/layouts/footer/index.ts ***!
  \*****************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _footer_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./footer.component */ "./src/app/layouts/footer/footer.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return _footer_component__WEBPACK_IMPORTED_MODULE_0__["FooterComponent"]; });




/***/ }),

/***/ "./src/app/layouts/header/fontsize-toggle/fontsize-toggle.component.sass":
/*!*******************************************************************************!*\
  !*** ./src/app/layouts/header/fontsize-toggle/fontsize-toggle.component.sass ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvaGVhZGVyL2ZvbnRzaXplLXRvZ2dsZS9mb250c2l6ZS10b2dnbGUuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/layouts/header/fontsize-toggle/fontsize-toggle.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/layouts/header/fontsize-toggle/fontsize-toggle.component.ts ***!
  \*****************************************************************************/
/*! exports provided: FontsizeToggleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FontsizeToggleComponent", function() { return FontsizeToggleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FontsizeToggleComponent = class FontsizeToggleComponent {
    constructor() {
        this.counter = 0;
        this.sizeChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ngOnInit() {
        this.counter = 0;
    }
    increment(data) {
        this.counter++;
        if (this.counter >= 3) {
            this.counter = 3;
        }
        data = this.counter;
        this.sizeChange.emit(data);
    }
    decrement(data) {
        this.counter--;
        if (this.counter <= -3) {
            this.counter = -3;
        }
        data = this.counter;
        this.sizeChange.emit(data);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
], FontsizeToggleComponent.prototype, "sizeChange", void 0);
FontsizeToggleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-fontsize-toggle",
        template: __webpack_require__(/*! raw-loader!./fontsize-toggle.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/header/fontsize-toggle/fontsize-toggle.component.html"),
        styles: [__webpack_require__(/*! ./fontsize-toggle.component.sass */ "./src/app/layouts/header/fontsize-toggle/fontsize-toggle.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FontsizeToggleComponent);



/***/ }),

/***/ "./src/app/layouts/header/fontsize-toggle/index.ts":
/*!*********************************************************!*\
  !*** ./src/app/layouts/header/fontsize-toggle/index.ts ***!
  \*********************************************************/
/*! exports provided: FontsizeToggleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fontsize_toggle_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fontsize-toggle.component */ "./src/app/layouts/header/fontsize-toggle/fontsize-toggle.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FontsizeToggleComponent", function() { return _fontsize_toggle_component__WEBPACK_IMPORTED_MODULE_0__["FontsizeToggleComponent"]; });




/***/ }),

/***/ "./src/app/layouts/header/header.component.sass":
/*!******************************************************!*\
  !*** ./src/app/layouts/header/header.component.sass ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1, h2, h3, h4,\n.h1, .h2, .h3, .h4 {\n  margin: 0;\n}\nh1:first-child, h2:first-child, h3:first-child, h4:first-child,\n.h1:first-child, .h2:first-child, .h3:first-child, .h4:first-child {\n  margin-top: 0;\n}\nh1:last-child, h2:last-child, h3:last-child, h4:last-child,\n.h1:last-child, .h2:last-child, .h3:last-child, .h4:last-child {\n  margin-bottom: 0;\n}\nh1, .h1 {\n  font-family: \"EPF Book\";\n  font-size: 56px;\n  font-size: 3.5em;\n  line-height: 5.25em;\n}\nh3, .h3 {\n  font-family: \"EPF Medium\";\n  font-size: 26px;\n  font-size: 1.625em;\n  line-height: 2.4375em;\n}\np {\n  width: 100%;\n  font-family: \"EPF Book\";\n  font-size: 16px;\n  font-size: 1em;\n  line-height: 1.5em;\n  transition: all 0.2s ease-in-out;\n}\np:first-child {\n  margin-top: 0;\n}\np:last-child {\n  margin-bottom: 0;\n}\np a {\n  text-decoration: none;\n  transition: all 0.2s ease-in-out;\n  color: inherit;\n}\np a:hover {\n  text-decoration: none;\n  color: inherit;\n}\n.epf-header {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  align-items: center;\n}\n.epf-header__logo {\n  width: 250px;\n  height: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  padding: 0 30px;\n  transition: all 0.2s ease-in-out;\n}\n.epf-header__logo .epf-logo {\n  width: 100px;\n}\n.epf-header__logo .epf-logo img {\n  width: 100%;\n}\n.epf-header__logo span.icon {\n  color: #312783;\n  font-size: 20px;\n  font-size: 1.25em;\n  line-height: 1.875em;\n  cursor: pointer;\n}\n.epf-header__logo span.icon:hover {\n  color: #E30613;\n}\n.epf-header__title {\n  flex-grow: 1;\n  height: 100%;\n  display: flex;\n  align-items: center;\n  padding: 0 30px;\n  border-left: 1px solid #DDDDDD;\n}\n.epf-header__title span.icon {\n  padding-right: 15px;\n  color: #312783;\n  font-size: 20px;\n  font-size: 1.25em;\n  line-height: 1.875em;\n  cursor: pointer;\n}\n.epf-header__title span.icon:hover {\n  color: #E30613;\n}\n.epf-header__title p {\n  color: #666666;\n  font-size: 14px;\n  font-size: 0.875em;\n  line-height: 1.3125em;\n}\n.epf-header__title p strong {\n  color: #312783;\n  font-weight: normal;\n}\n.epf-header__title p span.divider {\n  color: #DDDDDD;\n  padding: 0 20px;\n}\n.epf-header__actions {\n  display: flex;\n  align-items: center;\n  padding: 0 30px;\n}\n.epf-header__actions ul {\n  list-style: none;\n  margin: 0;\n  padding: 0;\n  display: flex;\n  align-items: center;\n}\n.epf-header__actions ul li {\n  margin-left: 30px;\n}\n.epf-header__actions ul li span {\n  color: #312783;\n  font-size: 20px;\n  font-size: 1.25em;\n  line-height: 1.875em;\n}\n.epf-header.toggleNav .epf-header__logo {\n  width: 126px;\n  padding: 0 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0cy9oZWFkZXIvQzpcXERldlxcUHJvamVjdHNcXGV5XFxjbG91ZFxccGF5cm9sbC1wcm90b3R5cGUtLW9wdC1pblxccHJvdG90eXBlL3NyY1xcYXBwXFxsYXlvdXRzXFxoZWFkZXJcXGhlYWRlci5jb21wb25lbnQuc2FzcyIsInNyYy9hcHAvbGF5b3V0cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zYXNzIiwic3JjL2FwcC9sYXlvdXRzL2hlYWRlci9DOlxcRGV2XFxQcm9qZWN0c1xcZXlcXGNsb3VkXFxwYXlyb2xsLXByb3RvdHlwZS0tb3B0LWluXFxwcm90b3R5cGUvc3RkaW4iLCJzcmMvYXBwL2xheW91dHMvaGVhZGVyL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zcmNcXF9zdHlsZXNcXF9jb2xvdXJzLnNhc3MiLCJzcmMvYXBwL2xheW91dHMvaGVhZGVyL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zcmNcXF9zdHlsZXNcXF91dGlscy5zYXNzIiwic3JjL2FwcC9sYXlvdXRzL2hlYWRlci9DOlxcRGV2XFxQcm9qZWN0c1xcZXlcXGNsb3VkXFxwYXlyb2xsLXByb3RvdHlwZS0tb3B0LWluXFxwcm90b3R5cGUvc3JjXFxfc3R5bGVzXFxfdmFyaWFibGVzLnNhc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBaUJBOztFQUVJLFNBQUE7QUNoQko7QURpQkk7O0VBQ0ksYUFBQTtBQ2RSO0FEZUk7O0VBQ0ksZ0JBQUE7QUNaUjtBRGNBO0VBQ0ksdUJBdEJFO0VBUUYsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNJSjtBRFdBO0VBQ0kseUJBdkJNO0VBS04sZUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QUNXSjtBRFFBO0VBQ0ksV0FBQTtFQUNBLHVCQS9CRTtFQVFGLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUF1QkEsZ0NBQUE7QUNISjtBRElJO0VBQ0ksYUFBQTtBQ0ZSO0FER0k7RUFDSSxnQkFBQTtBQ0RSO0FERUk7RUFDSSxxQkFBQTtFQUNBLGdDQUFBO0VBQ0EsY0FBQTtBQ0FSO0FEQ1E7RUFDSSxxQkFBQTtFQUNBLGNBQUE7QUNDWjtBQzVDQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FEK0NKO0FDOUNJO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxnQ0FBQTtBRGdEUjtBQy9DUTtFQUNJLFlBQUE7QURpRFo7QUNoRFk7RUFDSSxXQUFBO0FEa0RoQjtBQ2pEUTtFQUNJLGNDREs7RUhWYixlQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFRVdRLGVBQUE7QURxRFo7QUNwRFk7RUFDSSxjQ3pCTjtBRitFVjtBQ3JESTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLDhCQUFBO0FEdURSO0FDdERRO0VBQ0ksbUJBQUE7RUFDQSxjQ2ZLO0VIVmIsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUV5QlEsZUFBQTtBRDBEWjtBQ3pEWTtFQUNJLGNDdkNOO0FGa0dWO0FDMURRO0VBQ0ksY0MvQkM7RUhBVCxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBQzRGSjtBQzdEWTtFQUNJLGNDeEJDO0VEeUJELG1CQUFBO0FEK0RoQjtBQzlEWTtFQUNJLGNDbkNIO0VEb0NHLGVBQUE7QURnRWhCO0FDL0RJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBRGlFUjtBQ2hFUTtFRXRESixnQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VGc0RRLGFBQUE7RUFDQSxtQkFBQTtBRG9FWjtBQ25FWTtFQUNJLGlCRzNERjtBSmdJZDtBQ3BFZ0I7RUFDSSxjQ3hDSDtFSFZiLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FDeUhKO0FDckVZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUR1RWhCIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gRGVmYXVsdCBzaXppbmdcclxuJGJhc2UtZm9udC1zaXplOiAxNiAhZGVmYXVsdFxyXG4kYmFzZS1saW5lLWhlaWdodDogMS41ICFkZWZhdWx0XHJcblxyXG4kcHJpOiAnRVBGIEJvb2snXHJcbiRwcmktaXQ6ICdFUEYgQm9vayBJdGFsaWMnXHJcbiRwcmktZGVtaTogJ0VQRiBEZW1pJ1xyXG4kcHJpLW1lZDogJ0VQRiBNZWRpdW0nXHJcbiRwcmktaHZ5OiAnRVBGIEhlYXZ5J1xyXG5cclxuLy8gTWl4aW5zXHJcbkBtaXhpbiBmb250LXNpemUoJHNpemUtaW4tcHgsICRsaW5lLWhlaWdodDoxLjUpXHJcbiAgICBmb250LXNpemU6ICRzaXplLWluLXB4ICogMXB4XHJcbiAgICBmb250LXNpemU6ICgkc2l6ZS1pbi1weCAvICRiYXNlLWZvbnQtc2l6ZSkgKiAxZW1cclxuICAgIGxpbmUtaGVpZ2h0OiAoKCRzaXplLWluLXB4IC8gJGJhc2UtZm9udC1zaXplKSAqICRsaW5lLWhlaWdodCkgKiAxZW1cclxuXHJcblxyXG5oMSwgaDIsIGgzLCBoNCxcclxuLmgxLCAuaDIsIC5oMywgLmg0XHJcbiAgICBtYXJnaW46IDBcclxuICAgICY6Zmlyc3QtY2hpbGRcclxuICAgICAgICBtYXJnaW4tdG9wOiAwXHJcbiAgICAmOmxhc3QtY2hpbGRcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwXHJcblxyXG5oMSwgLmgxXHJcbiAgICBmb250LWZhbWlseTogJHByaVxyXG4gICAgQGluY2x1ZGUgZm9udC1zaXplKDU2KVxyXG5cclxuaDMsIC5oM1xyXG4gICAgZm9udC1mYW1pbHk6ICRwcmktbWVkXHJcbiAgICBAaW5jbHVkZSBmb250LXNpemUoMjYpXHJcblxyXG5wXHJcbiAgICB3aWR0aDogMTAwJVxyXG4gICAgZm9udC1mYW1pbHk6ICRwcmlcclxuICAgIEBpbmNsdWRlIGZvbnQtc2l6ZSgxNilcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0XHJcbiAgICAmOmZpcnN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLXRvcDogMFxyXG4gICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICAgYVxyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZVxyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0XHJcbiAgICAgICAgY29sb3I6IGluaGVyaXRcclxuICAgICAgICAmOmhvdmVyXHJcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZVxyXG4gICAgICAgICAgICBjb2xvcjogaW5oZXJpdFxyXG4iLCJoMSwgaDIsIGgzLCBoNCxcbi5oMSwgLmgyLCAuaDMsIC5oNCB7XG4gIG1hcmdpbjogMDtcbn1cbmgxOmZpcnN0LWNoaWxkLCBoMjpmaXJzdC1jaGlsZCwgaDM6Zmlyc3QtY2hpbGQsIGg0OmZpcnN0LWNoaWxkLFxuLmgxOmZpcnN0LWNoaWxkLCAuaDI6Zmlyc3QtY2hpbGQsIC5oMzpmaXJzdC1jaGlsZCwgLmg0OmZpcnN0LWNoaWxkIHtcbiAgbWFyZ2luLXRvcDogMDtcbn1cbmgxOmxhc3QtY2hpbGQsIGgyOmxhc3QtY2hpbGQsIGgzOmxhc3QtY2hpbGQsIGg0Omxhc3QtY2hpbGQsXG4uaDE6bGFzdC1jaGlsZCwgLmgyOmxhc3QtY2hpbGQsIC5oMzpsYXN0LWNoaWxkLCAuaDQ6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbmgxLCAuaDEge1xuICBmb250LWZhbWlseTogXCJFUEYgQm9va1wiO1xuICBmb250LXNpemU6IDU2cHg7XG4gIGZvbnQtc2l6ZTogMy41ZW07XG4gIGxpbmUtaGVpZ2h0OiA1LjI1ZW07XG59XG5cbmgzLCAuaDMge1xuICBmb250LWZhbWlseTogXCJFUEYgTWVkaXVtXCI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgZm9udC1zaXplOiAxLjYyNWVtO1xuICBsaW5lLWhlaWdodDogMi40Mzc1ZW07XG59XG5cbnAge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1mYW1pbHk6IFwiRVBGIEJvb2tcIjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXNpemU6IDFlbTtcbiAgbGluZS1oZWlnaHQ6IDEuNWVtO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbn1cbnA6Zmlyc3QtY2hpbGQge1xuICBtYXJnaW4tdG9wOiAwO1xufVxucDpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbnAgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIGNvbG9yOiBpbmhlcml0O1xufVxucCBhOmhvdmVyIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogaW5oZXJpdDtcbn1cblxuLmVwZi1oZWFkZXIge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmVwZi1oZWFkZXJfX2xvZ28ge1xuICB3aWR0aDogMjUwcHg7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nOiAwIDMwcHg7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xufVxuLmVwZi1oZWFkZXJfX2xvZ28gLmVwZi1sb2dvIHtcbiAgd2lkdGg6IDEwMHB4O1xufVxuLmVwZi1oZWFkZXJfX2xvZ28gLmVwZi1sb2dvIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmVwZi1oZWFkZXJfX2xvZ28gc3Bhbi5pY29uIHtcbiAgY29sb3I6ICMzMTI3ODM7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC1zaXplOiAxLjI1ZW07XG4gIGxpbmUtaGVpZ2h0OiAxLjg3NWVtO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uZXBmLWhlYWRlcl9fbG9nbyBzcGFuLmljb246aG92ZXIge1xuICBjb2xvcjogI0UzMDYxMztcbn1cbi5lcGYtaGVhZGVyX190aXRsZSB7XG4gIGZsZXgtZ3JvdzogMTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nOiAwIDMwcHg7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI0RERERERDtcbn1cbi5lcGYtaGVhZGVyX190aXRsZSBzcGFuLmljb24ge1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICBjb2xvcjogIzMxMjc4MztcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXNpemU6IDEuMjVlbTtcbiAgbGluZS1oZWlnaHQ6IDEuODc1ZW07XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5lcGYtaGVhZGVyX190aXRsZSBzcGFuLmljb246aG92ZXIge1xuICBjb2xvcjogI0UzMDYxMztcbn1cbi5lcGYtaGVhZGVyX190aXRsZSBwIHtcbiAgY29sb3I6ICM2NjY2NjY7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC1zaXplOiAwLjg3NWVtO1xuICBsaW5lLWhlaWdodDogMS4zMTI1ZW07XG59XG4uZXBmLWhlYWRlcl9fdGl0bGUgcCBzdHJvbmcge1xuICBjb2xvcjogIzMxMjc4MztcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbi5lcGYtaGVhZGVyX190aXRsZSBwIHNwYW4uZGl2aWRlciB7XG4gIGNvbG9yOiAjREREREREO1xuICBwYWRkaW5nOiAwIDIwcHg7XG59XG4uZXBmLWhlYWRlcl9fYWN0aW9ucyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDAgMzBweDtcbn1cbi5lcGYtaGVhZGVyX19hY3Rpb25zIHVsIHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmVwZi1oZWFkZXJfX2FjdGlvbnMgdWwgbGkge1xuICBtYXJnaW4tbGVmdDogMzBweDtcbn1cbi5lcGYtaGVhZGVyX19hY3Rpb25zIHVsIGxpIHNwYW4ge1xuICBjb2xvcjogIzMxMjc4MztcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXNpemU6IDEuMjVlbTtcbiAgbGluZS1oZWlnaHQ6IDEuODc1ZW07XG59XG4uZXBmLWhlYWRlci50b2dnbGVOYXYgLmVwZi1oZWFkZXJfX2xvZ28ge1xuICB3aWR0aDogMTI2cHg7XG4gIHBhZGRpbmc6IDAgMTVweDtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vLi4vX3N0eWxlcy9fdXRpbHNcIlxyXG5AaW1wb3J0IFwiLi4vLi4vLi4vX3N0eWxlcy9fY29sb3Vyc1wiXHJcbkBpbXBvcnQgXCIuLi8uLi8uLi9fc3R5bGVzL192YXJpYWJsZXNcIlxyXG5AaW1wb3J0IFwiLi4vLi4vLi4vX3N0eWxlcy9fZm9udHNcIlxyXG5cclxuLmVwZi1oZWFkZXJcclxuICAgIGhlaWdodDogMTAwJVxyXG4gICAgd2lkdGg6IDEwMCVcclxuICAgIGRpc3BsYXk6IGZsZXhcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXJcclxuICAgICZfX2xvZ29cclxuICAgICAgICB3aWR0aDogMjUwcHhcclxuICAgICAgICBoZWlnaHQ6IDEwMCVcclxuICAgICAgICBkaXNwbGF5OiBmbGV4XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlclxyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlblxyXG4gICAgICAgIHBhZGRpbmc6IDAgJHBhZGRpbmctYmlnXHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXRcclxuICAgICAgICAuZXBmLWxvZ29cclxuICAgICAgICAgICAgd2lkdGg6IDEwMHB4XHJcbiAgICAgICAgICAgIGltZ1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCVcclxuICAgICAgICBzcGFuLmljb25cclxuICAgICAgICAgICAgY29sb3I6ICRncmFkaWVudF9ibHVlMVxyXG4gICAgICAgICAgICBAaW5jbHVkZSBmb250LXNpemUoMjApXHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlclxyXG4gICAgICAgICAgICAmOmhvdmVyXHJcbiAgICAgICAgICAgICAgICBjb2xvcjogJGVwZl9yZWRcclxuICAgICZfX3RpdGxlXHJcbiAgICAgICAgZmxleC1ncm93OiAxXHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlXHJcbiAgICAgICAgZGlzcGxheTogZmxleFxyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXJcclxuICAgICAgICBwYWRkaW5nOiAwICRwYWRkaW5nLWJpZ1xyXG4gICAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgJGRhcmtfZ3JleTRcclxuICAgICAgICBzcGFuLmljb25cclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogJHBhZGRpbmdfYmlnLzJcclxuICAgICAgICAgICAgY29sb3I6ICRncmFkaWVudF9ibHVlMVxyXG4gICAgICAgICAgICBAaW5jbHVkZSBmb250LXNpemUoMjApXHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlclxyXG4gICAgICAgICAgICAmOmhvdmVyXHJcbiAgICAgICAgICAgICAgICBjb2xvcjogJGVwZl9yZWRcclxuICAgICAgICBwXHJcbiAgICAgICAgICAgIGNvbG9yOiAkZGFya19ncmV5MlxyXG4gICAgICAgICAgICBAaW5jbHVkZSBmb250LXNpemUoMTQpXHJcbiAgICAgICAgICAgIHN0cm9uZ1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICRncmFkaWVudF9ibHVlMVxyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbFxyXG4gICAgICAgICAgICBzcGFuLmRpdmlkZXJcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAkZGFya19ncmV5NFxyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMCAkcGFkZGluZyoyXHJcbiAgICAmX19hY3Rpb25zXHJcbiAgICAgICAgZGlzcGxheTogZmxleFxyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXJcclxuICAgICAgICBwYWRkaW5nOiAwICRwYWRkaW5nLWJpZ1xyXG4gICAgICAgIHVsXHJcbiAgICAgICAgICAgIEBpbmNsdWRlIGxpc3QtcmVzZXQoKVxyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXJcclxuICAgICAgICAgICAgbGlcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAkcGFkZGluZy1iaWdcclxuICAgICAgICAgICAgICAgIHNwYW5cclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJGdyYWRpZW50X2JsdWUxXHJcbiAgICAgICAgICAgICAgICAgICAgQGluY2x1ZGUgZm9udC1zaXplKDIwKVxyXG4gICAgJi50b2dnbGVOYXZcclxuICAgICAgICAuZXBmLWhlYWRlclxyXG4gICAgICAgICAgICAmX19sb2dvXHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTI2cHhcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDAgJHBhZGRpbmctYmlnLzIiLCIvLyBNYWluIENvbG91cnNcclxuJGVwZl9ibHVlOiAjMzEyNzgyXHJcbiRlcGZfcmVkOiAjRTMwNjEzXHJcbiRlcGZfZ29sZDogI0Q0QTkzOVxyXG5cclxuLy8gU3VwcG9ydGl2ZSBDb2xvdXJzXHJcbiRibHVlMTogIzQxMzFCOFxyXG4kYmx1ZTI6ICM1MDNGRENcclxuJGJsdWUzOiAjRjZGQUZFXHJcblxyXG4vLyBOZXV0cmFsIENvbG91cnNcclxuJGRhcmtfZ3JleTE6ICMzMzMzMzNcclxuJGRhcmtfZ3JleTI6ICM2NjY2NjZcclxuJGRhcmtfZ3JleTM6ICM5OTk5OTlcclxuJGRhcmtfZ3JleTQ6ICNERERERERcclxuJGxpZ2h0X2dyZXkxOiAjRjRGNEY0XHJcbiRsaWdodF9ncmV5MjogI0Y4RjhGOFxyXG4kbGlnaHRfZ3JleTM6ICNGQ0ZDRkNcclxuXHJcbiR3aGl0ZTogI2ZmZmZmZlxyXG4kYmxhY2s6ICMwMDAwMDBcclxuXHJcbiRncmFkaWVudF9ibHVlMTogIzMxMjc4M1xyXG4kZ3JhZGllbnRfYmx1ZTI6ICMzODI4QkIiLCJAbWl4aW4gbGlzdC1yZXNldCgpXHJcbiAgICBsaXN0LXN0eWxlOiBub25lXHJcbiAgICBtYXJnaW46IDBcclxuICAgIHBhZGRpbmc6IDAiLCIvLyBwYWRkaW5nXHJcbiRwYWRkaW5nX2JpZzogMzBweFxyXG4kcGFkZGluZzogMTBweFxyXG5cclxuLy8gbWFyZ2luXHJcbiRtYXJnaW5fYmlnOiAzMHB4XHJcbiRtYXJnaW46IDEwcHgiXX0= */"

/***/ }),

/***/ "./src/app/layouts/header/header.component.ts":
/*!****************************************************!*\
  !*** ./src/app/layouts/header/header.component.ts ***!
  \****************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared */ "./src/app/shared/index.ts");



let HeaderComponent = class HeaderComponent {
    constructor(optinService) {
        this.optinService = optinService;
        this.sizeClass = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.toggleNav = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.className = {
            plus: {
                one: "fontsize__plus__small",
                two: "fontsize__plus__medium",
                three: "fontsize__plus__large"
            },
            normal: "fontsize__normal",
            minus: {
                one: "fontsize__minus__small",
                two: "fontsize__minus__medium",
                three: "fontsize__minus__large"
            }
        };
        this.optin = {};
        this.state = this.className.normal;
        this.togglestate = false;
    }
    ngOnInit() {
        this.getOptIn();
    }
    getSize(fontsize) {
        switch (fontsize) {
            case 1:
                this.state = this.className.plus.one;
                break;
            case 2:
                this.state = this.className.plus.two;
                break;
            case 3:
                this.state = this.className.plus.three;
                break;
            case 0:
                this.state = this.className.normal;
                break;
            case -1:
                this.state = this.className.minus.one;
                break;
            case -2:
                this.state = this.className.minus.two;
                break;
            case -3:
                this.state = this.className.minus.three;
                break;
            default:
                this.state = this.state;
        }
        this.sizeClass.emit(this.state);
    }
    sendingToggleNav() {
        this.togglestate = !this.togglestate;
        this.toggleNav.emit(this.togglestate);
    }
    getOptIn() {
        this.optin = {};
        this.optinService.getEmployerEPF().subscribe((data) => {
            console.log(data);
            this.optin = data;
        });
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
], HeaderComponent.prototype, "sizeClass", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
], HeaderComponent.prototype, "toggleNav", void 0);
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-header",
        template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/header/header.component.html"),
        styles: [__webpack_require__(/*! ./header.component.sass */ "./src/app/layouts/header/header.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared__WEBPACK_IMPORTED_MODULE_2__["OptinService"]])
], HeaderComponent);



/***/ }),

/***/ "./src/app/layouts/header/index.ts":
/*!*****************************************!*\
  !*** ./src/app/layouts/header/index.ts ***!
  \*****************************************/
/*! exports provided: HeaderComponent, LanguageToggleComponent, FontsizeToggleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _header_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.component */ "./src/app/layouts/header/header.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return _header_component__WEBPACK_IMPORTED_MODULE_0__["HeaderComponent"]; });

/* harmony import */ var _language_toggle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./language-toggle */ "./src/app/layouts/header/language-toggle/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LanguageToggleComponent", function() { return _language_toggle__WEBPACK_IMPORTED_MODULE_1__["LanguageToggleComponent"]; });

/* harmony import */ var _fontsize_toggle__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fontsize-toggle */ "./src/app/layouts/header/fontsize-toggle/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FontsizeToggleComponent", function() { return _fontsize_toggle__WEBPACK_IMPORTED_MODULE_2__["FontsizeToggleComponent"]; });






/***/ }),

/***/ "./src/app/layouts/header/language-toggle/index.ts":
/*!*********************************************************!*\
  !*** ./src/app/layouts/header/language-toggle/index.ts ***!
  \*********************************************************/
/*! exports provided: LanguageToggleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _language_toggle_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./language-toggle.component */ "./src/app/layouts/header/language-toggle/language-toggle.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LanguageToggleComponent", function() { return _language_toggle_component__WEBPACK_IMPORTED_MODULE_0__["LanguageToggleComponent"]; });




/***/ }),

/***/ "./src/app/layouts/header/language-toggle/language-toggle.component.sass":
/*!*******************************************************************************!*\
  !*** ./src/app/layouts/header/language-toggle/language-toggle.component.sass ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1, h2, h3, h4,\n.h1, .h2, .h3, .h4 {\n  margin: 0;\n}\nh1:first-child, h2:first-child, h3:first-child, h4:first-child,\n.h1:first-child, .h2:first-child, .h3:first-child, .h4:first-child {\n  margin-top: 0;\n}\nh1:last-child, h2:last-child, h3:last-child, h4:last-child,\n.h1:last-child, .h2:last-child, .h3:last-child, .h4:last-child {\n  margin-bottom: 0;\n}\nh1, .h1 {\n  font-family: \"EPF Book\";\n  font-size: 56px;\n  font-size: 3.5em;\n  line-height: 5.25em;\n}\nh3, .h3 {\n  font-family: \"EPF Medium\";\n  font-size: 26px;\n  font-size: 1.625em;\n  line-height: 2.4375em;\n}\np {\n  width: 100%;\n  font-family: \"EPF Book\";\n  font-size: 16px;\n  font-size: 1em;\n  line-height: 1.5em;\n  transition: all 0.2s ease-in-out;\n}\np:first-child {\n  margin-top: 0;\n}\np:last-child {\n  margin-bottom: 0;\n}\np a {\n  text-decoration: none;\n  transition: all 0.2s ease-in-out;\n  color: inherit;\n}\np a:hover {\n  text-decoration: none;\n  color: inherit;\n}\n.epf-languagetoggle {\n  display: flex;\n}\n.epf-languagetoggle span {\n  text-transform: uppercase;\n  font-size: 14px;\n  font-size: 0.875em;\n  line-height: 1.3125em;\n}\n.epf-languagetoggle span.languagebtn {\n  color: #666666;\n  cursor: pointer;\n}\n.epf-languagetoggle span.languagebtn.active, .epf-languagetoggle span.languagebtn:hover {\n  color: #E30613;\n}\n.epf-languagetoggle span.divider {\n  padding: 0 10px;\n  color: #666666;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0cy9oZWFkZXIvbGFuZ3VhZ2UtdG9nZ2xlL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zcmNcXGFwcFxcbGF5b3V0c1xcaGVhZGVyXFxsYW5ndWFnZS10b2dnbGVcXGxhbmd1YWdlLXRvZ2dsZS5jb21wb25lbnQuc2FzcyIsInNyYy9hcHAvbGF5b3V0cy9oZWFkZXIvbGFuZ3VhZ2UtdG9nZ2xlL2xhbmd1YWdlLXRvZ2dsZS5jb21wb25lbnQuc2FzcyIsInNyYy9hcHAvbGF5b3V0cy9oZWFkZXIvbGFuZ3VhZ2UtdG9nZ2xlL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zdGRpbiIsInNyYy9hcHAvbGF5b3V0cy9oZWFkZXIvbGFuZ3VhZ2UtdG9nZ2xlL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zcmNcXF9zdHlsZXNcXF9jb2xvdXJzLnNhc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBaUJBOztFQUVJLFNBQUE7QUNoQko7QURpQkk7O0VBQ0ksYUFBQTtBQ2RSO0FEZUk7O0VBQ0ksZ0JBQUE7QUNaUjtBRGNBO0VBQ0ksdUJBdEJFO0VBUUYsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNJSjtBRFdBO0VBQ0kseUJBdkJNO0VBS04sZUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QUNXSjtBRFFBO0VBQ0ksV0FBQTtFQUNBLHVCQS9CRTtFQVFGLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUF1QkEsZ0NBQUE7QUNISjtBRElJO0VBQ0ksYUFBQTtBQ0ZSO0FER0k7RUFDSSxnQkFBQTtBQ0RSO0FERUk7RUFDSSxxQkFBQTtFQUNBLGdDQUFBO0VBQ0EsY0FBQTtBQ0FSO0FEQ1E7RUFDSSxxQkFBQTtFQUNBLGNBQUE7QUNDWjtBQzVDQTtFQUNJLGFBQUE7QUQrQ0o7QUM5Q0k7RUFDSSx5QkFBQTtFRklKLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0FDNkNKO0FDakRRO0VBQ0ksY0NDQztFREFELGVBQUE7QURtRFo7QUNsRFk7RUFDSSxjQ1pOO0FGZ0VWO0FDbkRRO0VBQ0ksZUFBQTtFQUNBLGNDTEM7QUYwRGIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXRzL2hlYWRlci9sYW5ndWFnZS10b2dnbGUvbGFuZ3VhZ2UtdG9nZ2xlLmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gRGVmYXVsdCBzaXppbmdcclxuJGJhc2UtZm9udC1zaXplOiAxNiAhZGVmYXVsdFxyXG4kYmFzZS1saW5lLWhlaWdodDogMS41ICFkZWZhdWx0XHJcblxyXG4kcHJpOiAnRVBGIEJvb2snXHJcbiRwcmktaXQ6ICdFUEYgQm9vayBJdGFsaWMnXHJcbiRwcmktZGVtaTogJ0VQRiBEZW1pJ1xyXG4kcHJpLW1lZDogJ0VQRiBNZWRpdW0nXHJcbiRwcmktaHZ5OiAnRVBGIEhlYXZ5J1xyXG5cclxuLy8gTWl4aW5zXHJcbkBtaXhpbiBmb250LXNpemUoJHNpemUtaW4tcHgsICRsaW5lLWhlaWdodDoxLjUpXHJcbiAgICBmb250LXNpemU6ICRzaXplLWluLXB4ICogMXB4XHJcbiAgICBmb250LXNpemU6ICgkc2l6ZS1pbi1weCAvICRiYXNlLWZvbnQtc2l6ZSkgKiAxZW1cclxuICAgIGxpbmUtaGVpZ2h0OiAoKCRzaXplLWluLXB4IC8gJGJhc2UtZm9udC1zaXplKSAqICRsaW5lLWhlaWdodCkgKiAxZW1cclxuXHJcblxyXG5oMSwgaDIsIGgzLCBoNCxcclxuLmgxLCAuaDIsIC5oMywgLmg0XHJcbiAgICBtYXJnaW46IDBcclxuICAgICY6Zmlyc3QtY2hpbGRcclxuICAgICAgICBtYXJnaW4tdG9wOiAwXHJcbiAgICAmOmxhc3QtY2hpbGRcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwXHJcblxyXG5oMSwgLmgxXHJcbiAgICBmb250LWZhbWlseTogJHByaVxyXG4gICAgQGluY2x1ZGUgZm9udC1zaXplKDU2KVxyXG5cclxuaDMsIC5oM1xyXG4gICAgZm9udC1mYW1pbHk6ICRwcmktbWVkXHJcbiAgICBAaW5jbHVkZSBmb250LXNpemUoMjYpXHJcblxyXG5wXHJcbiAgICB3aWR0aDogMTAwJVxyXG4gICAgZm9udC1mYW1pbHk6ICRwcmlcclxuICAgIEBpbmNsdWRlIGZvbnQtc2l6ZSgxNilcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0XHJcbiAgICAmOmZpcnN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLXRvcDogMFxyXG4gICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICAgYVxyXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZVxyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0XHJcbiAgICAgICAgY29sb3I6IGluaGVyaXRcclxuICAgICAgICAmOmhvdmVyXHJcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZVxyXG4gICAgICAgICAgICBjb2xvcjogaW5oZXJpdFxyXG4iLCJoMSwgaDIsIGgzLCBoNCxcbi5oMSwgLmgyLCAuaDMsIC5oNCB7XG4gIG1hcmdpbjogMDtcbn1cbmgxOmZpcnN0LWNoaWxkLCBoMjpmaXJzdC1jaGlsZCwgaDM6Zmlyc3QtY2hpbGQsIGg0OmZpcnN0LWNoaWxkLFxuLmgxOmZpcnN0LWNoaWxkLCAuaDI6Zmlyc3QtY2hpbGQsIC5oMzpmaXJzdC1jaGlsZCwgLmg0OmZpcnN0LWNoaWxkIHtcbiAgbWFyZ2luLXRvcDogMDtcbn1cbmgxOmxhc3QtY2hpbGQsIGgyOmxhc3QtY2hpbGQsIGgzOmxhc3QtY2hpbGQsIGg0Omxhc3QtY2hpbGQsXG4uaDE6bGFzdC1jaGlsZCwgLmgyOmxhc3QtY2hpbGQsIC5oMzpsYXN0LWNoaWxkLCAuaDQ6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbmgxLCAuaDEge1xuICBmb250LWZhbWlseTogXCJFUEYgQm9va1wiO1xuICBmb250LXNpemU6IDU2cHg7XG4gIGZvbnQtc2l6ZTogMy41ZW07XG4gIGxpbmUtaGVpZ2h0OiA1LjI1ZW07XG59XG5cbmgzLCAuaDMge1xuICBmb250LWZhbWlseTogXCJFUEYgTWVkaXVtXCI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgZm9udC1zaXplOiAxLjYyNWVtO1xuICBsaW5lLWhlaWdodDogMi40Mzc1ZW07XG59XG5cbnAge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1mYW1pbHk6IFwiRVBGIEJvb2tcIjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXNpemU6IDFlbTtcbiAgbGluZS1oZWlnaHQ6IDEuNWVtO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbn1cbnA6Zmlyc3QtY2hpbGQge1xuICBtYXJnaW4tdG9wOiAwO1xufVxucDpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbnAgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gIGNvbG9yOiBpbmhlcml0O1xufVxucCBhOmhvdmVyIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogaW5oZXJpdDtcbn1cblxuLmVwZi1sYW5ndWFnZXRvZ2dsZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uZXBmLWxhbmd1YWdldG9nZ2xlIHNwYW4ge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtc2l6ZTogMC44NzVlbTtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNWVtO1xufVxuLmVwZi1sYW5ndWFnZXRvZ2dsZSBzcGFuLmxhbmd1YWdlYnRuIHtcbiAgY29sb3I6ICM2NjY2NjY7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5lcGYtbGFuZ3VhZ2V0b2dnbGUgc3Bhbi5sYW5ndWFnZWJ0bi5hY3RpdmUsIC5lcGYtbGFuZ3VhZ2V0b2dnbGUgc3Bhbi5sYW5ndWFnZWJ0bjpob3ZlciB7XG4gIGNvbG9yOiAjRTMwNjEzO1xufVxuLmVwZi1sYW5ndWFnZXRvZ2dsZSBzcGFuLmRpdmlkZXIge1xuICBwYWRkaW5nOiAwIDEwcHg7XG4gIGNvbG9yOiAjNjY2NjY2O1xufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9fc3R5bGVzL191dGlsc1wiXHJcbkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9fc3R5bGVzL19jb2xvdXJzXCJcclxuQGltcG9ydCBcIi4uLy4uLy4uLy4uL19zdHlsZXMvX3ZhcmlhYmxlc1wiXHJcbkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9fc3R5bGVzL19mb250c1wiXHJcblxyXG4uZXBmLWxhbmd1YWdldG9nZ2xlXHJcbiAgICBkaXNwbGF5OiBmbGV4XHJcbiAgICBzcGFuXHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVxyXG4gICAgICAgIEBpbmNsdWRlIGZvbnQtc2l6ZSgxNClcclxuICAgICAgICAmLmxhbmd1YWdlYnRuXHJcbiAgICAgICAgICAgIGNvbG9yOiAkZGFya19ncmV5MlxyXG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXJcclxuICAgICAgICAgICAgJi5hY3RpdmUsICY6aG92ZXJcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAkZXBmX3JlZCBcclxuICAgICAgICAmLmRpdmlkZXJcclxuICAgICAgICAgICAgcGFkZGluZzogMCAkcGFkZGluZ1xyXG4gICAgICAgICAgICBjb2xvcjogJGRhcmtfZ3JleTIiLCIvLyBNYWluIENvbG91cnNcclxuJGVwZl9ibHVlOiAjMzEyNzgyXHJcbiRlcGZfcmVkOiAjRTMwNjEzXHJcbiRlcGZfZ29sZDogI0Q0QTkzOVxyXG5cclxuLy8gU3VwcG9ydGl2ZSBDb2xvdXJzXHJcbiRibHVlMTogIzQxMzFCOFxyXG4kYmx1ZTI6ICM1MDNGRENcclxuJGJsdWUzOiAjRjZGQUZFXHJcblxyXG4vLyBOZXV0cmFsIENvbG91cnNcclxuJGRhcmtfZ3JleTE6ICMzMzMzMzNcclxuJGRhcmtfZ3JleTI6ICM2NjY2NjZcclxuJGRhcmtfZ3JleTM6ICM5OTk5OTlcclxuJGRhcmtfZ3JleTQ6ICNERERERERcclxuJGxpZ2h0X2dyZXkxOiAjRjRGNEY0XHJcbiRsaWdodF9ncmV5MjogI0Y4RjhGOFxyXG4kbGlnaHRfZ3JleTM6ICNGQ0ZDRkNcclxuXHJcbiR3aGl0ZTogI2ZmZmZmZlxyXG4kYmxhY2s6ICMwMDAwMDBcclxuXHJcbiRncmFkaWVudF9ibHVlMTogIzMxMjc4M1xyXG4kZ3JhZGllbnRfYmx1ZTI6ICMzODI4QkIiXX0= */"

/***/ }),

/***/ "./src/app/layouts/header/language-toggle/language-toggle.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/layouts/header/language-toggle/language-toggle.component.ts ***!
  \*****************************************************************************/
/*! exports provided: LanguageToggleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageToggleComponent", function() { return LanguageToggleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");



let LanguageToggleComponent = class LanguageToggleComponent {
    constructor(translate) {
        this.translate = translate;
        this.toggle = false;
    }
    ngOnInit() { }
    useLanguage(language) {
        this.translate.use(language);
        this.toggle != this.toggle;
    }
};
LanguageToggleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-language-toggle",
        template: __webpack_require__(/*! raw-loader!./language-toggle.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/header/language-toggle/language-toggle.component.html"),
        styles: [__webpack_require__(/*! ./language-toggle.component.sass */ "./src/app/layouts/header/language-toggle/language-toggle.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])
], LanguageToggleComponent);



/***/ }),

/***/ "./src/app/layouts/index.ts":
/*!**********************************!*\
  !*** ./src/app/layouts/index.ts ***!
  \**********************************/
/*! exports provided: HeaderComponent, NavigationComponent, FooterComponent, ErrorPageComponent, RoutingSpinnerComponent, LanguageToggleComponent, FontsizeToggleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header */ "./src/app/layouts/header/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return _header__WEBPACK_IMPORTED_MODULE_0__["HeaderComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LanguageToggleComponent", function() { return _header__WEBPACK_IMPORTED_MODULE_0__["LanguageToggleComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FontsizeToggleComponent", function() { return _header__WEBPACK_IMPORTED_MODULE_0__["FontsizeToggleComponent"]; });

/* harmony import */ var _navigation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navigation */ "./src/app/layouts/navigation/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return _navigation__WEBPACK_IMPORTED_MODULE_1__["NavigationComponent"]; });

/* harmony import */ var _footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./footer */ "./src/app/layouts/footer/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return _footer__WEBPACK_IMPORTED_MODULE_2__["FooterComponent"]; });

/* harmony import */ var _error_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./error-page */ "./src/app/layouts/error-page/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ErrorPageComponent", function() { return _error_page__WEBPACK_IMPORTED_MODULE_3__["ErrorPageComponent"]; });

/* harmony import */ var _routing_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./routing-spinner */ "./src/app/layouts/routing-spinner/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RoutingSpinnerComponent", function() { return _routing_spinner__WEBPACK_IMPORTED_MODULE_4__["RoutingSpinnerComponent"]; });








/***/ }),

/***/ "./src/app/layouts/navigation/index.ts":
/*!*********************************************!*\
  !*** ./src/app/layouts/navigation/index.ts ***!
  \*********************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navigation.component */ "./src/app/layouts/navigation/navigation.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return _navigation_component__WEBPACK_IMPORTED_MODULE_0__["NavigationComponent"]; });




/***/ }),

/***/ "./src/app/layouts/navigation/navigation.component.sass":
/*!**************************************************************!*\
  !*** ./src/app/layouts/navigation/navigation.component.sass ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1, h2, h3, h4,\n.h1, .h2, .h3, .h4 {\n  margin: 0;\n}\nh1:first-child, h2:first-child, h3:first-child, h4:first-child,\n.h1:first-child, .h2:first-child, .h3:first-child, .h4:first-child {\n  margin-top: 0;\n}\nh1:last-child, h2:last-child, h3:last-child, h4:last-child,\n.h1:last-child, .h2:last-child, .h3:last-child, .h4:last-child {\n  margin-bottom: 0;\n}\nh1, .h1 {\n  font-family: \"EPF Book\";\n  font-size: 56px;\n  font-size: 3.5em;\n  line-height: 5.25em;\n}\nh3, .h3 {\n  font-family: \"EPF Medium\";\n  font-size: 26px;\n  font-size: 1.625em;\n  line-height: 2.4375em;\n}\np {\n  width: 100%;\n  font-family: \"EPF Book\";\n  font-size: 16px;\n  font-size: 1em;\n  line-height: 1.5em;\n  transition: all 0.2s ease-in-out;\n}\np:first-child {\n  margin-top: 0;\n}\np:last-child {\n  margin-bottom: 0;\n}\np a {\n  text-decoration: none;\n  transition: all 0.2s ease-in-out;\n  color: inherit;\n}\np a:hover {\n  text-decoration: none;\n  color: inherit;\n}\n.epf-navigation {\n  width: 100%;\n  height: 100%;\n  background-image: url(\"/assets/images/nav_bg.png\");\n  background-repeat: no-repeat;\n  background-position: center bottom;\n  background-size: 100%;\n  padding-top: 15px;\n}\n.epf-navigation ul {\n  width: 100%;\n  list-style: none;\n  margin: 0;\n  padding: 0;\n  position: relative;\n}\n.epf-navigation ul li {\n  color: #ffffff;\n}\n.epf-navigation ul li p {\n  text-align: left;\n  transition: all 0.2s ease-in-out;\n}\n.epf-navigation ul li p a {\n  display: inline-block;\n  padding: 30px;\n  font-size: 16px;\n  font-size: 1em;\n  line-height: 1.5em;\n}\n.epf-navigation ul li p a span.icon {\n  position: relative;\n  padding-right: 10px;\n}\n.epf-navigation ul li p a span.icon:before {\n  z-index: 1;\n}\n.epf-navigation ul li p a span.icon:after {\n  content: \"\";\n  width: 0px;\n  height: 0px;\n  position: absolute;\n  z-index: -1;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translateX(-50%) translateY(-50%);\n          transform: translateX(-50%) translateY(-50%);\n  border-radius: 50%;\n  background-color: #312782;\n  transition: all 0.2s ease-in-out;\n}\n.epf-navigation ul li.active, .epf-navigation ul li:hover {\n  color: #D4A939;\n}\n.epf-navigation ul li:last-child:after {\n  content: \"\";\n  width: 4px;\n  height: 24px;\n  background: #D4A939;\n  position: absolute;\n  pointer-events: none;\n  transition: top 0.4s ease-in-out;\n  border-radius: 2px;\n  top: calc(100% - 84px);\n  right: 0;\n  margin-top: 30px;\n  z-index: -1;\n}\n.epf-navigation ul li:nth-child(1).active ~ li:last-child:after {\n  top: 0%;\n}\n.epf-navigation ul li:nth-child(2).active ~ li:last-child:after {\n  top: 20%;\n}\n.epf-navigation ul li:nth-child(3).active ~ li:last-child:after {\n  top: 40%;\n}\n.epf-navigation ul li:nth-child(4).active ~ li:last-child:after {\n  top: 60%;\n}\n:host.toggleNav .epf-navigation ul li:last-child:after {\n  display: none;\n}\n:host.toggleNav .epf-navigation ul li p {\n  text-align: center;\n}\n:host.toggleNav .epf-navigation ul li p a {\n  padding: 30px 40px;\n}\n:host.toggleNav .epf-navigation ul li p a span.text {\n  display: none;\n}\n:host.toggleNav .epf-navigation ul li p a span.icon {\n  padding: 0;\n}\n:host.toggleNav .epf-navigation ul li p a span.icon:after {\n  width: 50px;\n  height: 50px;\n}\n:host.toggleNav .epf-navigation ul li.active p {\n  color: #ffffff;\n}\n:host.toggleNav .epf-navigation ul li.active p a span.icon:after {\n  background-color: #D4A939;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0cy9uYXZpZ2F0aW9uL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zcmNcXGFwcFxcbGF5b3V0c1xcbmF2aWdhdGlvblxcbmF2aWdhdGlvbi5jb21wb25lbnQuc2FzcyIsInNyYy9hcHAvbGF5b3V0cy9uYXZpZ2F0aW9uL25hdmlnYXRpb24uY29tcG9uZW50LnNhc3MiLCJzcmMvYXBwL2xheW91dHMvbmF2aWdhdGlvbi9DOlxcRGV2XFxQcm9qZWN0c1xcZXlcXGNsb3VkXFxwYXlyb2xsLXByb3RvdHlwZS0tb3B0LWluXFxwcm90b3R5cGUvc3RkaW4iLCJzcmMvYXBwL2xheW91dHMvbmF2aWdhdGlvbi9DOlxcRGV2XFxQcm9qZWN0c1xcZXlcXGNsb3VkXFxwYXlyb2xsLXByb3RvdHlwZS0tb3B0LWluXFxwcm90b3R5cGUvc3JjXFxfc3R5bGVzXFxfdXRpbHMuc2FzcyIsInNyYy9hcHAvbGF5b3V0cy9uYXZpZ2F0aW9uL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zcmNcXF9zdHlsZXNcXF9jb2xvdXJzLnNhc3MiLCJzcmMvYXBwL2xheW91dHMvbmF2aWdhdGlvbi9DOlxcRGV2XFxQcm9qZWN0c1xcZXlcXGNsb3VkXFxwYXlyb2xsLXByb3RvdHlwZS0tb3B0LWluXFxwcm90b3R5cGUvc3JjXFxfc3R5bGVzXFxfdmFyaWFibGVzLnNhc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBaUJBOztFQUVJLFNBQUE7QUNoQko7QURpQkk7O0VBQ0ksYUFBQTtBQ2RSO0FEZUk7O0VBQ0ksZ0JBQUE7QUNaUjtBRGNBO0VBQ0ksdUJBdEJFO0VBUUYsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNJSjtBRFdBO0VBQ0kseUJBdkJNO0VBS04sZUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QUNXSjtBRFFBO0VBQ0ksV0FBQTtFQUNBLHVCQS9CRTtFQVFGLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUF1QkEsZ0NBQUE7QUNISjtBRElJO0VBQ0ksYUFBQTtBQ0ZSO0FER0k7RUFDSSxnQkFBQTtBQ0RSO0FERUk7RUFDSSxxQkFBQTtFQUNBLGdDQUFBO0VBQ0EsY0FBQTtBQ0FSO0FEQ1E7RUFDSSxxQkFBQTtFQUNBLGNBQUE7QUNDWjtBQ3ZDQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0RBQUE7RUFDQSw0QkFBQTtFQUNBLGtDQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtBRDBDSjtBQ3pDSTtFQUNJLFdBQUE7RUNsQkosZ0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFRGtCSSxrQkFBQTtBRDZDUjtBQzVDUTtFQUNJLGNFSko7QUhrRFI7QUM3Q1k7RUFDSSxnQkFBQTtFQUNBLGdDQUFBO0FEK0NoQjtBQzlDZ0I7RUFDSSxxQkFBQTtFQUNBLGFHNUJOO0VMV1YsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQ2tFSjtBQ2pEb0I7RUFDSSxrQkFBQTtFQUNBLG1CRy9CZDtBSmtGVjtBQ2xEd0I7RUFDSSxVQUFBO0FEb0Q1QjtBQ25Ed0I7RUFDSSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLG9EQUFBO1VBQUEsNENBQUE7RUFDQSxrQkFBQTtFQUNBLHlCRTdDakI7RUY4Q2lCLGdDQUFBO0FEcUQ1QjtBQ3BEWTtFQUNJLGNFOUNMO0FIb0dYO0FDcERnQjtFQUNJLFdBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLG1CRXBEVDtFRnFEUyxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0NBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsUUFBQTtFQUNBLGdCRzdETjtFSDhETSxXQUFBO0FEc0RwQjtBQ3BEWTtFQUNJLE9BQUE7QURzRGhCO0FDdkRZO0VBQ0ksUUFBQTtBRHlEaEI7QUMxRFk7RUFDSSxRQUFBO0FENERoQjtBQzdEWTtFQUNJLFFBQUE7QUQrRGhCO0FDeERvQjtFQUNJLGFBQUE7QUQyRHhCO0FDekRnQjtFQUNJLGtCQUFBO0FEMkRwQjtBQzFEb0I7RUFDSSxrQkFBQTtBRDREeEI7QUMzRHdCO0VBQ0ksYUFBQTtBRDZENUI7QUM1RHdCO0VBQ0ksVUFBQTtBRDhENUI7QUM3RDRCO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUQrRGhDO0FDN0RnQjtFQUNJLGNFdEVaO0FIcUlSO0FDNUQ0QjtFQUNJLHlCRTFGckI7QUh3SlgiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnQuc2FzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIERlZmF1bHQgc2l6aW5nXHJcbiRiYXNlLWZvbnQtc2l6ZTogMTYgIWRlZmF1bHRcclxuJGJhc2UtbGluZS1oZWlnaHQ6IDEuNSAhZGVmYXVsdFxyXG5cclxuJHByaTogJ0VQRiBCb29rJ1xyXG4kcHJpLWl0OiAnRVBGIEJvb2sgSXRhbGljJ1xyXG4kcHJpLWRlbWk6ICdFUEYgRGVtaSdcclxuJHByaS1tZWQ6ICdFUEYgTWVkaXVtJ1xyXG4kcHJpLWh2eTogJ0VQRiBIZWF2eSdcclxuXHJcbi8vIE1peGluc1xyXG5AbWl4aW4gZm9udC1zaXplKCRzaXplLWluLXB4LCAkbGluZS1oZWlnaHQ6MS41KVxyXG4gICAgZm9udC1zaXplOiAkc2l6ZS1pbi1weCAqIDFweFxyXG4gICAgZm9udC1zaXplOiAoJHNpemUtaW4tcHggLyAkYmFzZS1mb250LXNpemUpICogMWVtXHJcbiAgICBsaW5lLWhlaWdodDogKCgkc2l6ZS1pbi1weCAvICRiYXNlLWZvbnQtc2l6ZSkgKiAkbGluZS1oZWlnaHQpICogMWVtXHJcblxyXG5cclxuaDEsIGgyLCBoMywgaDQsXHJcbi5oMSwgLmgyLCAuaDMsIC5oNFxyXG4gICAgbWFyZ2luOiAwXHJcbiAgICAmOmZpcnN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLXRvcDogMFxyXG4gICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG5cclxuaDEsIC5oMVxyXG4gICAgZm9udC1mYW1pbHk6ICRwcmlcclxuICAgIEBpbmNsdWRlIGZvbnQtc2l6ZSg1NilcclxuXHJcbmgzLCAuaDNcclxuICAgIGZvbnQtZmFtaWx5OiAkcHJpLW1lZFxyXG4gICAgQGluY2x1ZGUgZm9udC1zaXplKDI2KVxyXG5cclxucFxyXG4gICAgd2lkdGg6IDEwMCVcclxuICAgIGZvbnQtZmFtaWx5OiAkcHJpXHJcbiAgICBAaW5jbHVkZSBmb250LXNpemUoMTYpXHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dFxyXG4gICAgJjpmaXJzdC1jaGlsZFxyXG4gICAgICAgIG1hcmdpbi10b3A6IDBcclxuICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBcclxuICAgIGFcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmVcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dFxyXG4gICAgICAgIGNvbG9yOiBpbmhlcml0XHJcbiAgICAgICAgJjpob3ZlclxyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmVcclxuICAgICAgICAgICAgY29sb3I6IGluaGVyaXRcclxuIiwiaDEsIGgyLCBoMywgaDQsXG4uaDEsIC5oMiwgLmgzLCAuaDQge1xuICBtYXJnaW46IDA7XG59XG5oMTpmaXJzdC1jaGlsZCwgaDI6Zmlyc3QtY2hpbGQsIGgzOmZpcnN0LWNoaWxkLCBoNDpmaXJzdC1jaGlsZCxcbi5oMTpmaXJzdC1jaGlsZCwgLmgyOmZpcnN0LWNoaWxkLCAuaDM6Zmlyc3QtY2hpbGQsIC5oNDpmaXJzdC1jaGlsZCB7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5oMTpsYXN0LWNoaWxkLCBoMjpsYXN0LWNoaWxkLCBoMzpsYXN0LWNoaWxkLCBoNDpsYXN0LWNoaWxkLFxuLmgxOmxhc3QtY2hpbGQsIC5oMjpsYXN0LWNoaWxkLCAuaDM6bGFzdC1jaGlsZCwgLmg0Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG5oMSwgLmgxIHtcbiAgZm9udC1mYW1pbHk6IFwiRVBGIEJvb2tcIjtcbiAgZm9udC1zaXplOiA1NnB4O1xuICBmb250LXNpemU6IDMuNWVtO1xuICBsaW5lLWhlaWdodDogNS4yNWVtO1xufVxuXG5oMywgLmgzIHtcbiAgZm9udC1mYW1pbHk6IFwiRVBGIE1lZGl1bVwiO1xuICBmb250LXNpemU6IDI2cHg7XG4gIGZvbnQtc2l6ZTogMS42MjVlbTtcbiAgbGluZS1oZWlnaHQ6IDIuNDM3NWVtO1xufVxuXG5wIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtZmFtaWx5OiBcIkVQRiBCb29rXCI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC1zaXplOiAxZW07XG4gIGxpbmUtaGVpZ2h0OiAxLjVlbTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG59XG5wOmZpcnN0LWNoaWxkIHtcbiAgbWFyZ2luLXRvcDogMDtcbn1cbnA6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5wIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICBjb2xvcjogaW5oZXJpdDtcbn1cbnAgYTpob3ZlciB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY29sb3I6IGluaGVyaXQ7XG59XG5cbi5lcGYtbmF2aWdhdGlvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaW1hZ2VzL25hdl9iZy5wbmdcIik7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBib3R0b207XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG59XG4uZXBmLW5hdmlnYXRpb24gdWwge1xuICB3aWR0aDogMTAwJTtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZXBmLW5hdmlnYXRpb24gdWwgbGkge1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbi5lcGYtbmF2aWdhdGlvbiB1bCBsaSBwIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG59XG4uZXBmLW5hdmlnYXRpb24gdWwgbGkgcCBhIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nOiAzMHB4O1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsaW5lLWhlaWdodDogMS41ZW07XG59XG4uZXBmLW5hdmlnYXRpb24gdWwgbGkgcCBhIHNwYW4uaWNvbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbn1cbi5lcGYtbmF2aWdhdGlvbiB1bCBsaSBwIGEgc3Bhbi5pY29uOmJlZm9yZSB7XG4gIHotaW5kZXg6IDE7XG59XG4uZXBmLW5hdmlnYXRpb24gdWwgbGkgcCBhIHNwYW4uaWNvbjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHdpZHRoOiAwcHg7XG4gIGhlaWdodDogMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IC0xO1xuICBsZWZ0OiA1MCU7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSkgdHJhbnNsYXRlWSgtNTAlKTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzEyNzgyO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dDtcbn1cbi5lcGYtbmF2aWdhdGlvbiB1bCBsaS5hY3RpdmUsIC5lcGYtbmF2aWdhdGlvbiB1bCBsaTpob3ZlciB7XG4gIGNvbG9yOiAjRDRBOTM5O1xufVxuLmVwZi1uYXZpZ2F0aW9uIHVsIGxpOmxhc3QtY2hpbGQ6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICB3aWR0aDogNHB4O1xuICBoZWlnaHQ6IDI0cHg7XG4gIGJhY2tncm91bmQ6ICNENEE5Mzk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gIHRyYW5zaXRpb246IHRvcCAwLjRzIGVhc2UtaW4tb3V0O1xuICBib3JkZXItcmFkaXVzOiAycHg7XG4gIHRvcDogY2FsYygxMDAlIC0gODRweCk7XG4gIHJpZ2h0OiAwO1xuICBtYXJnaW4tdG9wOiAzMHB4O1xuICB6LWluZGV4OiAtMTtcbn1cbi5lcGYtbmF2aWdhdGlvbiB1bCBsaTpudGgtY2hpbGQoMSkuYWN0aXZlIH4gbGk6bGFzdC1jaGlsZDphZnRlciB7XG4gIHRvcDogMCU7XG59XG4uZXBmLW5hdmlnYXRpb24gdWwgbGk6bnRoLWNoaWxkKDIpLmFjdGl2ZSB+IGxpOmxhc3QtY2hpbGQ6YWZ0ZXIge1xuICB0b3A6IDIwJTtcbn1cbi5lcGYtbmF2aWdhdGlvbiB1bCBsaTpudGgtY2hpbGQoMykuYWN0aXZlIH4gbGk6bGFzdC1jaGlsZDphZnRlciB7XG4gIHRvcDogNDAlO1xufVxuLmVwZi1uYXZpZ2F0aW9uIHVsIGxpOm50aC1jaGlsZCg0KS5hY3RpdmUgfiBsaTpsYXN0LWNoaWxkOmFmdGVyIHtcbiAgdG9wOiA2MCU7XG59XG5cbjpob3N0LnRvZ2dsZU5hdiAuZXBmLW5hdmlnYXRpb24gdWwgbGk6bGFzdC1jaGlsZDphZnRlciB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG46aG9zdC50b2dnbGVOYXYgLmVwZi1uYXZpZ2F0aW9uIHVsIGxpIHAge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG46aG9zdC50b2dnbGVOYXYgLmVwZi1uYXZpZ2F0aW9uIHVsIGxpIHAgYSB7XG4gIHBhZGRpbmc6IDMwcHggNDBweDtcbn1cbjpob3N0LnRvZ2dsZU5hdiAuZXBmLW5hdmlnYXRpb24gdWwgbGkgcCBhIHNwYW4udGV4dCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG46aG9zdC50b2dnbGVOYXYgLmVwZi1uYXZpZ2F0aW9uIHVsIGxpIHAgYSBzcGFuLmljb24ge1xuICBwYWRkaW5nOiAwO1xufVxuOmhvc3QudG9nZ2xlTmF2IC5lcGYtbmF2aWdhdGlvbiB1bCBsaSBwIGEgc3Bhbi5pY29uOmFmdGVyIHtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogNTBweDtcbn1cbjpob3N0LnRvZ2dsZU5hdiAuZXBmLW5hdmlnYXRpb24gdWwgbGkuYWN0aXZlIHAge1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbjpob3N0LnRvZ2dsZU5hdiAuZXBmLW5hdmlnYXRpb24gdWwgbGkuYWN0aXZlIHAgYSBzcGFuLmljb246YWZ0ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRDRBOTM5O1xufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi9fc3R5bGVzL191dGlsc1wiXHJcbkBpbXBvcnQgXCIuLi8uLi8uLi9fc3R5bGVzL19jb2xvdXJzXCJcclxuQGltcG9ydCBcIi4uLy4uLy4uL19zdHlsZXMvX3ZhcmlhYmxlc1wiXHJcbkBpbXBvcnQgXCIuLi8uLi8uLi9fc3R5bGVzL19mb250c1wiXHJcblxyXG4kbWVudS1pdGVtczogNVxyXG4kbWVudS1pdGVtcy1sb29wLW9mZnNldDogJG1lbnUtaXRlbXMgLSAxXHJcbiRoZWlnaHQ6ICgxMDAvJG1lbnUtaXRlbXMpICogMSVcclxuJHRyYW5zaXRpb24tc3BlZWQ6IDAuNHNcclxuXHJcbi5lcGYtbmF2aWdhdGlvblxyXG4gICAgd2lkdGg6IDEwMCVcclxuICAgIGhlaWdodDogMTAwJVxyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2ltYWdlcy9uYXZfYmcucG5nJykgXHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0IFxyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGJvdHRvbVxyXG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlXHJcbiAgICBwYWRkaW5nLXRvcDogJHBhZGRpbmdfYmlnLzJcclxuICAgIHVsXHJcbiAgICAgICAgd2lkdGg6IDEwMCVcclxuICAgICAgICBAaW5jbHVkZSBsaXN0LXJlc2V0KClcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmVcclxuICAgICAgICBsaVxyXG4gICAgICAgICAgICBjb2xvcjogJHdoaXRlXHJcbiAgICAgICAgICAgIHBcclxuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnRcclxuICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0XHJcbiAgICAgICAgICAgICAgICBhXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrXHJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogJHBhZGRpbmdfYmlnXHJcbiAgICAgICAgICAgICAgICAgICAgQGluY2x1ZGUgZm9udC1zaXplKDE2KVxyXG4gICAgICAgICAgICAgICAgICAgIHNwYW4uaWNvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmVcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1yaWdodDogJHBhZGRpbmdcclxuICAgICAgICAgICAgICAgICAgICAgICAgJjpiZWZvcmVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHotaW5kZXg6IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgJjphZnRlclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudDogJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAwcHhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMHB4XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHotaW5kZXg6IC0xXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiA1MCVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogNTAlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSkgdHJhbnNsYXRlWSgtNTAlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkZXBmX2JsdWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0XHJcbiAgICAgICAgICAgICYuYWN0aXZlLCAmOmhvdmVyXHJcbiAgICAgICAgICAgICAgICBjb2xvcjogJGVwZl9nb2xkXHJcbiAgICAgICAgICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICAgICAgICAgICAgJjphZnRlclxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICcnXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDRweFxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMjRweFxyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICRlcGZfZ29sZFxyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZVxyXG4gICAgICAgICAgICAgICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lXHJcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogdG9wICN7JHRyYW5zaXRpb24tc3BlZWR9IGVhc2UtaW4tb3V0XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4XHJcbiAgICAgICAgICAgICAgICAgICAgdG9wOiBjYWxjKDEwMCUgLSA4NHB4KVxyXG4gICAgICAgICAgICAgICAgICAgIHJpZ2h0OiAwXHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogJHBhZGRpbmdfYmlnXHJcbiAgICAgICAgICAgICAgICAgICAgei1pbmRleDogLTFcclxuICAgICAgICBAZm9yICRpIGZyb20gMSB0aHJvdWdoICRtZW51LWl0ZW1zLWxvb3Atb2Zmc2V0XHJcbiAgICAgICAgICAgIGxpOm50aC1jaGlsZCgjeyRpfSkuYWN0aXZlIH4gbGk6bGFzdC1jaGlsZDphZnRlclxyXG4gICAgICAgICAgICAgICAgdG9wOiAoJGhlaWdodCokaSktJGhlaWdodFxyXG5cclxuOmhvc3RcclxuICAgICYudG9nZ2xlTmF2XHJcbiAgICAgICAgLmVwZi1uYXZpZ2F0aW9uXHJcbiAgICAgICAgICAgIHVsIGxpXHJcbiAgICAgICAgICAgICAgICAmOmxhc3QtY2hpbGRcclxuICAgICAgICAgICAgICAgICAgICAmOmFmdGVyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IG5vbmVcclxuICAgICAgICAgICAgdWwgbGkgXHJcbiAgICAgICAgICAgICAgICBwIFxyXG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlclxyXG4gICAgICAgICAgICAgICAgICAgIGFcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogJHBhZGRpbmdfYmlnICRwYWRkaW5nX2JpZyskcGFkZGluZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzcGFuLnRleHRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IG5vbmVcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3Bhbi5pY29uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmOmFmdGVyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDUwcHhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDUwcHhcclxuICAgICAgICAgICAgdWwgbGkuYWN0aXZlIFxyXG4gICAgICAgICAgICAgICAgcFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAkd2hpdGVcclxuICAgICAgICAgICAgICAgICAgICBhXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNwYW4uaWNvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJjphZnRlclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRlcGZfZ29sZFxyXG5cclxuIiwiQG1peGluIGxpc3QtcmVzZXQoKVxyXG4gICAgbGlzdC1zdHlsZTogbm9uZVxyXG4gICAgbWFyZ2luOiAwXHJcbiAgICBwYWRkaW5nOiAwIiwiLy8gTWFpbiBDb2xvdXJzXHJcbiRlcGZfYmx1ZTogIzMxMjc4MlxyXG4kZXBmX3JlZDogI0UzMDYxM1xyXG4kZXBmX2dvbGQ6ICNENEE5MzlcclxuXHJcbi8vIFN1cHBvcnRpdmUgQ29sb3Vyc1xyXG4kYmx1ZTE6ICM0MTMxQjhcclxuJGJsdWUyOiAjNTAzRkRDXHJcbiRibHVlMzogI0Y2RkFGRVxyXG5cclxuLy8gTmV1dHJhbCBDb2xvdXJzXHJcbiRkYXJrX2dyZXkxOiAjMzMzMzMzXHJcbiRkYXJrX2dyZXkyOiAjNjY2NjY2XHJcbiRkYXJrX2dyZXkzOiAjOTk5OTk5XHJcbiRkYXJrX2dyZXk0OiAjREREREREXHJcbiRsaWdodF9ncmV5MTogI0Y0RjRGNFxyXG4kbGlnaHRfZ3JleTI6ICNGOEY4RjhcclxuJGxpZ2h0X2dyZXkzOiAjRkNGQ0ZDXHJcblxyXG4kd2hpdGU6ICNmZmZmZmZcclxuJGJsYWNrOiAjMDAwMDAwXHJcblxyXG4kZ3JhZGllbnRfYmx1ZTE6ICMzMTI3ODNcclxuJGdyYWRpZW50X2JsdWUyOiAjMzgyOEJCIiwiLy8gcGFkZGluZ1xyXG4kcGFkZGluZ19iaWc6IDMwcHhcclxuJHBhZGRpbmc6IDEwcHhcclxuXHJcbi8vIG1hcmdpblxyXG4kbWFyZ2luX2JpZzogMzBweFxyXG4kbWFyZ2luOiAxMHB4Il19 */"

/***/ }),

/***/ "./src/app/layouts/navigation/navigation.component.ts":
/*!************************************************************!*\
  !*** ./src/app/layouts/navigation/navigation.component.ts ***!
  \************************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NavigationComponent = class NavigationComponent {
    constructor() { }
    ngOnInit() {
    }
};
NavigationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-navigation',
        template: __webpack_require__(/*! raw-loader!./navigation.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/navigation/navigation.component.html"),
        styles: [__webpack_require__(/*! ./navigation.component.sass */ "./src/app/layouts/navigation/navigation.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], NavigationComponent);



/***/ }),

/***/ "./src/app/layouts/routing-spinner/index.ts":
/*!**************************************************!*\
  !*** ./src/app/layouts/routing-spinner/index.ts ***!
  \**************************************************/
/*! exports provided: RoutingSpinnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _routing_spinner_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./routing-spinner.component */ "./src/app/layouts/routing-spinner/routing-spinner.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RoutingSpinnerComponent", function() { return _routing_spinner_component__WEBPACK_IMPORTED_MODULE_0__["RoutingSpinnerComponent"]; });




/***/ }),

/***/ "./src/app/layouts/routing-spinner/routing-spinner.component.sass":
/*!************************************************************************!*\
  !*** ./src/app/layouts/routing-spinner/routing-spinner.component.sass ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvcm91dGluZy1zcGlubmVyL3JvdXRpbmctc3Bpbm5lci5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/layouts/routing-spinner/routing-spinner.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/layouts/routing-spinner/routing-spinner.component.ts ***!
  \**********************************************************************/
/*! exports provided: RoutingSpinnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutingSpinnerComponent", function() { return RoutingSpinnerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let RoutingSpinnerComponent = class RoutingSpinnerComponent {
    constructor() { }
    ngOnInit() {
    }
};
RoutingSpinnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-routing-spinner',
        template: __webpack_require__(/*! raw-loader!./routing-spinner.component.html */ "./node_modules/raw-loader/index.js!./src/app/layouts/routing-spinner/routing-spinner.component.html"),
        styles: [__webpack_require__(/*! ./routing-spinner.component.sass */ "./src/app/layouts/routing-spinner/routing-spinner.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], RoutingSpinnerComponent);



/***/ }),

/***/ "./src/app/payroll/index.ts":
/*!**********************************!*\
  !*** ./src/app/payroll/index.ts ***!
  \**********************************/
/*! exports provided: PayrollModule, PayrollOverviewComponent, PayrollEditComponent, PayrollPaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _payroll_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./payroll.module */ "./src/app/payroll/payroll.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PayrollModule", function() { return _payroll_module__WEBPACK_IMPORTED_MODULE_0__["PayrollModule"]; });

/* harmony import */ var _payroll_overview__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./payroll-overview */ "./src/app/payroll/payroll-overview/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PayrollOverviewComponent", function() { return _payroll_overview__WEBPACK_IMPORTED_MODULE_1__["PayrollOverviewComponent"]; });

/* harmony import */ var _payroll_edit__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./payroll-edit */ "./src/app/payroll/payroll-edit/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PayrollEditComponent", function() { return _payroll_edit__WEBPACK_IMPORTED_MODULE_2__["PayrollEditComponent"]; });

/* harmony import */ var _payroll_payment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./payroll-payment */ "./src/app/payroll/payroll-payment/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PayrollPaymentComponent", function() { return _payroll_payment__WEBPACK_IMPORTED_MODULE_3__["PayrollPaymentComponent"]; });







/***/ }),

/***/ "./src/app/payroll/payroll-edit/index.ts":
/*!***********************************************!*\
  !*** ./src/app/payroll/payroll-edit/index.ts ***!
  \***********************************************/
/*! exports provided: PayrollEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _payroll_edit_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./payroll-edit.component */ "./src/app/payroll/payroll-edit/payroll-edit.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PayrollEditComponent", function() { return _payroll_edit_component__WEBPACK_IMPORTED_MODULE_0__["PayrollEditComponent"]; });




/***/ }),

/***/ "./src/app/payroll/payroll-edit/payroll-edit.component.sass":
/*!******************************************************************!*\
  !*** ./src/app/payroll/payroll-edit/payroll-edit.component.sass ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BheXJvbGwvcGF5cm9sbC1lZGl0L3BheXJvbGwtZWRpdC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/payroll/payroll-edit/payroll-edit.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/payroll/payroll-edit/payroll-edit.component.ts ***!
  \****************************************************************/
/*! exports provided: PayrollEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollEditComponent", function() { return PayrollEditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PayrollEditComponent = class PayrollEditComponent {
    constructor() { }
    ngOnInit() {
    }
};
PayrollEditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-payroll-edit',
        template: __webpack_require__(/*! raw-loader!./payroll-edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/payroll/payroll-edit/payroll-edit.component.html"),
        styles: [__webpack_require__(/*! ./payroll-edit.component.sass */ "./src/app/payroll/payroll-edit/payroll-edit.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PayrollEditComponent);



/***/ }),

/***/ "./src/app/payroll/payroll-overview/index.ts":
/*!***************************************************!*\
  !*** ./src/app/payroll/payroll-overview/index.ts ***!
  \***************************************************/
/*! exports provided: PayrollOverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _payroll_overview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./payroll-overview.component */ "./src/app/payroll/payroll-overview/payroll-overview.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PayrollOverviewComponent", function() { return _payroll_overview_component__WEBPACK_IMPORTED_MODULE_0__["PayrollOverviewComponent"]; });




/***/ }),

/***/ "./src/app/payroll/payroll-overview/payroll-overview.component.sass":
/*!**************************************************************************!*\
  !*** ./src/app/payroll/payroll-overview/payroll-overview.component.sass ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BheXJvbGwvcGF5cm9sbC1vdmVydmlldy9wYXlyb2xsLW92ZXJ2aWV3LmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/payroll/payroll-overview/payroll-overview.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/payroll/payroll-overview/payroll-overview.component.ts ***!
  \************************************************************************/
/*! exports provided: PayrollOverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollOverviewComponent", function() { return PayrollOverviewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _payroll_datatable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./payroll_datatable */ "./src/app/payroll/payroll-overview/payroll_datatable.ts");



let PayrollOverviewComponent = class PayrollOverviewComponent {
    constructor() {
        this.table_opts = {
            title: "Test title",
            searchable: true,
            searchby: "name_of_employee",
            pagination: true,
            sortable: true,
            table_data: _payroll_datatable__WEBPACK_IMPORTED_MODULE_2__["payrollTable"],
            table_columns: [
                {
                    display: "Name of Employee",
                    slug: "name_of_employee",
                    currency: false
                },
                {
                    display: "Salary",
                    slug: "salary",
                    currency: true
                },
                {
                    display: "Outstanding Due",
                    slug: "outstanding_due",
                    currency: true
                },
                {
                    display: "Contribution Amount",
                    slug: "contribution_amount",
                    currency: true
                },
                {
                    display: "Current Month",
                    slug: "current_month",
                    currency: false
                }
            ]
        };
    }
    ngOnInit() { }
};
PayrollOverviewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-payroll-overview",
        template: __webpack_require__(/*! raw-loader!./payroll-overview.component.html */ "./node_modules/raw-loader/index.js!./src/app/payroll/payroll-overview/payroll-overview.component.html"),
        styles: [__webpack_require__(/*! ./payroll-overview.component.sass */ "./src/app/payroll/payroll-overview/payroll-overview.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PayrollOverviewComponent);



/***/ }),

/***/ "./src/app/payroll/payroll-overview/payroll_datatable.ts":
/*!***************************************************************!*\
  !*** ./src/app/payroll/payroll-overview/payroll_datatable.ts ***!
  \***************************************************************/
/*! exports provided: payrollTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "payrollTable", function() { return payrollTable; });
const payrollTable = [
    {
        name_of_employee: "Chong Jia Yong ONE",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min ONE",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali ONE",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace ONE",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah ONE",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong TWO",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min  TWO",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali  TWO",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace  TWO",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah  TWO",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong THREE",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min THREE",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali THREE",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace THREE",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah THREE",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong FOUR",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min FOUR",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali FOUR",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace FOUR",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah FOUR",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong FIVE",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min FIVE",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali FIVE",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace FIVE",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah FIVE",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong SIX",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min SIX",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali SIX",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace SIX",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah SIX",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong SEVEN",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min SEVEN",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali SEVEN",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace SEVEN",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah SEVEN",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong EIGHT",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min EIGHT",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali EIGHT",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace EIGHT",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah EIGHT",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong NINE",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min NINE",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali NINE",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace NINE",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah NINE",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong TEN",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min TEN",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali TEN",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace TEN",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah TEN",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong ELEVEN",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min ELEVEN",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali ELEVEN",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace ELEVEN",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah ELEVEN",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    },
    {
        name_of_employee: "Chong Jia Yong TWELVE",
        salary: 2000.0,
        outstanding_due: 0.0,
        contribution_amount: 223.0,
        current_month: "January"
    },
    {
        name_of_employee: "Cristina Chen Hui Min TWELVE",
        salary: 2250.0,
        outstanding_due: 0.0,
        contribution_amount: 110.0,
        current_month: "January"
    },
    {
        name_of_employee: "Muhammad Famirul Huzainan Bin Mohammed Ali TWELVE",
        salary: 4600.0,
        outstanding_due: 560.0,
        contribution_amount: 690.0,
        current_month: "September"
    },
    {
        name_of_employee: "Terry Wallace TWELVE",
        salary: 5800.0,
        outstanding_due: 0.0,
        contribution_amount: 0.0,
        current_month: "September"
    },
    {
        name_of_employee: "Siti Aminah Binti Lah TWELVE",
        salary: 2600.0,
        outstanding_due: 0.0,
        contribution_amount: 135.0,
        current_month: "September"
    }
];


/***/ }),

/***/ "./src/app/payroll/payroll-payment/index.ts":
/*!**************************************************!*\
  !*** ./src/app/payroll/payroll-payment/index.ts ***!
  \**************************************************/
/*! exports provided: PayrollPaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _payroll_payment_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./payroll-payment.component */ "./src/app/payroll/payroll-payment/payroll-payment.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PayrollPaymentComponent", function() { return _payroll_payment_component__WEBPACK_IMPORTED_MODULE_0__["PayrollPaymentComponent"]; });




/***/ }),

/***/ "./src/app/payroll/payroll-payment/payroll-payment.component.sass":
/*!************************************************************************!*\
  !*** ./src/app/payroll/payroll-payment/payroll-payment.component.sass ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BheXJvbGwvcGF5cm9sbC1wYXltZW50L3BheXJvbGwtcGF5bWVudC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/payroll/payroll-payment/payroll-payment.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/payroll/payroll-payment/payroll-payment.component.ts ***!
  \**********************************************************************/
/*! exports provided: PayrollPaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollPaymentComponent", function() { return PayrollPaymentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PayrollPaymentComponent = class PayrollPaymentComponent {
    constructor() { }
    ngOnInit() {
    }
};
PayrollPaymentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-payroll-payment',
        template: __webpack_require__(/*! raw-loader!./payroll-payment.component.html */ "./node_modules/raw-loader/index.js!./src/app/payroll/payroll-payment/payroll-payment.component.html"),
        styles: [__webpack_require__(/*! ./payroll-payment.component.sass */ "./src/app/payroll/payroll-payment/payroll-payment.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PayrollPaymentComponent);



/***/ }),

/***/ "./src/app/payroll/payroll-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/payroll/payroll-routing.module.ts ***!
  \***************************************************/
/*! exports provided: PayrollRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollRoutingModule", function() { return PayrollRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _payroll_overview__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./payroll-overview */ "./src/app/payroll/payroll-overview/index.ts");
/* harmony import */ var _payroll_edit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./payroll-edit */ "./src/app/payroll/payroll-edit/index.ts");
/* harmony import */ var _payroll_payment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./payroll-payment */ "./src/app/payroll/payroll-payment/index.ts");






const routes = [
    {
        path: "",
        component: _payroll_overview__WEBPACK_IMPORTED_MODULE_3__["PayrollOverviewComponent"],
        children: [
            {
                path: "edit:id",
                component: _payroll_edit__WEBPACK_IMPORTED_MODULE_4__["PayrollEditComponent"]
            },
            {
                path: "payment:id",
                component: _payroll_payment__WEBPACK_IMPORTED_MODULE_5__["PayrollPaymentComponent"]
            }
        ]
    }
];
let PayrollRoutingModule = class PayrollRoutingModule {
};
PayrollRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PayrollRoutingModule);



/***/ }),

/***/ "./src/app/payroll/payroll.module.ts":
/*!*******************************************!*\
  !*** ./src/app/payroll/payroll.module.ts ***!
  \*******************************************/
/*! exports provided: PayrollModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollModule", function() { return PayrollModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _payroll_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./payroll-routing.module */ "./src/app/payroll/payroll-routing.module.ts");
/* harmony import */ var _payroll_overview__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./payroll-overview */ "./src/app/payroll/payroll-overview/index.ts");
/* harmony import */ var _payroll_edit__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payroll-edit */ "./src/app/payroll/payroll-edit/index.ts");
/* harmony import */ var _payroll_payment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./payroll-payment */ "./src/app/payroll/payroll-payment/index.ts");








let PayrollModule = class PayrollModule {
};
PayrollModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _payroll_overview__WEBPACK_IMPORTED_MODULE_5__["PayrollOverviewComponent"],
            _payroll_edit__WEBPACK_IMPORTED_MODULE_6__["PayrollEditComponent"],
            _payroll_payment__WEBPACK_IMPORTED_MODULE_7__["PayrollPaymentComponent"]
        ],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _payroll_routing_module__WEBPACK_IMPORTED_MODULE_4__["PayrollRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]],
        exports: [
            _payroll_overview__WEBPACK_IMPORTED_MODULE_5__["PayrollOverviewComponent"],
            _payroll_edit__WEBPACK_IMPORTED_MODULE_6__["PayrollEditComponent"],
            _payroll_payment__WEBPACK_IMPORTED_MODULE_7__["PayrollPaymentComponent"]
        ]
    })
], PayrollModule);



/***/ }),

/***/ "./src/app/shared/_generals/index.ts":
/*!*******************************************!*\
  !*** ./src/app/shared/_generals/index.ts ***!
  \*******************************************/
/*! exports provided: Ringgit, getHTMLRinggit, Nric, OptinService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _pipes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pipes */ "./src/app/shared/_generals/pipes/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Ringgit", function() { return _pipes__WEBPACK_IMPORTED_MODULE_0__["Ringgit"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getHTMLRinggit", function() { return _pipes__WEBPACK_IMPORTED_MODULE_0__["getHTMLRinggit"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Nric", function() { return _pipes__WEBPACK_IMPORTED_MODULE_0__["Nric"]; });

/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services */ "./src/app/shared/_generals/services/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OptinService", function() { return _services__WEBPACK_IMPORTED_MODULE_1__["OptinService"]; });





/***/ }),

/***/ "./src/app/shared/_generals/pipes/index.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/_generals/pipes/index.ts ***!
  \*************************************************/
/*! exports provided: Ringgit, getHTMLRinggit, Nric */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ringgit_pipe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ringgit.pipe */ "./src/app/shared/_generals/pipes/ringgit.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Ringgit", function() { return _ringgit_pipe__WEBPACK_IMPORTED_MODULE_0__["Ringgit"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getHTMLRinggit", function() { return _ringgit_pipe__WEBPACK_IMPORTED_MODULE_0__["getHTMLRinggit"]; });

/* harmony import */ var _nric_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./nric.pipe */ "./src/app/shared/_generals/pipes/nric.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Nric", function() { return _nric_pipe__WEBPACK_IMPORTED_MODULE_1__["Nric"]; });





/***/ }),

/***/ "./src/app/shared/_generals/pipes/nric.pipe.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/_generals/pipes/nric.pipe.ts ***!
  \*****************************************************/
/*! exports provided: Nric */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Nric", function() { return Nric; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let Nric = class Nric {
    transform(value) {
        return value;
    }
};
Nric = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: "nric"
    })
], Nric);



/***/ }),

/***/ "./src/app/shared/_generals/pipes/ringgit.pipe.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/_generals/pipes/ringgit.pipe.ts ***!
  \********************************************************/
/*! exports provided: Ringgit, getHTMLRinggit */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ringgit", function() { return Ringgit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHTMLRinggit", function() { return getHTMLRinggit; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var numeral_es6__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! numeral-es6 */ "./node_modules/numeral-es6/index.js");
/* harmony import */ var numeral_es6__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(numeral_es6__WEBPACK_IMPORTED_MODULE_3__);




let Ringgit = class Ringgit {
    constructor(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    transform(value, event) {
        let wrapper = getHTMLRinggit(value);
        return this._sanitizer.bypassSecurityTrustHtml(wrapper);
    }
};
Ringgit = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: "ringgit"
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
], Ringgit);

function getHTMLRinggit(value) {
    return (`<div class="epf-priceformat">` +
        `<span class="epf-priceformat__currency">RM</span>` +
        `<span class="epf-priceformat__value">` +
        numeral_es6__WEBPACK_IMPORTED_MODULE_3___default()(value).format("0,0.00") +
        `</span>` +
        `</div>`);
}


/***/ }),

/***/ "./src/app/shared/_generals/services/index.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/_generals/services/index.ts ***!
  \****************************************************/
/*! exports provided: OptinService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _optin_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./optin.service */ "./src/app/shared/_generals/services/optin.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OptinService", function() { return _optin_service__WEBPACK_IMPORTED_MODULE_0__["OptinService"]; });




/***/ }),

/***/ "./src/app/shared/_generals/services/optin.service.ts":
/*!************************************************************!*\
  !*** ./src/app/shared/_generals/services/optin.service.ts ***!
  \************************************************************/
/*! exports provided: OptinService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OptinService", function() { return OptinService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");





const endpoint = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrl;
// const httpOptions = {
//   headers: new HttpHeaders({
//     "Content-Type": "application/json"
//   })
// };
let OptinService = class OptinService {
    constructor(_http) {
        this._http = _http;
    }
    extractData(res) {
        let body = res;
        return body || {};
    }
    getEmployerEPF() {
        return this._http.get(endpoint + "/optin").pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.extractData));
    }
};
OptinService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], OptinService);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/epf-fields-buttons.component.sass":
/*!**********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/epf-fields-buttons.component.sass ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9lcGYtZmllbGRzL2VwZi1maWVsZHMtZXh0ZW5kL2VwZi1maWVsZHMtYnV0dG9ucy9lcGYtZmllbGRzLWJ1dHRvbnMuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/epf-fields-buttons.component.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/epf-fields-buttons.component.ts ***!
  \********************************************************************************************************/
/*! exports provided: EpfFieldsButtonsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsButtonsComponent", function() { return EpfFieldsButtonsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EpfFieldsButtonsComponent = class EpfFieldsButtonsComponent {
    constructor() { }
    ngOnInit() { }
};
EpfFieldsButtonsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-fields-buttons",
        template: __webpack_require__(/*! raw-loader!./epf-fields-buttons.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/epf-fields-buttons.component.html"),
        styles: [__webpack_require__(/*! ./epf-fields-buttons.component.sass */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/epf-fields-buttons.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EpfFieldsButtonsComponent);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/index.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/index.ts ***!
  \*********************************************************************************/
/*! exports provided: EpfFieldsButtonsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_buttons_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields-buttons.component */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/epf-fields-buttons.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsButtonsComponent", function() { return _epf_fields_buttons_component__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsButtonsComponent"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/epf-fields-dropdowns.component.sass":
/*!**************************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/epf-fields-dropdowns.component.sass ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9lcGYtZmllbGRzL2VwZi1maWVsZHMtZXh0ZW5kL2VwZi1maWVsZHMtZHJvcGRvd25zL2VwZi1maWVsZHMtZHJvcGRvd25zLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/epf-fields-dropdowns.component.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/epf-fields-dropdowns.component.ts ***!
  \************************************************************************************************************/
/*! exports provided: EpfFieldsDropdownsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsDropdownsComponent", function() { return EpfFieldsDropdownsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EpfFieldsDropdownsComponent = class EpfFieldsDropdownsComponent {
    constructor() {
        this.labelText = "";
        this.labelvalue = "";
    }
    ngOnInit() {
        this.labelvalue = this.labelText;
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("label"),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], EpfFieldsDropdownsComponent.prototype, "labelText", void 0);
EpfFieldsDropdownsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-fields-dropdowns",
        template: __webpack_require__(/*! raw-loader!./epf-fields-dropdowns.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/epf-fields-dropdowns.component.html"),
        styles: [__webpack_require__(/*! ./epf-fields-dropdowns.component.sass */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/epf-fields-dropdowns.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EpfFieldsDropdownsComponent);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/index.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/index.ts ***!
  \***********************************************************************************/
/*! exports provided: EpfFieldsDropdownsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_dropdowns_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields-dropdowns.component */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/epf-fields-dropdowns.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsDropdownsComponent", function() { return _epf_fields_dropdowns_component__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsDropdownsComponent"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/epf-fields-inputs.component.sass":
/*!********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/epf-fields-inputs.component.sass ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9lcGYtZmllbGRzL2VwZi1maWVsZHMtZXh0ZW5kL2VwZi1maWVsZHMtaW5wdXRzL2VwZi1maWVsZHMtaW5wdXRzLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/epf-fields-inputs.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/epf-fields-inputs.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: EpfFieldsInputsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsInputsComponent", function() { return EpfFieldsInputsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EpfFieldsInputsComponent = class EpfFieldsInputsComponent {
    constructor() {
        this.labelText = "";
        this.labelvalue = "";
        this.showErrorFlag = true;
        this.hideError = () => {
            this.showErrorFlag = false;
        };
        this.showError = () => {
            this.showErrorFlag = true;
        };
    }
    ngOnInit() {
        this.labelvalue = this.labelText;
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("label"),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], EpfFieldsInputsComponent.prototype, "labelText", void 0);
EpfFieldsInputsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-fields-inputs",
        template: __webpack_require__(/*! raw-loader!./epf-fields-inputs.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/epf-fields-inputs.component.html"),
        styles: [__webpack_require__(/*! ./epf-fields-inputs.component.sass */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/epf-fields-inputs.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EpfFieldsInputsComponent);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/index.ts":
/*!********************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/index.ts ***!
  \********************************************************************************/
/*! exports provided: EpfFieldsInputsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_inputs_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields-inputs.component */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/epf-fields-inputs.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsInputsComponent", function() { return _epf_fields_inputs_component__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsInputsComponent"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/epf-fields-textarea.component.sass":
/*!************************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/epf-fields-textarea.component.sass ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9lcGYtZmllbGRzL2VwZi1maWVsZHMtZXh0ZW5kL2VwZi1maWVsZHMtdGV4dGFyZWEvZXBmLWZpZWxkcy10ZXh0YXJlYS5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/epf-fields-textarea.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/epf-fields-textarea.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: EpfFieldsTextareaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTextareaComponent", function() { return EpfFieldsTextareaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EpfFieldsTextareaComponent = class EpfFieldsTextareaComponent {
    constructor() {
        this.labelText = "";
        this.labelvalue = "";
    }
    ngOnInit() {
        this.labelvalue = this.labelText;
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("label"),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], EpfFieldsTextareaComponent.prototype, "labelText", void 0);
EpfFieldsTextareaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-fields-textarea",
        template: __webpack_require__(/*! raw-loader!./epf-fields-textarea.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/epf-fields-textarea.component.html"),
        styles: [__webpack_require__(/*! ./epf-fields-textarea.component.sass */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/epf-fields-textarea.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EpfFieldsTextareaComponent);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/index.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/index.ts ***!
  \**********************************************************************************/
/*! exports provided: EpfFieldsTextareaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_textarea_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields-textarea.component */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/epf-fields-textarea.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTextareaComponent", function() { return _epf_fields_textarea_component__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsTextareaComponent"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/epf-fields-toggles.component.sass":
/*!**********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/epf-fields-toggles.component.sass ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9lcGYtZmllbGRzL2VwZi1maWVsZHMtZXh0ZW5kL2VwZi1maWVsZHMtdG9nZ2xlcy9lcGYtZmllbGRzLXRvZ2dsZXMuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/epf-fields-toggles.component.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/epf-fields-toggles.component.ts ***!
  \********************************************************************************************************/
/*! exports provided: EpfFieldsTogglesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTogglesComponent", function() { return EpfFieldsTogglesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EpfFieldsTogglesComponent = class EpfFieldsTogglesComponent {
    constructor() { }
    ngOnInit() { }
};
EpfFieldsTogglesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-fields-toggles",
        template: __webpack_require__(/*! raw-loader!./epf-fields-toggles.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/epf-fields-toggles.component.html"),
        styles: [__webpack_require__(/*! ./epf-fields-toggles.component.sass */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/epf-fields-toggles.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EpfFieldsTogglesComponent);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/index.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/index.ts ***!
  \*********************************************************************************/
/*! exports provided: EpfFieldsTogglesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_toggles_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields-toggles.component */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/epf-fields-toggles.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTogglesComponent", function() { return _epf_fields_toggles_component__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsTogglesComponent"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-extend/index.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-extend/index.ts ***!
  \**************************************************************/
/*! exports provided: EpfFieldsInputsComponent, EpfFieldsTogglesComponent, EpfFieldsDropdownsComponent, EpfFieldsTextareaComponent, EpfFieldsButtonsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_inputs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields-inputs */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-inputs/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsInputsComponent", function() { return _epf_fields_inputs__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsInputsComponent"]; });

/* harmony import */ var _epf_fields_toggles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./epf-fields-toggles */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-toggles/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTogglesComponent", function() { return _epf_fields_toggles__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsTogglesComponent"]; });

/* harmony import */ var _epf_fields_dropdowns__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./epf-fields-dropdowns */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-dropdowns/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsDropdownsComponent", function() { return _epf_fields_dropdowns__WEBPACK_IMPORTED_MODULE_2__["EpfFieldsDropdownsComponent"]; });

/* harmony import */ var _epf_fields_textarea__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./epf-fields-textarea */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-textarea/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTextareaComponent", function() { return _epf_fields_textarea__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsTextareaComponent"]; });

/* harmony import */ var _epf_fields_buttons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./epf-fields-buttons */ "./src/app/shared/epf-fields/epf-fields-extend/epf-fields-buttons/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsButtonsComponent", function() { return _epf_fields_buttons__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsButtonsComponent"]; });








/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/epf-fields-error.component.sass":
/*!********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/epf-fields-error.component.sass ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hide {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2VwZi1maWVsZHMvZXBmLWZpZWxkcy1nZW5lcmFscy9lcGYtZmllbGRzLWVycm9yL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zcmNcXGFwcFxcc2hhcmVkXFxlcGYtZmllbGRzXFxlcGYtZmllbGRzLWdlbmVyYWxzXFxlcGYtZmllbGRzLWVycm9yXFxlcGYtZmllbGRzLWVycm9yLmNvbXBvbmVudC5zYXNzIiwic3JjL2FwcC9zaGFyZWQvZXBmLWZpZWxkcy9lcGYtZmllbGRzLWdlbmVyYWxzL2VwZi1maWVsZHMtZXJyb3IvZXBmLWZpZWxkcy1lcnJvci5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9lcGYtZmllbGRzL2VwZi1maWVsZHMtZ2VuZXJhbHMvZXBmLWZpZWxkcy1lcnJvci9lcGYtZmllbGRzLWVycm9yLmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiLmhpZGVcclxuICBkaXNwbGF5OiBub25lIiwiLmhpZGUge1xuICBkaXNwbGF5OiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/epf-fields-error.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/epf-fields-error.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: EpfFieldsErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsErrorComponent", function() { return EpfFieldsErrorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EpfFieldsErrorComponent = class EpfFieldsErrorComponent {
    constructor(cdr) {
        this.cdr = cdr;
        this._hide = true;
    }
    set text(value) {
        if (value !== this._text) {
            this._text = value;
            this._hide = !value;
            this.cdr.detectChanges();
        }
    }
    ngOnInit() { }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
], EpfFieldsErrorComponent.prototype, "text", null);
EpfFieldsErrorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-fields-error",
        template: __webpack_require__(/*! raw-loader!./epf-fields-error.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/epf-fields-error.component.html"),
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        styles: [__webpack_require__(/*! ./epf-fields-error.component.sass */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/epf-fields-error.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
], EpfFieldsErrorComponent);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/index.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/index.ts ***!
  \*********************************************************************************/
/*! exports provided: EpfFieldsErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_error_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields-error.component */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/epf-fields-error.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsErrorComponent", function() { return _epf_fields_error_component__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsErrorComponent"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/epf-fields-label.component.sass":
/*!********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/epf-fields-label.component.sass ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9lcGYtZmllbGRzL2VwZi1maWVsZHMtZ2VuZXJhbHMvZXBmLWZpZWxkcy1sYWJlbC9lcGYtZmllbGRzLWxhYmVsLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/epf-fields-label.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/epf-fields-label.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: EpfFieldsLabelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsLabelComponent", function() { return EpfFieldsLabelComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EpfFieldsLabelComponent = class EpfFieldsLabelComponent {
    constructor() {
        this.label = "";
    }
    ngOnInit() { }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("labelInput"),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], EpfFieldsLabelComponent.prototype, "label", void 0);
EpfFieldsLabelComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-fields-label",
        template: __webpack_require__(/*! raw-loader!./epf-fields-label.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/epf-fields-label.component.html"),
        styles: [__webpack_require__(/*! ./epf-fields-label.component.sass */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/epf-fields-label.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EpfFieldsLabelComponent);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/index.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/index.ts ***!
  \*********************************************************************************/
/*! exports provided: EpfFieldsLabelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_label_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields-label.component */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/epf-fields-label.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsLabelComponent", function() { return _epf_fields_label_component__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsLabelComponent"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/epf-fields-wrapper.component.sass":
/*!************************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/epf-fields-wrapper.component.sass ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9lcGYtZmllbGRzL2VwZi1maWVsZHMtZ2VuZXJhbHMvZXBmLWZpZWxkcy13cmFwcGVyL2VwZi1maWVsZHMtd3JhcHBlci5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/epf-fields-wrapper.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/epf-fields-wrapper.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: EpfFieldsWrapperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsWrapperComponent", function() { return EpfFieldsWrapperComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EpfFieldsWrapperComponent = class EpfFieldsWrapperComponent {
    constructor() { }
    ngOnInit() { }
};
EpfFieldsWrapperComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-fields-wrapper",
        template: __webpack_require__(/*! raw-loader!./epf-fields-wrapper.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/epf-fields-wrapper.component.html"),
        styles: [__webpack_require__(/*! ./epf-fields-wrapper.component.sass */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/epf-fields-wrapper.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EpfFieldsWrapperComponent);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/index.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/index.ts ***!
  \***********************************************************************************/
/*! exports provided: EpfFieldsWrapperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_wrapper_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields-wrapper.component */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/epf-fields-wrapper.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsWrapperComponent", function() { return _epf_fields_wrapper_component__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsWrapperComponent"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors-container/control-errors-container.directive.ts":
/*!*************************************************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors-container/control-errors-container.directive.ts ***!
  \*************************************************************************************************************************************/
/*! exports provided: ControlErrorsContainerDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsContainerDirective", function() { return ControlErrorsContainerDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ControlErrorsContainerDirective = class ControlErrorsContainerDirective {
    constructor(vcr) {
        this.vcr = vcr;
    }
};
ControlErrorsContainerDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: "[epfControlErrorsContainer]"
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"]])
], ControlErrorsContainerDirective);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors-container/index.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors-container/index.ts ***!
  \********************************************************************************************************/
/*! exports provided: ControlErrorsContainerDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _control_errors_container_directive__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./control-errors-container.directive */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors-container/control-errors-container.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsContainerDirective", function() { return _control_errors_container_directive__WEBPACK_IMPORTED_MODULE_0__["ControlErrorsContainerDirective"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors/control-errors.directive.ts":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors/control-errors.directive.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: ControlErrorsDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsDirective", function() { return ControlErrorsDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ngx_take_until_destroy__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-take-until-destroy */ "./node_modules/ngx-take-until-destroy/fesm2015/ngx-take-until-destroy.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _error_handling__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../error-handling */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/error-handling.ts");
/* harmony import */ var _epf_fields_error__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../epf-fields-error */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/index.ts");
/* harmony import */ var _control_errors_container__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../control-errors-container */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors-container/index.ts");
/* harmony import */ var _control_submission__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../control-submission */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-submission/index.ts");









let ControlErrorsDirective = class ControlErrorsDirective {
    constructor(vcr, resolver, controlErrorContainer, errors, form, controlDir) {
        this.vcr = vcr;
        this.resolver = resolver;
        this.errors = errors;
        this.form = form;
        this.controlDir = controlDir;
        this.customErrors = {};
        this.container = controlErrorContainer ? controlErrorContainer.vcr : vcr;
        this.submit$ = this.form ? this.form.submit$ : rxjs__WEBPACK_IMPORTED_MODULE_4__["EMPTY"];
    }
    ngOnInit() {
        Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["merge"])(this.submit$, this.control.valueChanges)
            .pipe(Object(ngx_take_until_destroy__WEBPACK_IMPORTED_MODULE_3__["untilDestroyed"])(this))
            .subscribe(v => {
            const controlErrors = this.control.errors;
            if (controlErrors) {
                const firstKey = Object.keys(controlErrors)[0];
                const getError = this.errors[firstKey];
                const text = this.customErrors[firstKey] || getError(controlErrors[firstKey]);
                this.setError(text);
            }
            else if (this.ref) {
                this.setError(null);
            }
        });
    }
    get control() {
        return this.controlDir.control;
    }
    setError(text) {
        if (!this.ref) {
            const factory = this.resolver.resolveComponentFactory(_epf_fields_error__WEBPACK_IMPORTED_MODULE_6__["EpfFieldsErrorComponent"]);
            this.ref = this.container.createComponent(factory);
        }
        this.ref.instance.text = text;
    }
    ngOnDestroy() { }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ControlErrorsDirective.prototype, "customErrors", void 0);
ControlErrorsDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: "[formControl], [formControlName]"
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"])()),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_error_handling__WEBPACK_IMPORTED_MODULE_5__["FORM_ERRORS"])),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"])()), tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"])()),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"],
        _control_errors_container__WEBPACK_IMPORTED_MODULE_7__["ControlErrorsContainerDirective"], Object, _control_submission__WEBPACK_IMPORTED_MODULE_8__["ControlSubmissionDirective"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControl"]])
], ControlErrorsDirective);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors/index.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors/index.ts ***!
  \**********************************************************************************************/
/*! exports provided: ControlErrorsDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _control_errors_directive__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./control-errors.directive */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors/control-errors.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsDirective", function() { return _control_errors_directive__WEBPACK_IMPORTED_MODULE_0__["ControlErrorsDirective"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-submission/control-submission.directive.ts":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-submission/control-submission.directive.ts ***!
  \*************************************************************************************************************************/
/*! exports provided: ControlSubmissionDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ControlSubmissionDirective", function() { return ControlSubmissionDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let ControlSubmissionDirective = class ControlSubmissionDirective {
    constructor(host) {
        this.host = host;
        this.submit$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["fromEvent"])(this.element, "submit").pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(() => {
            if (this.element.classList.contains("submitted") === false) {
                this.element.classList.add("submitted");
            }
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["shareReplay"])(1));
    }
    get element() {
        return this.host.nativeElement;
    }
};
ControlSubmissionDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: "form"
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
], ControlSubmissionDirective);



/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-submission/index.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-submission/index.ts ***!
  \**************************************************************************************************/
/*! exports provided: ControlSubmissionDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _control_submission_directive__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./control-submission.directive */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-submission/control-submission.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlSubmissionDirective", function() { return _control_submission_directive__WEBPACK_IMPORTED_MODULE_0__["ControlSubmissionDirective"]; });




/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/error-handling.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/error-handling/error-handling.ts ***!
  \****************************************************************************************/
/*! exports provided: defaultErrors, FORM_ERRORS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultErrors", function() { return defaultErrors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FORM_ERRORS", function() { return FORM_ERRORS; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");

const defaultErrors = {
    required: (error) => `This field is required`,
    minlength: ({ requiredLength, actualLength }) => `Expect ${requiredLength} but got ${actualLength}`
};
const FORM_ERRORS = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("FORM_ERRORS", {
    providedIn: "root",
    factory: () => defaultErrors
});


/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/index.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/error-handling/index.ts ***!
  \*******************************************************************************/
/*! exports provided: defaultErrors, FORM_ERRORS, ControlErrorsDirective, ControlErrorsContainerDirective, ControlSubmissionDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _error_handling__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./error-handling */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/error-handling.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "defaultErrors", function() { return _error_handling__WEBPACK_IMPORTED_MODULE_0__["defaultErrors"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FORM_ERRORS", function() { return _error_handling__WEBPACK_IMPORTED_MODULE_0__["FORM_ERRORS"]; });

/* harmony import */ var _control_errors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./control-errors */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsDirective", function() { return _control_errors__WEBPACK_IMPORTED_MODULE_1__["ControlErrorsDirective"]; });

/* harmony import */ var _control_errors_container__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./control-errors-container */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-errors-container/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsContainerDirective", function() { return _control_errors_container__WEBPACK_IMPORTED_MODULE_2__["ControlErrorsContainerDirective"]; });

/* harmony import */ var _control_submission__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./control-submission */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/control-submission/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlSubmissionDirective", function() { return _control_submission__WEBPACK_IMPORTED_MODULE_3__["ControlSubmissionDirective"]; });







/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields-generals/index.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields-generals/index.ts ***!
  \****************************************************************/
/*! exports provided: defaultErrors, FORM_ERRORS, EpfFieldsWrapperComponent, EpfFieldsLabelComponent, EpfFieldsErrorComponent, ControlErrorsDirective, ControlErrorsContainerDirective, ControlSubmissionDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _error_handling__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./error-handling */ "./src/app/shared/epf-fields/epf-fields-generals/error-handling/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "defaultErrors", function() { return _error_handling__WEBPACK_IMPORTED_MODULE_0__["defaultErrors"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FORM_ERRORS", function() { return _error_handling__WEBPACK_IMPORTED_MODULE_0__["FORM_ERRORS"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsDirective", function() { return _error_handling__WEBPACK_IMPORTED_MODULE_0__["ControlErrorsDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsContainerDirective", function() { return _error_handling__WEBPACK_IMPORTED_MODULE_0__["ControlErrorsContainerDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlSubmissionDirective", function() { return _error_handling__WEBPACK_IMPORTED_MODULE_0__["ControlSubmissionDirective"]; });

/* harmony import */ var _epf_fields_wrapper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./epf-fields-wrapper */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-wrapper/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsWrapperComponent", function() { return _epf_fields_wrapper__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsWrapperComponent"]; });

/* harmony import */ var _epf_fields_label__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./epf-fields-label */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-label/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsLabelComponent", function() { return _epf_fields_label__WEBPACK_IMPORTED_MODULE_2__["EpfFieldsLabelComponent"]; });

/* harmony import */ var _epf_fields_error__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./epf-fields-error */ "./src/app/shared/epf-fields/epf-fields-generals/epf-fields-error/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsErrorComponent", function() { return _epf_fields_error__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsErrorComponent"]; });







/***/ }),

/***/ "./src/app/shared/epf-fields/epf-fields.module.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/epf-fields/epf-fields.module.ts ***!
  \********************************************************/
/*! exports provided: EpfFieldsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsModule", function() { return EpfFieldsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./epf-fields-generals */ "./src/app/shared/epf-fields/epf-fields-generals/index.ts");
/* harmony import */ var _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./epf-fields-extend */ "./src/app/shared/epf-fields/epf-fields-extend/index.ts");





let EpfFieldsModule = class EpfFieldsModule {
};
EpfFieldsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsWrapperComponent"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsErrorComponent"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsLabelComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsInputsComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsTogglesComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsDropdownsComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsButtonsComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsTextareaComponent"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["ControlErrorsDirective"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["ControlErrorsContainerDirective"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["ControlSubmissionDirective"]
        ],
        exports: [
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsWrapperComponent"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsErrorComponent"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsLabelComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsInputsComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsTogglesComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsDropdownsComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsButtonsComponent"],
            _epf_fields_extend__WEBPACK_IMPORTED_MODULE_4__["EpfFieldsTextareaComponent"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["ControlErrorsDirective"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["ControlErrorsContainerDirective"],
            _epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["ControlSubmissionDirective"]
        ],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
        entryComponents: [_epf_fields_generals__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsErrorComponent"]]
    })
], EpfFieldsModule);



/***/ }),

/***/ "./src/app/shared/epf-fields/index.ts":
/*!********************************************!*\
  !*** ./src/app/shared/epf-fields/index.ts ***!
  \********************************************/
/*! exports provided: EpfFieldsModule, EpfFieldsInputsComponent, EpfFieldsTogglesComponent, EpfFieldsDropdownsComponent, EpfFieldsTextareaComponent, EpfFieldsButtonsComponent, defaultErrors, FORM_ERRORS, EpfFieldsWrapperComponent, EpfFieldsLabelComponent, EpfFieldsErrorComponent, ControlErrorsDirective, ControlErrorsContainerDirective, ControlSubmissionDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_fields_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-fields.module */ "./src/app/shared/epf-fields/epf-fields.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsModule", function() { return _epf_fields_module__WEBPACK_IMPORTED_MODULE_0__["EpfFieldsModule"]; });

/* harmony import */ var _epf_fields_extend__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./epf-fields-extend */ "./src/app/shared/epf-fields/epf-fields-extend/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsInputsComponent", function() { return _epf_fields_extend__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsInputsComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTogglesComponent", function() { return _epf_fields_extend__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsTogglesComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsDropdownsComponent", function() { return _epf_fields_extend__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsDropdownsComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTextareaComponent", function() { return _epf_fields_extend__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsTextareaComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsButtonsComponent", function() { return _epf_fields_extend__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsButtonsComponent"]; });

/* harmony import */ var _epf_fields_generals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./epf-fields-generals */ "./src/app/shared/epf-fields/epf-fields-generals/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "defaultErrors", function() { return _epf_fields_generals__WEBPACK_IMPORTED_MODULE_2__["defaultErrors"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FORM_ERRORS", function() { return _epf_fields_generals__WEBPACK_IMPORTED_MODULE_2__["FORM_ERRORS"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsWrapperComponent", function() { return _epf_fields_generals__WEBPACK_IMPORTED_MODULE_2__["EpfFieldsWrapperComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsLabelComponent", function() { return _epf_fields_generals__WEBPACK_IMPORTED_MODULE_2__["EpfFieldsLabelComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsErrorComponent", function() { return _epf_fields_generals__WEBPACK_IMPORTED_MODULE_2__["EpfFieldsErrorComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsDirective", function() { return _epf_fields_generals__WEBPACK_IMPORTED_MODULE_2__["ControlErrorsDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsContainerDirective", function() { return _epf_fields_generals__WEBPACK_IMPORTED_MODULE_2__["ControlErrorsContainerDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlSubmissionDirective", function() { return _epf_fields_generals__WEBPACK_IMPORTED_MODULE_2__["ControlSubmissionDirective"]; });






/***/ }),

/***/ "./src/app/shared/epf-table/epf-table.module.ts":
/*!******************************************************!*\
  !*** ./src/app/shared/epf-table/epf-table.module.ts ***!
  \******************************************************/
/*! exports provided: EpfTableModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfTableModule", function() { return EpfTableModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm2015/table.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _generals__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_generals */ "./src/app/shared/_generals/index.ts");
/* harmony import */ var _epf_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./epf-table */ "./src/app/shared/epf-table/epf-table/index.ts");









let EpfTableModule = class EpfTableModule {
};
EpfTableModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_generals__WEBPACK_IMPORTED_MODULE_6__["Ringgit"], _generals__WEBPACK_IMPORTED_MODULE_6__["Nric"], _epf_table__WEBPACK_IMPORTED_MODULE_7__["EpfTableComponent"]],
        exports: [
            _generals__WEBPACK_IMPORTED_MODULE_6__["Ringgit"],
            _epf_table__WEBPACK_IMPORTED_MODULE_7__["EpfTableComponent"],
            _angular_material_table__WEBPACK_IMPORTED_MODULE_4__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSortModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginatorModule"]
        ],
        imports: [
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCheckboxModule"],
            _angular_material_table__WEBPACK_IMPORTED_MODULE_4__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSortModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginatorModule"]
        ]
    })
], EpfTableModule);



/***/ }),

/***/ "./src/app/shared/epf-table/epf-table/epf-table.component.sass":
/*!*********************************************************************!*\
  !*** ./src/app/shared/epf-table/epf-table/epf-table.component.sass ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".epf-table_wrapper__header {\n  display: flex;\n  align-items: stretch;\n  flex-wrap: wrap;\n}\n.epf-table_wrapper__title {\n  flex-grow: 1;\n}\n.epf-table_wrapper__title::ng-deep caption {\n  display: block;\n}\n.epf-table_wrapper__search {\n  flex-grow: 0;\n}\n.epf-table_wrapper__table .epf-table_wrapper__search {\n  width: 100%;\n}\n.epf-table_wrapper__table table.mat-table {\n  width: 100%;\n}\n.epf-table_wrapper__table table.mat-table tr.mat-header-row, .epf-table_wrapper__table table.mat-table tr.mat-row {\n  width: 100%;\n  height: auto;\n  display: flex;\n}\n.epf-table_wrapper__table table.mat-table tr.mat-header-row th.mat-header-cell, .epf-table_wrapper__table table.mat-table tr.mat-header-row td.mat-cell, .epf-table_wrapper__table table.mat-table tr.mat-row th.mat-header-cell, .epf-table_wrapper__table table.mat-table tr.mat-row td.mat-cell {\n  flex: 1;\n  padding: 20px;\n  font-size: 1em;\n  border-bottom: none;\n}\n.epf-table_wrapper__table table.mat-table tr.mat-header-row th.mat-header-cell.mat-column-select, .epf-table_wrapper__table table.mat-table tr.mat-header-row td.mat-cell.mat-column-select, .epf-table_wrapper__table table.mat-table tr.mat-row th.mat-header-cell.mat-column-select, .epf-table_wrapper__table table.mat-table tr.mat-row td.mat-cell.mat-column-select {\n  flex: inherit;\n  display: flex;\n  align-items: center;\n}\n.epf-table_wrapper__table table.mat-table tr.mat-header-row {\n  border-bottom: 2px solid #ededed;\n}\n.epf-table_wrapper__table table.mat-table tr.mat-row {\n  border-bottom: 1px solid #ededed;\n}\n.epf-table_wrapper__table table.mat-table tr.mat-row:nth-child(odd) {\n  background-color: #f9fafe;\n}\n.epf-table_wrapper__table.table_expandable table.mat-table tr.mat-row {\n  background-color: #ffffff;\n}\n.epf-table_wrapper__table.table_expandable table.mat-table tr.mat-row:nth-child(odd) {\n  border-bottom: none;\n}\n.epf-table_wrapper__table.table_expandable table.mat-table tr.mat-row:nth-child(4n+1) {\n  background-color: #f9fafe;\n}\n.epf-table_wrapper__table.table_expandable table.mat-table tr.mat-row.expanded {\n  border-bottom: 1px solid #ededed;\n}\n.epf-table_wrapper__table.table_expandable table.mat-table tr.mat-row table {\n  width: 100%;\n}\n@media screen and (max-width: 576px) {\n  .epf-table_wrapper__title {\n    width: 100%;\n  }\n  .epf-table_wrapper__search {\n    width: 100%;\n  }\n  .epf-table_wrapper__table {\n    grid-column: 2/2;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-header-row th.mat-header-cell:not(:nth-child(1)) {\n    display: none;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-header-row th.mat-header-cell:nth-child(1) {\n    padding: 20px 10px;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-row {\n    display: block;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-row td.mat-cell {\n    width: 100%;\n    display: flex;\n    padding: 2px 10px;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-row td.mat-cell.mat-column-select {\n    display: none;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-row td.mat-cell:before {\n    flex-grow: 1;\n    content: attr(data-label);\n    text-transform: capitalize;\n    font-size: 0.9em;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-row td.mat-cell:nth-child(1) {\n    padding: 20px 10px;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-row td.mat-cell:nth-child(1):before {\n    content: \"\";\n    display: none;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-row td.mat-cell:nth-child(2) {\n    padding-top: 0px;\n  }\n  .epf-table_wrapper__table table.mat-table tr.mat-row td.mat-cell:last-child {\n    padding-bottom: 20px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2VwZi10YWJsZS9lcGYtdGFibGUvQzpcXERldlxcUHJvamVjdHNcXGV5XFxjbG91ZFxccGF5cm9sbC1wcm90b3R5cGUtLW9wdC1pblxccHJvdG90eXBlL3NyY1xcYXBwXFxzaGFyZWRcXGVwZi10YWJsZVxcZXBmLXRhYmxlXFxlcGYtdGFibGUuY29tcG9uZW50LnNhc3MiLCJzcmMvYXBwL3NoYXJlZC9lcGYtdGFibGUvZXBmLXRhYmxlL2VwZi10YWJsZS5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGFBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7QUNBUjtBRENJO0VBQ0ksWUFBQTtBQ0NSO0FEQ1k7RUFDSSxjQUFBO0FDQ2hCO0FEQUk7RUFDSSxZQUFBO0FDRVI7QURBSTtFQUNJLFdBQUE7QUNFUjtBREFRO0VBQ0ksV0FBQTtBQ0VaO0FERFk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNHaEI7QURGZ0I7RUFDSSxPQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQ0lwQjtBREhvQjtFQUNJLGFBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUNLeEI7QURKWTtFQUNJLGdDQUFBO0FDTWhCO0FETFk7RUFDSSxnQ0FBQTtBQ09oQjtBRE5nQjtFQUNJLHlCQUFBO0FDUXBCO0FETGdCO0VBQ0kseUJBQUE7QUNPcEI7QUROb0I7RUFDSSxtQkFBQTtBQ1F4QjtBRFBvQjtFQUNJLHlCQUFBO0FDU3hCO0FEUm9CO0VBQ0ksZ0NBQUE7QUNVeEI7QURUb0I7RUFDSSxXQUFBO0FDV3hCO0FEVEE7RUFFUTtJQUNJLFdBQUE7RUNXVjtFRFZNO0lBQ0ksV0FBQTtFQ1lWO0VEWE07SUFDSSxnQkFBQTtFQ2FWO0VEVHNCO0lBQ0ksYUFBQTtFQ1cxQjtFRFZzQjtJQUNJLGtCQUFBO0VDWTFCO0VEWGM7SUFDSSxjQUFBO0VDYWxCO0VEWmtCO0lBQ0ksV0FBQTtJQUNBLGFBQUE7SUFDQSxpQkFBQTtFQ2N0QjtFRGJzQjtJQUNJLGFBQUE7RUNlMUI7RURkc0I7SUFDSSxZQUFBO0lBQ0EseUJBQUE7SUFDQSwwQkFBQTtJQUNBLGdCQUFBO0VDZ0IxQjtFRGZzQjtJQUNJLGtCQUFBO0VDaUIxQjtFRGhCMEI7SUFDSSxXQUFBO0lBQ0EsYUFBQTtFQ2tCOUI7RURqQnNCO0lBQ0ksZ0JBQUE7RUNtQjFCO0VEbEJzQjtJQUNJLG9CQUFBO0VDb0IxQjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL2VwZi10YWJsZS9lcGYtdGFibGUvZXBmLXRhYmxlLmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiLmVwZi10YWJsZV93cmFwcGVyXHJcbiAgICAmX19oZWFkZXJcclxuICAgICAgICBkaXNwbGF5OiBmbGV4XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IHN0cmV0Y2hcclxuICAgICAgICBmbGV4LXdyYXA6IHdyYXBcclxuICAgICZfX3RpdGxlXHJcbiAgICAgICAgZmxleC1ncm93OiAxXHJcbiAgICAgICAgJjo6bmctZGVlcFxyXG4gICAgICAgICAgICBjYXB0aW9uXHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9ja1xyXG4gICAgJl9fc2VhcmNoXHJcbiAgICAgICAgZmxleC1ncm93OiAwXHJcbiAgICAvLyAmX19wYWdpbmF0aW9uXHJcbiAgICAmX190YWJsZSAmX19zZWFyY2hcclxuICAgICAgICB3aWR0aDogMTAwJVxyXG4gICAgJl9fdGFibGVcclxuICAgICAgICB0YWJsZS5tYXQtdGFibGVcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCVcclxuICAgICAgICAgICAgdHIubWF0LWhlYWRlci1yb3csIHRyLm1hdC1yb3dcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlXHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG9cclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXhcclxuICAgICAgICAgICAgICAgIHRoLm1hdC1oZWFkZXItY2VsbCwgdGQubWF0LWNlbGxcclxuICAgICAgICAgICAgICAgICAgICBmbGV4OiAxXHJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMjBweFxyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMWVtXHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogbm9uZVxyXG4gICAgICAgICAgICAgICAgICAgICYubWF0LWNvbHVtbi1zZWxlY3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmxleDogaW5oZXJpdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXJcclxuICAgICAgICAgICAgdHIubWF0LWhlYWRlci1yb3dcclxuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjZWRlZGVkXHJcbiAgICAgICAgICAgIHRyLm1hdC1yb3dcclxuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWRlZGVkXHJcbiAgICAgICAgICAgICAgICAmOm50aC1jaGlsZChvZGQpXHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZmFmZSBcclxuICAgICAgICAmLnRhYmxlX2V4cGFuZGFibGVcclxuICAgICAgICAgICAgdGFibGUubWF0LXRhYmxlXHJcbiAgICAgICAgICAgICAgICB0ci5tYXQtcm93XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZlxyXG4gICAgICAgICAgICAgICAgICAgICY6bnRoLWNoaWxkKG9kZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogbm9uZVxyXG4gICAgICAgICAgICAgICAgICAgICY6bnRoLWNoaWxkKDRuICsgMSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZmFmZVxyXG4gICAgICAgICAgICAgICAgICAgICYuZXhwYW5kZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlZGVkZWRcclxuICAgICAgICAgICAgICAgICAgICB0YWJsZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTc2cHgpXHJcbiAgICAuZXBmLXRhYmxlX3dyYXBwZXJcclxuICAgICAgICAmX190aXRsZVxyXG4gICAgICAgICAgICB3aWR0aDogMTAwJVxyXG4gICAgICAgICZfX3NlYXJjaFxyXG4gICAgICAgICAgICB3aWR0aDogMTAwJVxyXG4gICAgICAgICZfX3RhYmxlXHJcbiAgICAgICAgICAgIGdyaWQtY29sdW1uOiAyIC8gMlxyXG4gICAgICAgICAgICB0YWJsZS5tYXQtdGFibGVcclxuICAgICAgICAgICAgICAgIHRyLm1hdC1oZWFkZXItcm93XHJcbiAgICAgICAgICAgICAgICAgICAgdGgubWF0LWhlYWRlci1jZWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICY6bm90KDpudGgtY2hpbGQoMSkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBub25lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICY6bnRoLWNoaWxkKDEpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAyMHB4IDEwcHhcclxuICAgICAgICAgICAgICAgIHRyLm1hdC1yb3dcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9ja1xyXG4gICAgICAgICAgICAgICAgICAgIHRkLm1hdC1jZWxsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXhcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMnB4IDEwcHhcclxuICAgICAgICAgICAgICAgICAgICAgICAgJi5tYXQtY29sdW1uLXNlbGVjdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmOmJlZm9yZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxleC1ncm93OiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50OiBhdHRyKGRhdGEtbGFiZWwpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAwLjllbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmOm50aC1jaGlsZCgxKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMjBweCAxMHB4XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmOmJlZm9yZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmOm50aC1jaGlsZCgyKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDBweFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmOmxhc3QtY2hpbGRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4ICIsIi5lcGYtdGFibGVfd3JhcHBlcl9faGVhZGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IHN0cmV0Y2g7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cbi5lcGYtdGFibGVfd3JhcHBlcl9fdGl0bGUge1xuICBmbGV4LWdyb3c6IDE7XG59XG4uZXBmLXRhYmxlX3dyYXBwZXJfX3RpdGxlOjpuZy1kZWVwIGNhcHRpb24ge1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5lcGYtdGFibGVfd3JhcHBlcl9fc2VhcmNoIHtcbiAgZmxleC1ncm93OiAwO1xufVxuLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSAuZXBmLXRhYmxlX3dyYXBwZXJfX3NlYXJjaCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUge1xuICB3aWR0aDogMTAwJTtcbn1cbi5lcGYtdGFibGVfd3JhcHBlcl9fdGFibGUgdGFibGUubWF0LXRhYmxlIHRyLm1hdC1oZWFkZXItcm93LCAuZXBmLXRhYmxlX3dyYXBwZXJfX3RhYmxlIHRhYmxlLm1hdC10YWJsZSB0ci5tYXQtcm93IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbiAgZGlzcGxheTogZmxleDtcbn1cbi5lcGYtdGFibGVfd3JhcHBlcl9fdGFibGUgdGFibGUubWF0LXRhYmxlIHRyLm1hdC1oZWFkZXItcm93IHRoLm1hdC1oZWFkZXItY2VsbCwgLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LWhlYWRlci1yb3cgdGQubWF0LWNlbGwsIC5lcGYtdGFibGVfd3JhcHBlcl9fdGFibGUgdGFibGUubWF0LXRhYmxlIHRyLm1hdC1yb3cgdGgubWF0LWhlYWRlci1jZWxsLCAuZXBmLXRhYmxlX3dyYXBwZXJfX3RhYmxlIHRhYmxlLm1hdC10YWJsZSB0ci5tYXQtcm93IHRkLm1hdC1jZWxsIHtcbiAgZmxleDogMTtcbiAgcGFkZGluZzogMjBweDtcbiAgZm9udC1zaXplOiAxZW07XG4gIGJvcmRlci1ib3R0b206IG5vbmU7XG59XG4uZXBmLXRhYmxlX3dyYXBwZXJfX3RhYmxlIHRhYmxlLm1hdC10YWJsZSB0ci5tYXQtaGVhZGVyLXJvdyB0aC5tYXQtaGVhZGVyLWNlbGwubWF0LWNvbHVtbi1zZWxlY3QsIC5lcGYtdGFibGVfd3JhcHBlcl9fdGFibGUgdGFibGUubWF0LXRhYmxlIHRyLm1hdC1oZWFkZXItcm93IHRkLm1hdC1jZWxsLm1hdC1jb2x1bW4tc2VsZWN0LCAuZXBmLXRhYmxlX3dyYXBwZXJfX3RhYmxlIHRhYmxlLm1hdC10YWJsZSB0ci5tYXQtcm93IHRoLm1hdC1oZWFkZXItY2VsbC5tYXQtY29sdW1uLXNlbGVjdCwgLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LXJvdyB0ZC5tYXQtY2VsbC5tYXQtY29sdW1uLXNlbGVjdCB7XG4gIGZsZXg6IGluaGVyaXQ7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZXBmLXRhYmxlX3dyYXBwZXJfX3RhYmxlIHRhYmxlLm1hdC10YWJsZSB0ci5tYXQtaGVhZGVyLXJvdyB7XG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjZWRlZGVkO1xufVxuLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LXJvdyB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWRlZGVkO1xufVxuLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LXJvdzpudGgtY2hpbGQob2RkKSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmOWZhZmU7XG59XG4uZXBmLXRhYmxlX3dyYXBwZXJfX3RhYmxlLnRhYmxlX2V4cGFuZGFibGUgdGFibGUubWF0LXRhYmxlIHRyLm1hdC1yb3cge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufVxuLmVwZi10YWJsZV93cmFwcGVyX190YWJsZS50YWJsZV9leHBhbmRhYmxlIHRhYmxlLm1hdC10YWJsZSB0ci5tYXQtcm93Om50aC1jaGlsZChvZGQpIHtcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcbn1cbi5lcGYtdGFibGVfd3JhcHBlcl9fdGFibGUudGFibGVfZXhwYW5kYWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LXJvdzpudGgtY2hpbGQoNG4rMSkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmYWZlO1xufVxuLmVwZi10YWJsZV93cmFwcGVyX190YWJsZS50YWJsZV9leHBhbmRhYmxlIHRhYmxlLm1hdC10YWJsZSB0ci5tYXQtcm93LmV4cGFuZGVkIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlZGVkZWQ7XG59XG4uZXBmLXRhYmxlX3dyYXBwZXJfX3RhYmxlLnRhYmxlX2V4cGFuZGFibGUgdGFibGUubWF0LXRhYmxlIHRyLm1hdC1yb3cgdGFibGUge1xuICB3aWR0aDogMTAwJTtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTc2cHgpIHtcbiAgLmVwZi10YWJsZV93cmFwcGVyX190aXRsZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgLmVwZi10YWJsZV93cmFwcGVyX19zZWFyY2gge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5lcGYtdGFibGVfd3JhcHBlcl9fdGFibGUge1xuICAgIGdyaWQtY29sdW1uOiAyLzI7XG4gIH1cbiAgLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LWhlYWRlci1yb3cgdGgubWF0LWhlYWRlci1jZWxsOm5vdCg6bnRoLWNoaWxkKDEpKSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuICAuZXBmLXRhYmxlX3dyYXBwZXJfX3RhYmxlIHRhYmxlLm1hdC10YWJsZSB0ci5tYXQtaGVhZGVyLXJvdyB0aC5tYXQtaGVhZGVyLWNlbGw6bnRoLWNoaWxkKDEpIHtcbiAgICBwYWRkaW5nOiAyMHB4IDEwcHg7XG4gIH1cbiAgLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LXJvdyB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbiAgLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LXJvdyB0ZC5tYXQtY2VsbCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwYWRkaW5nOiAycHggMTBweDtcbiAgfVxuICAuZXBmLXRhYmxlX3dyYXBwZXJfX3RhYmxlIHRhYmxlLm1hdC10YWJsZSB0ci5tYXQtcm93IHRkLm1hdC1jZWxsLm1hdC1jb2x1bW4tc2VsZWN0IHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIC5lcGYtdGFibGVfd3JhcHBlcl9fdGFibGUgdGFibGUubWF0LXRhYmxlIHRyLm1hdC1yb3cgdGQubWF0LWNlbGw6YmVmb3JlIHtcbiAgICBmbGV4LWdyb3c6IDE7XG4gICAgY29udGVudDogYXR0cihkYXRhLWxhYmVsKTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBmb250LXNpemU6IDAuOWVtO1xuICB9XG4gIC5lcGYtdGFibGVfd3JhcHBlcl9fdGFibGUgdGFibGUubWF0LXRhYmxlIHRyLm1hdC1yb3cgdGQubWF0LWNlbGw6bnRoLWNoaWxkKDEpIHtcbiAgICBwYWRkaW5nOiAyMHB4IDEwcHg7XG4gIH1cbiAgLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LXJvdyB0ZC5tYXQtY2VsbDpudGgtY2hpbGQoMSk6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbiAgLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LXJvdyB0ZC5tYXQtY2VsbDpudGgtY2hpbGQoMikge1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gIH1cbiAgLmVwZi10YWJsZV93cmFwcGVyX190YWJsZSB0YWJsZS5tYXQtdGFibGUgdHIubWF0LXJvdyB0ZC5tYXQtY2VsbDpsYXN0LWNoaWxkIHtcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/shared/epf-table/epf-table/epf-table.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shared/epf-table/epf-table/epf-table.component.ts ***!
  \*******************************************************************/
/*! exports provided: EpfTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EpfTableComponent", function() { return EpfTableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");
/* harmony import */ var lit_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lit-html */ "./node_modules/lit-html/lit-html.js");





let EpfTableComponent = class EpfTableComponent {
    constructor() {
        this.searchby = "";
        this._searchby = "";
        this._searchbycol = false;
    }
    ngOnInit() {
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.property.table_data);
        //This is where we set the content. Function generateExpandedContent would
        //be triggered for every row of data. This will return a html template.
        if (this.property.table_expandable) {
            this.table_expandable = this.property.table_expandable;
        }
        else {
            this.table_expandable = () => { };
        }
        this.displayedColumns = this.property.table_columns.map((o) => o.slug);
        this.columnNames = {};
        this.property.table_columns.forEach((o) => {
            this.columnNames[o.slug] = o.display;
        });
        this.columnTypes = {};
        this.property.table_columns.forEach((o) => {
            this.columnTypes[o.slug] = o.currency;
        });
        this.searchby = this.property.searchby;
        if (this.property.searchby != "") {
            this._searchby = this.property.searchby.replace(/_/g, " ");
            this._searchbycol = true;
        }
        this.dataSource.filterPredicate = (data, filtersJson) => {
            const matchFilter = [];
            const filters = JSON.parse(filtersJson);
            filters.forEach((filter) => {
                const val = data[filter.id] === null ? "" : data[filter.id];
                matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
            });
            return matchFilter.every(Boolean);
        };
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this._paginator;
        this.dataSource.sort = this._sort;
    }
    // filter
    applyFilter(filterValue) {
        if (this.searchby == "") {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
        else {
            const tableFilters = [];
            tableFilters.push({
                id: this.searchby,
                value: filterValue
            });
            this.dataSource.filter = JSON.stringify(tableFilters);
        }
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    getHTML(template) {
        let host = document.createElement("div");
        Object(lit_html__WEBPACK_IMPORTED_MODULE_4__["render"])(template, host);
        return host.innerHTML;
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("prop"),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], EpfTableComponent.prototype, "property", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
], EpfTableComponent.prototype, "_paginator", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
], EpfTableComponent.prototype, "_sort", void 0);
EpfTableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-table",
        template: __webpack_require__(/*! raw-loader!./epf-table.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/epf-table/epf-table/epf-table.component.html"),
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["trigger"])("detailExpand", [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["state"])("collapsed", Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ height: "0px", minHeight: "0", padding: "0px 20px" })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["state"])("expanded", Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ height: "*" })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["transition"])("expanded <=> collapsed", Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))
            ])
        ],
        styles: [__webpack_require__(/*! ./epf-table.component.sass */ "./src/app/shared/epf-table/epf-table/epf-table.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EpfTableComponent);



/***/ }),

/***/ "./src/app/shared/epf-table/epf-table/index.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/epf-table/epf-table/index.ts ***!
  \*****************************************************/
/*! exports provided: EpfTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_table_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-table.component */ "./src/app/shared/epf-table/epf-table/epf-table.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfTableComponent", function() { return _epf_table_component__WEBPACK_IMPORTED_MODULE_0__["EpfTableComponent"]; });




/***/ }),

/***/ "./src/app/shared/epf-table/index.ts":
/*!*******************************************!*\
  !*** ./src/app/shared/epf-table/index.ts ***!
  \*******************************************/
/*! exports provided: EpfTableModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _epf_table_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./epf-table.module */ "./src/app/shared/epf-table/epf-table.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfTableModule", function() { return _epf_table_module__WEBPACK_IMPORTED_MODULE_0__["EpfTableModule"]; });




/***/ }),

/***/ "./src/app/shared/index.ts":
/*!*********************************!*\
  !*** ./src/app/shared/index.ts ***!
  \*********************************/
/*! exports provided: SharedModule, EpfFieldsModule, EpfTableModule, Ringgit, getHTMLRinggit, Nric, OptinService, EpfFieldsInputsComponent, EpfFieldsTogglesComponent, EpfFieldsDropdownsComponent, EpfFieldsTextareaComponent, EpfFieldsButtonsComponent, defaultErrors, FORM_ERRORS, EpfFieldsWrapperComponent, EpfFieldsLabelComponent, EpfFieldsErrorComponent, ControlErrorsDirective, ControlErrorsContainerDirective, ControlSubmissionDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shared_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shared.module */ "./src/app/shared/shared.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return _shared_module__WEBPACK_IMPORTED_MODULE_0__["SharedModule"]; });

/* harmony import */ var _epf_fields__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./epf-fields */ "./src/app/shared/epf-fields/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsModule", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsInputsComponent", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsInputsComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTogglesComponent", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsTogglesComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsDropdownsComponent", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsDropdownsComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsTextareaComponent", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsTextareaComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsButtonsComponent", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsButtonsComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "defaultErrors", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["defaultErrors"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FORM_ERRORS", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["FORM_ERRORS"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsWrapperComponent", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsWrapperComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsLabelComponent", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsLabelComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfFieldsErrorComponent", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["EpfFieldsErrorComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsDirective", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["ControlErrorsDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlErrorsContainerDirective", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["ControlErrorsContainerDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ControlSubmissionDirective", function() { return _epf_fields__WEBPACK_IMPORTED_MODULE_1__["ControlSubmissionDirective"]; });

/* harmony import */ var _epf_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./epf-table */ "./src/app/shared/epf-table/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EpfTableModule", function() { return _epf_table__WEBPACK_IMPORTED_MODULE_2__["EpfTableModule"]; });

/* harmony import */ var _generals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_generals */ "./src/app/shared/_generals/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Ringgit", function() { return _generals__WEBPACK_IMPORTED_MODULE_3__["Ringgit"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getHTMLRinggit", function() { return _generals__WEBPACK_IMPORTED_MODULE_3__["getHTMLRinggit"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Nric", function() { return _generals__WEBPACK_IMPORTED_MODULE_3__["Nric"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OptinService", function() { return _generals__WEBPACK_IMPORTED_MODULE_3__["OptinService"]; });







/***/ }),

/***/ "./src/app/shared/page-title/index.ts":
/*!********************************************!*\
  !*** ./src/app/shared/page-title/index.ts ***!
  \********************************************/
/*! exports provided: PageTitleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _page_title_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./page-title.component */ "./src/app/shared/page-title/page-title.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PageTitleComponent", function() { return _page_title_component__WEBPACK_IMPORTED_MODULE_0__["PageTitleComponent"]; });




/***/ }),

/***/ "./src/app/shared/page-title/page-title.component.sass":
/*!*************************************************************!*\
  !*** ./src/app/shared/page-title/page-title.component.sass ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1, h2, h3, h4,\n.h1, .h2, .h3, .h4 {\n  margin: 0;\n}\nh1:first-child, h2:first-child, h3:first-child, h4:first-child,\n.h1:first-child, .h2:first-child, .h3:first-child, .h4:first-child {\n  margin-top: 0;\n}\nh1:last-child, h2:last-child, h3:last-child, h4:last-child,\n.h1:last-child, .h2:last-child, .h3:last-child, .h4:last-child {\n  margin-bottom: 0;\n}\nh1, .h1 {\n  font-family: \"EPF Book\";\n  font-size: 56px;\n  font-size: 3.5em;\n  line-height: 5.25em;\n}\nh3, .h3 {\n  font-family: \"EPF Medium\";\n  font-size: 26px;\n  font-size: 1.625em;\n  line-height: 2.4375em;\n}\np {\n  width: 100%;\n  font-family: \"EPF Book\";\n  font-size: 16px;\n  font-size: 1em;\n  line-height: 1.5em;\n  transition: all 0.2s ease-in-out;\n}\np:first-child {\n  margin-top: 0;\n}\np:last-child {\n  margin-bottom: 0;\n}\np a {\n  text-decoration: none;\n  transition: all 0.2s ease-in-out;\n  color: inherit;\n}\np a:hover {\n  text-decoration: none;\n  color: inherit;\n}\n.epf-page-title {\n  margin-bottom: 30px;\n}\n.epf-page-title h1 {\n  color: #312783;\n  font-size: 32px;\n  font-size: 2em;\n  line-height: 3em;\n  line-height: normal;\n  padding-bottom: 20px;\n  border-bottom: 1px solid #DDDDDD;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3BhZ2UtdGl0bGUvQzpcXERldlxcUHJvamVjdHNcXGV5XFxjbG91ZFxccGF5cm9sbC1wcm90b3R5cGUtLW9wdC1pblxccHJvdG90eXBlL3NyY1xcYXBwXFxzaGFyZWRcXHBhZ2UtdGl0bGVcXHBhZ2UtdGl0bGUuY29tcG9uZW50LnNhc3MiLCJzcmMvYXBwL3NoYXJlZC9wYWdlLXRpdGxlL3BhZ2UtdGl0bGUuY29tcG9uZW50LnNhc3MiLCJzcmMvYXBwL3NoYXJlZC9wYWdlLXRpdGxlL0M6XFxEZXZcXFByb2plY3RzXFxleVxcY2xvdWRcXHBheXJvbGwtcHJvdG90eXBlLS1vcHQtaW5cXHByb3RvdHlwZS9zdGRpbiIsInNyYy9hcHAvc2hhcmVkL3BhZ2UtdGl0bGUvQzpcXERldlxcUHJvamVjdHNcXGV5XFxjbG91ZFxccGF5cm9sbC1wcm90b3R5cGUtLW9wdC1pblxccHJvdG90eXBlL3NyY1xcX3N0eWxlc1xcX3ZhcmlhYmxlcy5zYXNzIiwic3JjL2FwcC9zaGFyZWQvcGFnZS10aXRsZS9DOlxcRGV2XFxQcm9qZWN0c1xcZXlcXGNsb3VkXFxwYXlyb2xsLXByb3RvdHlwZS0tb3B0LWluXFxwcm90b3R5cGUvc3JjXFxfc3R5bGVzXFxfY29sb3Vycy5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWlCQTs7RUFFSSxTQUFBO0FDaEJKO0FEaUJJOztFQUNJLGFBQUE7QUNkUjtBRGVJOztFQUNJLGdCQUFBO0FDWlI7QURjQTtFQUNJLHVCQXRCRTtFQVFGLGVBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDSUo7QURXQTtFQUNJLHlCQXZCTTtFQUtOLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0FDV0o7QURRQTtFQUNJLFdBQUE7RUFDQSx1QkEvQkU7RUFRRixlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBdUJBLGdDQUFBO0FDSEo7QURJSTtFQUNJLGFBQUE7QUNGUjtBREdJO0VBQ0ksZ0JBQUE7QUNEUjtBREVJO0VBQ0kscUJBQUE7RUFDQSxnQ0FBQTtFQUNBLGNBQUE7QUNBUjtBRENRO0VBQ0kscUJBQUE7RUFDQSxjQUFBO0FDQ1o7QUM1Q0E7RUFDSSxtQkNEUztBRmdEYjtBQzlDSTtFQUNJLGNFY1M7RUpWYixlQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VFSkksbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdDQUFBO0FEa0RSIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL3BhZ2UtdGl0bGUvcGFnZS10aXRsZS5jb21wb25lbnQuc2FzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIERlZmF1bHQgc2l6aW5nXHJcbiRiYXNlLWZvbnQtc2l6ZTogMTYgIWRlZmF1bHRcclxuJGJhc2UtbGluZS1oZWlnaHQ6IDEuNSAhZGVmYXVsdFxyXG5cclxuJHByaTogJ0VQRiBCb29rJ1xyXG4kcHJpLWl0OiAnRVBGIEJvb2sgSXRhbGljJ1xyXG4kcHJpLWRlbWk6ICdFUEYgRGVtaSdcclxuJHByaS1tZWQ6ICdFUEYgTWVkaXVtJ1xyXG4kcHJpLWh2eTogJ0VQRiBIZWF2eSdcclxuXHJcbi8vIE1peGluc1xyXG5AbWl4aW4gZm9udC1zaXplKCRzaXplLWluLXB4LCAkbGluZS1oZWlnaHQ6MS41KVxyXG4gICAgZm9udC1zaXplOiAkc2l6ZS1pbi1weCAqIDFweFxyXG4gICAgZm9udC1zaXplOiAoJHNpemUtaW4tcHggLyAkYmFzZS1mb250LXNpemUpICogMWVtXHJcbiAgICBsaW5lLWhlaWdodDogKCgkc2l6ZS1pbi1weCAvICRiYXNlLWZvbnQtc2l6ZSkgKiAkbGluZS1oZWlnaHQpICogMWVtXHJcblxyXG5cclxuaDEsIGgyLCBoMywgaDQsXHJcbi5oMSwgLmgyLCAuaDMsIC5oNFxyXG4gICAgbWFyZ2luOiAwXHJcbiAgICAmOmZpcnN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLXRvcDogMFxyXG4gICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG5cclxuaDEsIC5oMVxyXG4gICAgZm9udC1mYW1pbHk6ICRwcmlcclxuICAgIEBpbmNsdWRlIGZvbnQtc2l6ZSg1NilcclxuXHJcbmgzLCAuaDNcclxuICAgIGZvbnQtZmFtaWx5OiAkcHJpLW1lZFxyXG4gICAgQGluY2x1ZGUgZm9udC1zaXplKDI2KVxyXG5cclxucFxyXG4gICAgd2lkdGg6IDEwMCVcclxuICAgIGZvbnQtZmFtaWx5OiAkcHJpXHJcbiAgICBAaW5jbHVkZSBmb250LXNpemUoMTYpXHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dFxyXG4gICAgJjpmaXJzdC1jaGlsZFxyXG4gICAgICAgIG1hcmdpbi10b3A6IDBcclxuICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBcclxuICAgIGFcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmVcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlLWluLW91dFxyXG4gICAgICAgIGNvbG9yOiBpbmhlcml0XHJcbiAgICAgICAgJjpob3ZlclxyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmVcclxuICAgICAgICAgICAgY29sb3I6IGluaGVyaXRcclxuIiwiaDEsIGgyLCBoMywgaDQsXG4uaDEsIC5oMiwgLmgzLCAuaDQge1xuICBtYXJnaW46IDA7XG59XG5oMTpmaXJzdC1jaGlsZCwgaDI6Zmlyc3QtY2hpbGQsIGgzOmZpcnN0LWNoaWxkLCBoNDpmaXJzdC1jaGlsZCxcbi5oMTpmaXJzdC1jaGlsZCwgLmgyOmZpcnN0LWNoaWxkLCAuaDM6Zmlyc3QtY2hpbGQsIC5oNDpmaXJzdC1jaGlsZCB7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5oMTpsYXN0LWNoaWxkLCBoMjpsYXN0LWNoaWxkLCBoMzpsYXN0LWNoaWxkLCBoNDpsYXN0LWNoaWxkLFxuLmgxOmxhc3QtY2hpbGQsIC5oMjpsYXN0LWNoaWxkLCAuaDM6bGFzdC1jaGlsZCwgLmg0Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG5oMSwgLmgxIHtcbiAgZm9udC1mYW1pbHk6IFwiRVBGIEJvb2tcIjtcbiAgZm9udC1zaXplOiA1NnB4O1xuICBmb250LXNpemU6IDMuNWVtO1xuICBsaW5lLWhlaWdodDogNS4yNWVtO1xufVxuXG5oMywgLmgzIHtcbiAgZm9udC1mYW1pbHk6IFwiRVBGIE1lZGl1bVwiO1xuICBmb250LXNpemU6IDI2cHg7XG4gIGZvbnQtc2l6ZTogMS42MjVlbTtcbiAgbGluZS1oZWlnaHQ6IDIuNDM3NWVtO1xufVxuXG5wIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtZmFtaWx5OiBcIkVQRiBCb29rXCI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC1zaXplOiAxZW07XG4gIGxpbmUtaGVpZ2h0OiAxLjVlbTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG59XG5wOmZpcnN0LWNoaWxkIHtcbiAgbWFyZ2luLXRvcDogMDtcbn1cbnA6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5wIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICBjb2xvcjogaW5oZXJpdDtcbn1cbnAgYTpob3ZlciB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY29sb3I6IGluaGVyaXQ7XG59XG5cbi5lcGYtcGFnZS10aXRsZSB7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG4uZXBmLXBhZ2UtdGl0bGUgaDEge1xuICBjb2xvcjogIzMxMjc4MztcbiAgZm9udC1zaXplOiAzMnB4O1xuICBmb250LXNpemU6IDJlbTtcbiAgbGluZS1oZWlnaHQ6IDNlbTtcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjREREREREO1xufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi9fc3R5bGVzL191dGlsc1wiXHJcbkBpbXBvcnQgXCIuLi8uLi8uLi9fc3R5bGVzL19jb2xvdXJzXCJcclxuQGltcG9ydCBcIi4uLy4uLy4uL19zdHlsZXMvX3ZhcmlhYmxlc1wiXHJcbkBpbXBvcnQgXCIuLi8uLi8uLi9fc3R5bGVzL19mb250c1wiXHJcblxyXG4uZXBmLXBhZ2UtdGl0bGVcclxuICAgIG1hcmdpbi1ib3R0b206ICRtYXJnaW4tYmlnXHJcbiAgICBoMVxyXG4gICAgICAgIGNvbG9yOiAkZ3JhZGllbnRfYmx1ZTFcclxuICAgICAgICBAaW5jbHVkZSBmb250LXNpemUoMzIpXHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbFxyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAkcGFkZGluZyoyXHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRkYXJrX2dyZXk0IiwiLy8gcGFkZGluZ1xyXG4kcGFkZGluZ19iaWc6IDMwcHhcclxuJHBhZGRpbmc6IDEwcHhcclxuXHJcbi8vIG1hcmdpblxyXG4kbWFyZ2luX2JpZzogMzBweFxyXG4kbWFyZ2luOiAxMHB4IiwiLy8gTWFpbiBDb2xvdXJzXHJcbiRlcGZfYmx1ZTogIzMxMjc4MlxyXG4kZXBmX3JlZDogI0UzMDYxM1xyXG4kZXBmX2dvbGQ6ICNENEE5MzlcclxuXHJcbi8vIFN1cHBvcnRpdmUgQ29sb3Vyc1xyXG4kYmx1ZTE6ICM0MTMxQjhcclxuJGJsdWUyOiAjNTAzRkRDXHJcbiRibHVlMzogI0Y2RkFGRVxyXG5cclxuLy8gTmV1dHJhbCBDb2xvdXJzXHJcbiRkYXJrX2dyZXkxOiAjMzMzMzMzXHJcbiRkYXJrX2dyZXkyOiAjNjY2NjY2XHJcbiRkYXJrX2dyZXkzOiAjOTk5OTk5XHJcbiRkYXJrX2dyZXk0OiAjREREREREXHJcbiRsaWdodF9ncmV5MTogI0Y0RjRGNFxyXG4kbGlnaHRfZ3JleTI6ICNGOEY4RjhcclxuJGxpZ2h0X2dyZXkzOiAjRkNGQ0ZDXHJcblxyXG4kd2hpdGU6ICNmZmZmZmZcclxuJGJsYWNrOiAjMDAwMDAwXHJcblxyXG4kZ3JhZGllbnRfYmx1ZTE6ICMzMTI3ODNcclxuJGdyYWRpZW50X2JsdWUyOiAjMzgyOEJCIl19 */"

/***/ }),

/***/ "./src/app/shared/page-title/page-title.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/page-title/page-title.component.ts ***!
  \***********************************************************/
/*! exports provided: PageTitleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageTitleComponent", function() { return PageTitleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PageTitleComponent = class PageTitleComponent {
    constructor() { }
    ngOnInit() { }
};
PageTitleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "epf-page-title",
        template: __webpack_require__(/*! raw-loader!./page-title.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/page-title/page-title.component.html"),
        styles: [__webpack_require__(/*! ./page-title.component.sass */ "./src/app/shared/page-title/page-title.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PageTitleComponent);



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _epf_fields__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./epf-fields */ "./src/app/shared/epf-fields/index.ts");
/* harmony import */ var _epf_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./epf-table */ "./src/app/shared/epf-table/index.ts");
/* harmony import */ var _page_title__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./page-title */ "./src/app/shared/page-title/index.ts");






let SharedModule = class SharedModule {
};
SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_page_title__WEBPACK_IMPORTED_MODULE_5__["PageTitleComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _epf_fields__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsModule"], _epf_table__WEBPACK_IMPORTED_MODULE_4__["EpfTableModule"]],
        exports: [_epf_fields__WEBPACK_IMPORTED_MODULE_3__["EpfFieldsModule"], _epf_table__WEBPACK_IMPORTED_MODULE_4__["EpfTableModule"], _page_title__WEBPACK_IMPORTED_MODULE_5__["PageTitleComponent"]]
    })
], SharedModule);



/***/ }),

/***/ "./src/app/transactions/index.ts":
/*!***************************************!*\
  !*** ./src/app/transactions/index.ts ***!
  \***************************************/
/*! exports provided: TransactionsModule, TransactionOverviewComponent, TransactionAgencyComponent, TransactionSlipsComponent, TransactionRecordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _transactions_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./transactions.module */ "./src/app/transactions/transactions.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TransactionsModule", function() { return _transactions_module__WEBPACK_IMPORTED_MODULE_0__["TransactionsModule"]; });

/* harmony import */ var _transaction_overview__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./transaction-overview */ "./src/app/transactions/transaction-overview/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TransactionOverviewComponent", function() { return _transaction_overview__WEBPACK_IMPORTED_MODULE_1__["TransactionOverviewComponent"]; });

/* harmony import */ var _transaction_agency__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./transaction-agency */ "./src/app/transactions/transaction-agency/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TransactionAgencyComponent", function() { return _transaction_agency__WEBPACK_IMPORTED_MODULE_2__["TransactionAgencyComponent"]; });

/* harmony import */ var _transaction_slips__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./transaction-slips */ "./src/app/transactions/transaction-slips/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TransactionSlipsComponent", function() { return _transaction_slips__WEBPACK_IMPORTED_MODULE_3__["TransactionSlipsComponent"]; });

/* harmony import */ var _transaction_record__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./transaction-record */ "./src/app/transactions/transaction-record/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TransactionRecordComponent", function() { return _transaction_record__WEBPACK_IMPORTED_MODULE_4__["TransactionRecordComponent"]; });








/***/ }),

/***/ "./src/app/transactions/transaction-agency/index.ts":
/*!**********************************************************!*\
  !*** ./src/app/transactions/transaction-agency/index.ts ***!
  \**********************************************************/
/*! exports provided: TransactionAgencyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _transaction_agency_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./transaction-agency.component */ "./src/app/transactions/transaction-agency/transaction-agency.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TransactionAgencyComponent", function() { return _transaction_agency_component__WEBPACK_IMPORTED_MODULE_0__["TransactionAgencyComponent"]; });




/***/ }),

/***/ "./src/app/transactions/transaction-agency/transaction-agency.component.sass":
/*!***********************************************************************************!*\
  !*** ./src/app/transactions/transaction-agency/transaction-agency.component.sass ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RyYW5zYWN0aW9ucy90cmFuc2FjdGlvbi1hZ2VuY3kvdHJhbnNhY3Rpb24tYWdlbmN5LmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/transactions/transaction-agency/transaction-agency.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/transactions/transaction-agency/transaction-agency.component.ts ***!
  \*********************************************************************************/
/*! exports provided: TransactionAgencyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionAgencyComponent", function() { return TransactionAgencyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TransactionAgencyComponent = class TransactionAgencyComponent {
    constructor() { }
    ngOnInit() {
    }
};
TransactionAgencyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-transaction-agency',
        template: __webpack_require__(/*! raw-loader!./transaction-agency.component.html */ "./node_modules/raw-loader/index.js!./src/app/transactions/transaction-agency/transaction-agency.component.html"),
        styles: [__webpack_require__(/*! ./transaction-agency.component.sass */ "./src/app/transactions/transaction-agency/transaction-agency.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], TransactionAgencyComponent);



/***/ }),

/***/ "./src/app/transactions/transaction-overview/index.ts":
/*!************************************************************!*\
  !*** ./src/app/transactions/transaction-overview/index.ts ***!
  \************************************************************/
/*! exports provided: TransactionOverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _transaction_overview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./transaction-overview.component */ "./src/app/transactions/transaction-overview/transaction-overview.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TransactionOverviewComponent", function() { return _transaction_overview_component__WEBPACK_IMPORTED_MODULE_0__["TransactionOverviewComponent"]; });




/***/ }),

/***/ "./src/app/transactions/transaction-overview/transaction-overview.component.sass":
/*!***************************************************************************************!*\
  !*** ./src/app/transactions/transaction-overview/transaction-overview.component.sass ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RyYW5zYWN0aW9ucy90cmFuc2FjdGlvbi1vdmVydmlldy90cmFuc2FjdGlvbi1vdmVydmlldy5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/transactions/transaction-overview/transaction-overview.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/transactions/transaction-overview/transaction-overview.component.ts ***!
  \*************************************************************************************/
/*! exports provided: TransactionOverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionOverviewComponent", function() { return TransactionOverviewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TransactionOverviewComponent = class TransactionOverviewComponent {
    constructor() { }
    ngOnInit() {
    }
};
TransactionOverviewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-transaction-overview',
        template: __webpack_require__(/*! raw-loader!./transaction-overview.component.html */ "./node_modules/raw-loader/index.js!./src/app/transactions/transaction-overview/transaction-overview.component.html"),
        styles: [__webpack_require__(/*! ./transaction-overview.component.sass */ "./src/app/transactions/transaction-overview/transaction-overview.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], TransactionOverviewComponent);



/***/ }),

/***/ "./src/app/transactions/transaction-record/index.ts":
/*!**********************************************************!*\
  !*** ./src/app/transactions/transaction-record/index.ts ***!
  \**********************************************************/
/*! exports provided: TransactionRecordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _transaction_record_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./transaction-record.component */ "./src/app/transactions/transaction-record/transaction-record.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TransactionRecordComponent", function() { return _transaction_record_component__WEBPACK_IMPORTED_MODULE_0__["TransactionRecordComponent"]; });




/***/ }),

/***/ "./src/app/transactions/transaction-record/transaction-record.component.sass":
/*!***********************************************************************************!*\
  !*** ./src/app/transactions/transaction-record/transaction-record.component.sass ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RyYW5zYWN0aW9ucy90cmFuc2FjdGlvbi1yZWNvcmQvdHJhbnNhY3Rpb24tcmVjb3JkLmNvbXBvbmVudC5zYXNzIn0= */"

/***/ }),

/***/ "./src/app/transactions/transaction-record/transaction-record.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/transactions/transaction-record/transaction-record.component.ts ***!
  \*********************************************************************************/
/*! exports provided: TransactionRecordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionRecordComponent", function() { return TransactionRecordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TransactionRecordComponent = class TransactionRecordComponent {
    constructor() { }
    ngOnInit() {
    }
};
TransactionRecordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-transaction-record',
        template: __webpack_require__(/*! raw-loader!./transaction-record.component.html */ "./node_modules/raw-loader/index.js!./src/app/transactions/transaction-record/transaction-record.component.html"),
        styles: [__webpack_require__(/*! ./transaction-record.component.sass */ "./src/app/transactions/transaction-record/transaction-record.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], TransactionRecordComponent);



/***/ }),

/***/ "./src/app/transactions/transaction-slips/index.ts":
/*!*********************************************************!*\
  !*** ./src/app/transactions/transaction-slips/index.ts ***!
  \*********************************************************/
/*! exports provided: TransactionSlipsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _transaction_slips_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./transaction-slips.component */ "./src/app/transactions/transaction-slips/transaction-slips.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TransactionSlipsComponent", function() { return _transaction_slips_component__WEBPACK_IMPORTED_MODULE_0__["TransactionSlipsComponent"]; });




/***/ }),

/***/ "./src/app/transactions/transaction-slips/transaction-slips.component.sass":
/*!*********************************************************************************!*\
  !*** ./src/app/transactions/transaction-slips/transaction-slips.component.sass ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RyYW5zYWN0aW9ucy90cmFuc2FjdGlvbi1zbGlwcy90cmFuc2FjdGlvbi1zbGlwcy5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/transactions/transaction-slips/transaction-slips.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/transactions/transaction-slips/transaction-slips.component.ts ***!
  \*******************************************************************************/
/*! exports provided: TransactionSlipsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionSlipsComponent", function() { return TransactionSlipsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TransactionSlipsComponent = class TransactionSlipsComponent {
    constructor() { }
    ngOnInit() {
    }
};
TransactionSlipsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'epf-transaction-slips',
        template: __webpack_require__(/*! raw-loader!./transaction-slips.component.html */ "./node_modules/raw-loader/index.js!./src/app/transactions/transaction-slips/transaction-slips.component.html"),
        styles: [__webpack_require__(/*! ./transaction-slips.component.sass */ "./src/app/transactions/transaction-slips/transaction-slips.component.sass")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], TransactionSlipsComponent);



/***/ }),

/***/ "./src/app/transactions/transactions-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/transactions/transactions-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: TransactionsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsRoutingModule", function() { return TransactionsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _transaction_overview__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./transaction-overview */ "./src/app/transactions/transaction-overview/index.ts");
/* harmony import */ var _transaction_agency__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./transaction-agency */ "./src/app/transactions/transaction-agency/index.ts");
/* harmony import */ var _transaction_slips__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./transaction-slips */ "./src/app/transactions/transaction-slips/index.ts");
/* harmony import */ var _transaction_record__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./transaction-record */ "./src/app/transactions/transaction-record/index.ts");







const routes = [
    {
        path: "",
        component: _transaction_overview__WEBPACK_IMPORTED_MODULE_3__["TransactionOverviewComponent"],
        children: [
            {
                path: "agency:id",
                component: _transaction_agency__WEBPACK_IMPORTED_MODULE_4__["TransactionAgencyComponent"]
            },
            {
                path: "slips:id",
                component: _transaction_slips__WEBPACK_IMPORTED_MODULE_5__["TransactionSlipsComponent"]
            },
            {
                path: "record:id",
                component: _transaction_record__WEBPACK_IMPORTED_MODULE_6__["TransactionRecordComponent"]
            }
        ]
    }
];
let TransactionsRoutingModule = class TransactionsRoutingModule {
};
TransactionsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], TransactionsRoutingModule);



/***/ }),

/***/ "./src/app/transactions/transactions.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/transactions/transactions.module.ts ***!
  \*****************************************************/
/*! exports provided: TransactionsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsModule", function() { return TransactionsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _transactions_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./transactions-routing.module */ "./src/app/transactions/transactions-routing.module.ts");
/* harmony import */ var _transaction_overview__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./transaction-overview */ "./src/app/transactions/transaction-overview/index.ts");
/* harmony import */ var _transaction_agency__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./transaction-agency */ "./src/app/transactions/transaction-agency/index.ts");
/* harmony import */ var _transaction_slips__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./transaction-slips */ "./src/app/transactions/transaction-slips/index.ts");
/* harmony import */ var _transaction_record__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./transaction-record */ "./src/app/transactions/transaction-record/index.ts");









let TransactionsModule = class TransactionsModule {
};
TransactionsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _transaction_overview__WEBPACK_IMPORTED_MODULE_5__["TransactionOverviewComponent"],
            _transaction_agency__WEBPACK_IMPORTED_MODULE_6__["TransactionAgencyComponent"],
            _transaction_slips__WEBPACK_IMPORTED_MODULE_7__["TransactionSlipsComponent"],
            _transaction_record__WEBPACK_IMPORTED_MODULE_8__["TransactionRecordComponent"]
        ],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _transactions_routing_module__WEBPACK_IMPORTED_MODULE_4__["TransactionsRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]],
        exports: [
            _transaction_overview__WEBPACK_IMPORTED_MODULE_5__["TransactionOverviewComponent"],
            _transaction_agency__WEBPACK_IMPORTED_MODULE_6__["TransactionAgencyComponent"],
            _transaction_slips__WEBPACK_IMPORTED_MODULE_7__["TransactionSlipsComponent"],
            _transaction_record__WEBPACK_IMPORTED_MODULE_8__["TransactionRecordComponent"]
        ]
    })
], TransactionsModule);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    apiUrl: 'http://localhost:3000'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
if (window.WebComponents && window.WebComponents.ready) {
    // Web Components are ready
    bootstrapModule();
}
else {
    // Wait for polyfills to load
    window.addEventListener('WebComponentsReady', bootstrapModule);
}
function bootstrapModule() {
    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
        .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
        .catch(err => console.error(err));
}


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Dev\Projects\ey\cloud\payroll-prototype--opt-in\prototype\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map