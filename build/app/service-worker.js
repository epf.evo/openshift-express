const version = 'v20190626081736'

/*************************************************************/
/* Section 1 : Cache management                              */
/*************************************************************/
self.addEventListener('updatefound',() => {
  console.log("Service Worker update found")
  const installingWorker = reg.installing;
  installingWorker.onstatechange = () => {
      switch (installingWorker.state) {
          case 'installed':
          if (navigator.serviceWorker.controller) {
              console.log("[ServiceWorker] Update installed")
          } else {
              console.log("[ServiceWorker] Service Worker up to date")
          }
      }
  }
})


/*************************************************************/
/* Section 2 : Cache management                              */
/*************************************************************/

const filesToCache = [
  '/',
  '/home',
  '/payroll',
  '/employees',
  '/transactions',
  '/settings',
/*INJECT STATIC CACHE*/
];

const runtimeCacheName = `epf-payroll-runtime-${version}`;
const staticCacheName = runtimeCacheName; //`epf-payroll-precache-${version}`;

self.addEventListener('install', event => {
  console.log(`Attempting to install service worker and cache static assets ${version}`);
  event.waitUntil(
    caches.open(staticCacheName)
      .then(cache => {
        return cache.addAll(filesToCache);
      })
  );
});
self.addEventListener('activate', function (e) {
  console.log(`[ServiceWorker] Activated ${version}`);
  e.waitUntil(
    // Get all the cache keys (cacheName)
    caches.keys().then(function (cacheNames) {
      return Promise.all(cacheNames.map(function (thisCacheName) {
        // If a cached item is saved under a previous cacheName
        if ((thisCacheName !== staticCacheName) && (thisCacheName !== runtimeCacheName)) {
          // Delete that cached file
          console.log('[ServiceWorker] Removing Cached Files from Cache - ', thisCacheName);
          return caches.delete(thisCacheName);
        }
      }));
    })
  ); // end e.waitUntil
});


const fetchThenCache = function(event) {
  return fetch(event.request).then(response => {
    if(!response.ok){
      //Fetch failed. For now we handle error internally
      console.log(`Fetch request failed for ${event.request.url}`)
      //throw Error(`Fetch request failed for ${event.request.url}`)
    }
    return caches.open(runtimeCacheName).then(cache => {
      cache.put(event.request.url, response.clone());
      return response;
    });
  });
}

const respondCacheFirst = function(event) {

  console.log(`[Cache First] : ${event.request.url}`);
  event.respondWith( 
    caches.match(event.request)
      .then(response => {
        if (response) {
          console.log('[Cache First] : Found ', event.request.url, ' in cache.');
          if(navigator.onLine) { 
            console.log('[Cache First] : Updating cache anyway.');
            fetchThenCache(event)
          }
          return response;
        }
        console.log('[Cache First] : No cache found. Network request for ', event.request.url);
        return fetchThenCache(event)

    }).catch(error => {
      console.log('[Cache First] Error executing strategy for ', event.request.url);
      // TODO 6 - Respond with custom offline page
      return fetchThenCache(event)
    })
  );
}

const respondNetworkFirst = function(event) {
  console.log(`[Network First] : ${event.request.url}`);
  event.respondWith( 
    fetchThenCache(event)
    .catch(error => {
      console.log('[Network First] Failed fetch ', event.request.url);
      // TODO 6 - Respond with custom offline page
      return caches.match(event.request)
        .then(response => {
          if (response) {
            console.log('[Network First] Found cache ', event.request.url);
            return response;
          } else {
            console.log('[Network First] No cache found after network request failed for ', event.request.url);
          }
          throw error
        })
      })
  );
}
self.addEventListener('fetch', event => {
  switch (true) {
    case /\/api\/md\//g.test(event.request.url):
      return respondNetworkFirst(event)
    case /\/api\/tx\//g.test(event.request.url):
      return fetch(event.request)
    case /\/api\/push\//g.test(event.request.url):
      return fetch(event.request)
    default:
      return respondCacheFirst(event)
  }
});

/*************************************************************/
/* Section 3 : Push notification management                  */
/*************************************************************/

self.addEventListener('push', function(event) {
  const payload = JSON.parse(event.data ? event.data.text() : `{
    title : 'No title',
    message : { body : 'No message' }
  }`);
  console.log(payload)

  event.waitUntil(self.registration.showNotification(payload.title, payload.options));
});
// Notification handling
self.addEventListener('notificationclose', event => {
  const notification = event.notification;
  const primaryKey = notification.data.primaryKey;
  console.log('Closed notification: ' + primaryKey);
});
self.addEventListener('notificationclick', event => {
  console.log('Notification click event',event);
});

// Listen to  `pushsubscriptionchange` event which is fired when
// subscription expires. Subscribe again and register the new subscription
// in the server by sending a POST request with endpoint. Real world
// application would probably use also user identification.
self.addEventListener('pushsubscriptionchange', function (event) {
  console.log('Subscription expired');
  event.waitUntil(
    self.registration.pushManager.subscribe({ userVisibleOnly: true })
      .then(function (subscription) {
        console.log('Subscribed after expiration', subscription.endpoint);
        return fetch('/api/push/register', {
          method: 'post',
          headers: {
            'Content-type': 'application/json'
          },
          body: JSON.stringify({
            endpoint: subscription.endpoint
          })
        }).then(function (response) {
          if(!response.ok) {throw Error("Cannot register subscription to push notifications")}
          return response;
        }).catch(function(error){
          console.error(error)
        });
      })
  );
});

/*************************************************************/
/* Section 4 :                                               */
/*************************************************************/

/*
importScripts('workbox-lib/workbox-sw.js');

//Basic Workbox configuration
workbox.setConfig({
  debug: true, //Force production build
  modulePathPrefix: 'workbox-lib/'
});


//Advanced Workbox configuration
workbox.core.setCacheNameDetails({
  prefix: 'epf-payroll',
  suffix: 'v4',
  precache: 'precache',
  runtime: 'runtime'
});

//Workbox injection
workbox.precaching.precacheAndRoute([]);

workbox.routing.registerRoute(
  new RegExp("/api/md/.*"),
  new workbox.strategies.NetworkFirst({
    cacheName: workbox.core.cacheNames.runtime
  })
);

//Transaction requests are sent via POST anyway, so these won't be cached.
workbox.routing.registerRoute(
  new RegExp("/api/tx/.*"),
  new workbox.strategies.NetworkOnly()
);

workbox.routing.setCatchHandler(({event}) => {
  console.log('Fallback catch for destination',event.request.destination)
  return caches.match('/');
});
*/
